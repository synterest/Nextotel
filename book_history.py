# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'book_history.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_book_history(object):
    def setupUi(self, book_history):
        book_history.setObjectName("book_history")
        book_history.resize(699, 535)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        book_history.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(book_history)
        self.verticalLayout.setObjectName("verticalLayout")
        self.history = QtWidgets.QTableView(book_history)
        self.history.setObjectName("history")
        self.verticalLayout.addWidget(self.history)

        self.retranslateUi(book_history)
        QtCore.QMetaObject.connectSlotsByName(book_history)

    def retranslateUi(self, book_history):
        _translate = QtCore.QCoreApplication.translate
        book_history.setWindowTitle(_translate("book_history", "Booking history"))

import pms_rc
