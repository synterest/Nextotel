# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'current_accounts.ui'
#
# Created: Tue Dec 22 18:10:38 2015
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_current_guests(object):
    def setupUi(self, current_guests):
        current_guests.setObjectName("current_guests")
        current_guests.resize(330, 680)
        self.widget = QtWidgets.QWidget(current_guests)
        self.widget.setGeometry(QtCore.QRect(30, 20, 258, 641))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.current_accounts = QtWidgets.QTableView(self.widget)
        self.current_accounts.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.current_accounts.setShowGrid(False)
        self.current_accounts.setObjectName("current_accounts")
        self.current_accounts.horizontalHeader().setVisible(True)
        self.current_accounts.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.current_accounts)
        self.label.setBuddy(self.current_accounts)

        self.retranslateUi(current_guests)
        QtCore.QMetaObject.connectSlotsByName(current_guests)

    def retranslateUi(self, current_guests):
        _translate = QtCore.QCoreApplication.translate
        current_guests.setWindowTitle(_translate("current_guests", "Form"))
        self.label.setText(_translate("current_guests", "Select Account"))

