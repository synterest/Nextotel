# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'posting5.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_posting_form(object):
    def setupUi(self, posting_form):
        posting_form.setObjectName("posting_form")
        posting_form.resize(400, 300)
        self.accept_button = QtWidgets.QPushButton(posting_form)
        self.accept_button.setGeometry(QtCore.QRect(190, 240, 80, 23))
        self.accept_button.setObjectName("accept_button")
        self.reject_button = QtWidgets.QPushButton(posting_form)
        self.reject_button.setGeometry(QtCore.QRect(290, 240, 80, 23))
        self.reject_button.setObjectName("reject_button")
        self.widget = QtWidgets.QWidget(posting_form)
        self.widget.setGeometry(QtCore.QRect(60, 50, 276, 129))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lineEdit = QtWidgets.QLineEdit(self.widget)
        self.lineEdit.setReadOnly(True)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.res_id_edit = QtWidgets.QLineEdit(self.widget)
        self.res_id_edit.setReadOnly(True)
        self.res_id_edit.setObjectName("res_id_edit")
        self.horizontalLayout.addWidget(self.res_id_edit)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.debit = QtWidgets.QRadioButton(self.widget)
        self.debit.setChecked(True)
        self.debit.setObjectName("debit")
        self.horizontalLayout_2.addWidget(self.debit)
        self.credit = QtWidgets.QRadioButton(self.widget)
        self.credit.setObjectName("credit")
        self.horizontalLayout_2.addWidget(self.credit)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.charge_codes_combo = QtWidgets.QComboBox(self.widget)
        self.charge_codes_combo.setObjectName("charge_codes_combo")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.charge_codes_combo)
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.charge_description = QtWidgets.QLineEdit(self.widget)
        self.charge_description.setObjectName("charge_description")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.charge_description)
        self.label_3 = QtWidgets.QLabel(self.widget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.charge_amount = QtWidgets.QDoubleSpinBox(self.widget)
        self.charge_amount.setMaximum(9999.99)
        self.charge_amount.setObjectName("charge_amount")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.charge_amount)
        self.verticalLayout.addLayout(self.formLayout)

        self.retranslateUi(posting_form)
        QtCore.QMetaObject.connectSlotsByName(posting_form)

    def retranslateUi(self, posting_form):
        _translate = QtCore.QCoreApplication.translate
        posting_form.setWindowTitle(_translate("posting_form", "Posting"))
        self.accept_button.setText(_translate("posting_form", "Accept"))
        self.reject_button.setText(_translate("posting_form", "Cancel"))
        self.debit.setText(_translate("posting_form", "Debit"))
        self.credit.setText(_translate("posting_form", "Credit"))
        self.label.setText(_translate("posting_form", "Charge code"))
        self.label_2.setText(_translate("posting_form", "Description"))
        self.label_3.setText(_translate("posting_form", "Amount"))

