# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pms_users.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_pms_users(object):
    def setupUi(self, pms_users):
        pms_users.setObjectName("pms_users")
        pms_users.resize(338, 428)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        pms_users.setWindowIcon(icon)
        self.layoutWidget = QtWidgets.QWidget(pms_users)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 322, 411))
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.username = QtWidgets.QLineEdit(self.layoutWidget)
        self.username.setObjectName("username")
        self.gridLayout.addWidget(self.username, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.password1 = QtWidgets.QLineEdit(self.layoutWidget)
        self.password1.setObjectName("password1")
        self.gridLayout.addWidget(self.password1, 1, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.password2 = QtWidgets.QLineEdit(self.layoutWidget)
        self.password2.setObjectName("password2")
        self.gridLayout.addWidget(self.password2, 2, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.layoutWidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.user_role_cbo = QtWidgets.QComboBox(self.layoutWidget)
        self.user_role_cbo.setObjectName("user_role_cbo")
        self.gridLayout.addWidget(self.user_role_cbo, 3, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.user_list = QtWidgets.QTableView(self.layoutWidget)
        self.user_list.setObjectName("user_list")
        self.verticalLayout.addWidget(self.user_list)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.add = QtWidgets.QPushButton(self.layoutWidget)
        self.add.setObjectName("add")
        self.gridLayout_2.addWidget(self.add, 0, 0, 1, 1)
        self.change = QtWidgets.QPushButton(self.layoutWidget)
        self.change.setObjectName("change")
        self.gridLayout_2.addWidget(self.change, 0, 1, 1, 1)
        self.delete_2 = QtWidgets.QPushButton(self.layoutWidget)
        self.delete_2.setObjectName("delete_2")
        self.gridLayout_2.addWidget(self.delete_2, 0, 2, 1, 1)
        self.cancel = QtWidgets.QPushButton(self.layoutWidget)
        self.cancel.setObjectName("cancel")
        self.gridLayout_2.addWidget(self.cancel, 0, 3, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout_2)

        self.retranslateUi(pms_users)
        QtCore.QMetaObject.connectSlotsByName(pms_users)

    def retranslateUi(self, pms_users):
        _translate = QtCore.QCoreApplication.translate
        pms_users.setWindowTitle(_translate("pms_users", "Manage users"))
        self.label.setText(_translate("pms_users", "Username"))
        self.label_2.setText(_translate("pms_users", "Password"))
        self.label_3.setText(_translate("pms_users", "Retype password"))
        self.label_4.setText(_translate("pms_users", "Role"))
        self.add.setText(_translate("pms_users", "Add"))
        self.change.setText(_translate("pms_users", "Change"))
        self.delete_2.setText(_translate("pms_users", "Delete"))
        self.cancel.setText(_translate("pms_users", "Cancel"))

import pms_rc
