# -*- coding: utf-8 -*-
from reportlab.lib.pagesizes import A6, A4, landscape, portrait, cm
from reportlab.platypus import SimpleDocTemplate, BaseDocTemplate, Table, TableStyle, Paragraph, PageBreak, Frame, PageTemplate, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER, TA_RIGHT

from time import sleep
import sys
import os
import subprocess
import random
import platform
from datetime import datetime, timedelta, time


if sys.platform == 'linux':
    import cups
    conn = cups.Connection()
    printers = conn.getPrinters ()
    for printer in printers:
        print(printer, printers[printer]["device-uri"])


class Key_card():
    def __init__(self, start_date=datetime.now(),
                 room="1",
                 end_date=datetime.now()+timedelta(days=1),
                 password = "999999"):
        c_height, c_width = A6
        
        if password == "999999":
            password = str(random.randint(10000,99999))
        uid="Room" + room
       
        
        
        f = open(os.path.expanduser("~/passcard.txt"), "w")
        f.write("Configure terminal\n")
        f.write("guest-user ")
        f.write(uid + " ")
        f.write(password + " ")
        f.write('"' + start_date.strftime('%m/%d/%Y %H:%M:%S') + '" ')
        f.write('"' + end_date.strftime('%m/%d/%Y %H:%M:%S') + '"\n')
        f.write("exit\n")
        f.write("exit\n")
        f.close()
    
        a = subprocess.check_output([os.path.expanduser("~\plink.exe"), "-ssh",  "-pw", "pr10ry", "admin@192.168.0.200", "<",  os.path.expanduser("~\passcard.txt")], shell=True)
        
        #run_cmd = os.path.expanduser("~\plink.exe") + " -ssh -pw pr10ry admin@192.168.0.200 < " + os.path.expanduser("~\passcard.txt")
        #os.system(run_cmd)
        
        my_frame = Frame(0, 0, c_height, c_width, id='my_frame')
        my_template = PageTemplate(id='my_template', frames=[my_frame])
        stylesheet = getSampleStyleSheet()
        doc = SimpleDocTemplate(os.path.expanduser("~\key_card.pdf"), pagesize=landscape(A6), rightMargin=30,leftMargin=30,topMargin=30, bottomMargin=18)
       
       
        roomno = "Room" + room
        w_text = []
        w_text.append(Paragraph("To use the wireless broadband, select the priory_guest network, open up your browser and enter the following:", stylesheet["BodyText"]))
        w_text.append(Paragraph("User id: <b>" + roomno + "</b>", stylesheet["BodyText"]))
        w_text.append(Paragraph("Password: <b>" + password + "</b>", stylesheet["BodyText"]))
        w_text.append(Paragraph("", stylesheet["BodyText"]))
        w_text.append(Paragraph("<b>The User id is case sensitive!</b>", stylesheet["BodyText"]))
        

        message_text = []
        message_text.append(Paragraph("Our current menus including today's specials can be seen at www.theprioryinn.co.uk.", stylesheet["BodyText"]))
        message_text.append(Paragraph("Our restaurant can get very busy so pick up your phone and dial 0 to make a booking to avoid disappointment.",stylesheet["BodyText"])) 
        message_text.append(Paragraph("Your shower has a safety device to prevent scalding.  Please call reception if you are not getting sufficiently hot water.", stylesheet["BodyText"]))
        message_text .append(Paragraph("Please do not hesitate to call us if there is anything we can do to make your stay more comfortable.",stylesheet["BodyText"]))
        
        #back_page = []
        #back_page.append(Paragraph("Your room rate may or may not include breakfast"))

        elements=[]
        table_data = [(w_text, message_text)]
        the_table = Table(table_data)
        the_table.setStyle(TableStyle([('VALIGN',(0,0),(-1,-1),'MIDDLE'), ('HALIGN',(0,0),(-1,-1),'MIDDLE')]))
        elements.append(the_table)
        elements.append(PageBreak())
        

        #Cover page
        stylesheet.add(ParagraphStyle(name='centered', alignment=TA_CENTER, fontsize=14))
        
        cover_data = []
        logo= "logo.jpg"
        im = Image(logo, 3*cm, 1.258*cm)
        cover_data.append(im)
        cover_data.append(Paragraph("Room number: " + room, stylesheet["centered"], ))
        left_cell = []
        left_cell.append(Paragraph("The Duty Manager is available 24 hours a day.", stylesheet["BodyText"]))
        left_cell.append(Paragraph("There is an emergency telephone to contact the Duty Manager outside to the left of "
                                   "the reception doors should you get locked out.  Simply lift the phone and it will ring "
                                   "the Duty manager.", stylesheet["BodyText"]))
        
        
        table_data1 = [(left_cell, cover_data)]
        the_table1 = Table(table_data1)
        the_table1.setStyle(TableStyle([('VALIGN',(0,0),(-1,-1),'MIDDLE'), ('ALIGN',(0,0),(-1,-1),'CENTRE'), ('FONTSIZE', (0,0), (-1,-1), 12),]))
        
        elements.append(the_table1)
        
        doc.build(elements)

        #a = subprocess.check_output(["gsprint.exe", "-printer", '"Oki_A6-1"', "-duplex_horizontal",  os.path.expanduser("~\key_card.pdf")], shell=True)
        
        run_cmd = 'gsprint.exe -printer "OKI_A6-1" -duplex_horizontal ' + os.path.expanduser("~\key_card.pdf")
        os.system(run_cmd)

class Key_card1():
    def __init__(self, start_date=datetime.now(),
                 room="1",
                 end_date=datetime.now()+timedelta(days=1),
                 password = "999999"):
        c_height, c_width = A6
        
        if password == "999999":
            password = str(random.randint(10000,99999))
        uid="Room" + room
       
        
        
        f = open(os.path.expanduser("~/passcard.txt"), "w")
        f.write("Configure terminal\n")
        f.write("guest-user ")
        f.write(uid + " ")
        f.write(password + " ")
        f.write('"' + start_date.strftime('%m/%d/%Y %H:%M:%S') + '" ')
        f.write('"' + end_date.strftime('%m/%d/%Y %H:%M:%S') + '"\n')
        f.write("exit\n")
        f.write("exit\n")
        f.close()
    
        a = subprocess.check_output([os.path.expanduser("~\plink.exe"), "-ssh",  "-pw", "pr10ry", "admin@192.168.0.200", "<",  os.path.expanduser("~\passcard.txt")], shell=True)
        
        #run_cmd = os.path.expanduser("~\plink.exe") + " -ssh -pw pr10ry admin@192.168.0.200 < " + os.path.expanduser("~\passcard.txt")
        #os.system(run_cmd)
        
        my_frame = Frame(0, 0, c_height, c_width, id='my_frame')
        my_template = PageTemplate(id='my_template', frames=[my_frame])
        stylesheet = getSampleStyleSheet()
        doc = SimpleDocTemplate(os.path.expanduser("~\key_card.pdf"), pagesize=landscape(A6), rightMargin=30,leftMargin=30,topMargin=30, bottomMargin=18)
       
       
        roomno = "Room" + room
        w_text = []
        w_text.append(Paragraph("To use the wireless broadband, select the priory_guest network, open up your browser and enter the following:", stylesheet["BodyText"]))
        w_text.append(Paragraph("User id: <b>" + roomno + "</b>", stylesheet["BodyText"]))
        w_text.append(Paragraph("Password: <b>" + password + "</b>", stylesheet["BodyText"]))

        message_text = []
        message_text.append(Paragraph("Our current menus including today's specials can be seen at www.theprioryinn.co.uk.", stylesheet["BodyText"]))
        message_text.append(Paragraph("Our restaurant can get very busy so pick up your phone and dial 0 to make a booking to avoid disappointment.",stylesheet["BodyText"])) 
        message_text.append(Paragraph("Your shower has a safety device to prevent scalding.  Please call reception if you are not getting sufficiently hot water.", stylesheet["BodyText"]))
        message_text .append(Paragraph("Please do not hesitate to call us if there is anything we can do to make your stay more comfortable.",stylesheet["BodyText"]))


        elements=[]
        table_data = [(w_text, message_text)]
        the_table = Table(table_data)
        the_table.setStyle(TableStyle([('VALIGN',(0,0),(-1,-1),'MIDDLE'), ('HALIGN',(0,0),(-1,-1),'MIDDLE')]))
        elements.append(the_table)
        elements.append(PageBreak())
        

        #Cover page
        stylesheet.add(ParagraphStyle(name='centered', alignment=TA_CENTER, fontsize=14))
        
        cover_data = []
        logo= "logo.jpg"
        im = Image(logo, 3*cm, 1.258*cm)
        cover_data.append(im)
        cover_data.append(Paragraph("Room number: " + room, stylesheet["centered"], ))
        left_cell = []
        left_cell.append(Paragraph("", stylesheet["BodyText"]))
        
        
        table_data1 = [(left_cell, cover_data)]
        the_table1 = Table(table_data1)
        the_table1.setStyle(TableStyle([('VALIGN',(0,0),(-1,-1),'MIDDLE'), ('ALIGN',(0,0),(-1,-1),'CENTRE'), ('FONTSIZE', (0,0), (-1,-1), 12),]))
        
        elements.append(the_table1)
        
        doc.build(elements)

        #a = subprocess.check_output(["gsprint.exe", "-printer", '"Oki_A6-1"', "-duplex_horizontal",  os.path.expanduser("~\key_card.pdf")], shell=True)
        
        run_cmd = 'gsprint.exe -printer "Oki_A6-1" -duplex_horizontal ' + os.path.expanduser("~\key_card.pdf")
        os.system(run_cmd)

class Reg_card():
    def __init__(self, guest_details = [], room_no="1", start_date=datetime.now(), end_date=datetime.now(), adults=0, kids=0, email="", room_only=False):
            doc = SimpleDocTemplate("regcard.pdf", pagesize=portrait(A4))
            underline = "____________________________________________________________"
            space=12
            
            if email == "": 
                email = underline
                
            if len(guest_details) > 0:
                while (guest_details[-1] == ""):
                    guest_details.pop()
            print_style = "Normal"    
            if len(guest_details) < 2:
                print_style = "BodyText"
                for x in range(3):
                    guest_details.append(underline)
            
            story=[]
            logo= "logo.jpg"
            im = Image(logo, 6*cm, 2.516*cm)
            story.append(im)
            story.append(Spacer(1,space))
            story.append(Spacer(1,space))
            styles=getSampleStyleSheet()
            styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
            story.append(Paragraph("<b>Guest registration form</b>", styles["centered"]))
            story.append(Spacer(1,space))
            story.append(Spacer(1,space))
            
            
            r_text = [Paragraph(item, styles[print_style]) for item in guest_details]
           
            
            
            table_data = [["Guest details:", r_text],
                          ["Room number:", room_no],
                          ["Arriving:", start_date],
                          ["Departing:", end_date],
                          ["Adults:", adults],
                          ["Children:", kids],
                          ["Email address:", email],
                          ["Car registration:", underline],
                          ["Newspapers required:", underline],
                          ["Alarm call required:", underline]]
            the_table = Table(table_data, hAlign='LEFT', colWidths=(107,450))
            the_table.setStyle(TableStyle([('ALIGN', (0,0), (0,9), 'RIGHT'),]))
            the_table.setStyle(TableStyle([('VALIGN',(0,0),(0,0),'TOP')]))
            
            story.append(the_table)
            
            story.append(Spacer(1,space))
            story.append(Spacer(1,space))

            story.append(Paragraph("This information may be used to tailor our services and to send you "
            "information on offers that you may appreciate in the future.  "
            "If you do not wish to recieve such offers please tick here. [  ]", styles["BodyText"]))
            
            
            story.append(Paragraph("I hereby authorise The Priory Inn to apply any authorised expenses to my credit/debit card.  "
                                   "I understand that my room is non-smoking.  "
                                   "If smoking has occured in my room during my stay I agree to pay an "
                                   "additional £25.00 cleaning charge.", styles["BodyText"]))
            
            story.append(Paragraph("GUEST SIGNATURE______________________________________________________________", styles["BodyText"]))
            
            story.append(Paragraph("Please remember to take your key with you if you will be returning to the hotel after 11:00pm.  "
                                   "Our bar closes strictly at 11:00pm for both non-residents and residents!  "
                                   "All our rooms are non-smoking."
                                   , styles["BodyText"]))
            

            if room_only:
                story.append(Paragraph("Your rate does not include breakfast.  However, we would be delighted to look after you in the morning.  "
                                       "We will set up a table for you and you will be able to order from our farm-fresh a la carte menu.  "
                                       "Your breakfast will then be charged to your room."
                                       , styles["BodyText"]))

            
            doc.build(story)
            
        
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "regcard.pdf"], shell=True)
            
            
class Comment_card():
    def __init__(self, room_no='1', booking_id='999999'):
        doc = SimpleDocTemplate("comment_card.pdf", topMargin=15, pagesize=portrait(A4))
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER, fontsize=14))
        styles.add(ParagraphStyle(name='righted', alignment=TA_RIGHT, fontsize=8))
        underline = "____________________________________________________________"
        space = 10
        
        
        story=[]
        logo= "logo.jpg"
        im = Image(logo, 3*cm, 1.258*cm)
        story.append(im)
        story.append(Spacer(1,space))
        
        
         
        story.append(Paragraph("At The Priory Inn, we care deeply about your impressions "
                               "of the comfort and standards of our hotel rooms.  We would "
                               "be very grateful if you took a few moments to give us your feedback on how you enjoyed your stay."
                               , styles["BodyText"]))
        story.append(Paragraph("We will use your information in confidence to improve our hotel. You can hand this to reception, "
                               "drop into the box on your way out or post it to me. If there is anything you think we should "
                               "act on immediately, please let our Duty Manager know as soon as possible."
                               , styles["BodyText"]))
        story.append(Paragraph("Our Duty Manager can be contacted 24 hours a day by dialling 0 from your room telephone.", styles["BodyText"]))
        story.append(Paragraph("~"*60, styles["centered"]))
        story.append(Spacer(1,space))
        
        story.append(Paragraph("<b>I have stayed at The Priory Inn (please circle one)</b>", styles["BodyText"]))
        table_data = ["For the first time", "for the second or third time", "For four or more times"],
        the_table = Table(table_data, colWidths=150)
        story.append(the_table)
        
        story.append(Paragraph("<b>The purpose of my stay was:</b>", styles["BodyText"]))
        table_data = ["Attend a wedding", "Business", "Pleasure", "Other"],
        the_table = Table(table_data, colWidths=100)
        story.append(the_table)
        story.append(Paragraph("_"*79, styles["centered"]))
        
        story.append(Paragraph("<b>When I made the booking, the reservation was handled competently and politely</b>", styles["BodyText"]))
        table_data = [["Strongly", " ", " ", " ", "Strongly", "Don't know"],
                      ["Disagree", " ", " ", " ", "Agree", "or N/A"],
                      ["1", "2", "3", "4", "5", " "]]
        the_table = Table(table_data, colWidths=75, rowHeights=(20, 10, 15))
        the_table.setStyle(TableStyle([('ALIGN',(0,0),(-1,-1),'CENTRE'),]))
        story.append(the_table)
        story.append(Paragraph("Comments" + "_"*70, styles["BodyText"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Spacer(1,space))
         
        story.append(Paragraph("<b>On check-in, I was warmly welcomed, efficiently checked-in and well informed of the services The Priory Inn offers.</b>", styles["BodyText"]))
        story.append(the_table)
        story.append(Paragraph("Comments" + "_"*70, styles["BodyText"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Spacer(1,space))
 
        story.append(Paragraph("<b>During my stay, I found my room to be clean and well maintained.</b>", styles["BodyText"]))
        story.append(the_table)
        story.append(Paragraph("Comments" + "_"*70, styles["BodyText"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Spacer(1,space))
 
        story.append(Paragraph("<b>For breakfast, the quality and presentation of food and service for was excellent.</b>", styles["BodyText"]))
        story.append(the_table)
        story.append(Paragraph("Comments" + "_"*70, styles["BodyText"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Spacer(1,space))
 
        story.append(Paragraph("<B>PLEASE TURN OVER</B>", styles["centered"]))
        story.append(PageBreak())
        
        
        story.append(im)
        story.append(Spacer(1,space))
 
        story.append(Paragraph("<b>On checking out, my bill was handled well and my final impressions were positive.</b>", styles["BodyText"]))
        story.append(the_table)
        story.append(Paragraph("Comments" + "_"*70, styles["BodyText"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Spacer(1,space))
       
        story.append(Paragraph("<b>The prices and room rates represent good value for money.</b>", styles["BodyText"]))
        story.append(the_table)
        story.append(Paragraph("Comments" + "_"*70, styles["BodyText"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Spacer(1,space))

        story.append(Paragraph("<b>I will recommend The Priory Inn to my colleagues, friends and family.</b>", styles["BodyText"]))
        story.append(the_table)
        story.append(Paragraph("Comments" + "_"*70, styles["BodyText"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Spacer(1,space))
        
        story.append(Paragraph("<b>Please expand on any of the above or make any further comments here:</b>", styles["BodyText"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Paragraph("_"*79, styles["centered"]))
        story.append(Spacer(1,space))

        story.append(Paragraph("I would be happy to be contacted to discuss my experience today: [ ]", styles["BodyText"]))
        
        story.append(Paragraph("Please <b>DO NOT</b> contact me with events and promotions: [ ]", styles["BodyText"]))
        
        sp_style = ParagraphStyle('small_print')
        sp_style.fontSize = 8
        story.append(Paragraph("(We will not divulge your details to any third party and we only contact you with special offers maximum once a quarter)", sp_style))
        
        underline = "_"*58
        table_data = [["Name:", underline],
                 ["Address:", underline],
                 [" ", underline],
                 [" ", underline],
                 ["Post code:", underline],
                 ["Email:", underline],
                 ["Phone number:", underline]]
        the_table = Table(table_data, hAlign='LEFT', colWidths=(107,450))
        
        story.append(Spacer(1,space))
        story.append(the_table)
        
        story.append(Spacer(1,space))
        story.append(Paragraph("Thank you very much!", styles["centered"]))
        
        story.append(Spacer(1,space))
        story.append(Spacer(1,space))
  
        r_style = ParagraphStyle('small_print')
        r_style.fontSize = 8
        r_style.alignment = TA_RIGHT
        
        if len(room_no)<2:
            room_no = "0" + room_no
  
        story.append(Paragraph(booking_id + room_no, r_style))
        

        doc.build(story)
         
     
        a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-duplex_vertical", "comment_card.pdf"], shell=True)
      

            
if __name__ == '__main__':
    a = Reg_card()