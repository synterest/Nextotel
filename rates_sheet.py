# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rates_sheet.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_rates_sheet(object):
    def setupUi(self, rates_sheet):
        rates_sheet.setObjectName("rates_sheet")
        rates_sheet.resize(467, 987)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        rates_sheet.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(rates_sheet)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.publish_avail = QtWidgets.QPushButton(rates_sheet)
        self.publish_avail.setObjectName("publish_avail")
        self.gridLayout.addWidget(self.publish_avail, 0, 1, 1, 1)
        self.label = QtWidgets.QLabel(rates_sheet)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 2, 1, 1)
        self.num_days = QtWidgets.QSpinBox(rates_sheet)
        self.num_days.setObjectName("num_days")
        self.gridLayout.addWidget(self.num_days, 0, 3, 1, 1)
        self.get_avail = QtWidgets.QPushButton(rates_sheet)
        self.get_avail.setObjectName("get_avail")
        self.gridLayout.addWidget(self.get_avail, 0, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.rates_grid = QtWidgets.QTableView(rates_sheet)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.rates_grid.sizePolicy().hasHeightForWidth())
        self.rates_grid.setSizePolicy(sizePolicy)
        self.rates_grid.setMaximumSize(QtCore.QSize(1677721, 16777215))
        self.rates_grid.setObjectName("rates_grid")
        self.rates_grid.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.rates_grid)

        self.retranslateUi(rates_sheet)
        QtCore.QMetaObject.connectSlotsByName(rates_sheet)
        rates_sheet.setTabOrder(self.num_days, self.publish_avail)
        rates_sheet.setTabOrder(self.publish_avail, self.rates_grid)
        rates_sheet.setTabOrder(self.rates_grid, self.get_avail)

    def retranslateUi(self, rates_sheet):
        _translate = QtCore.QCoreApplication.translate
        rates_sheet.setWindowTitle(_translate("rates_sheet", "Rates sheet"))
        self.publish_avail.setText(_translate("rates_sheet", "Publish rates"))
        self.label.setText(_translate("rates_sheet", "Days to show"))
        self.get_avail.setText(_translate("rates_sheet", "Get availability"))

import pms_rc
