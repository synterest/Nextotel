# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'configurations.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_configuration_form(object):
    def setupUi(self, configuration_form):
        configuration_form.setObjectName("configuration_form")
        configuration_form.resize(581, 81)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        configuration_form.setWindowIcon(icon)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(configuration_form)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(configuration_form)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.configuration = QtWidgets.QLineEdit(configuration_form)
        self.configuration.setObjectName("configuration")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.configuration)
        self.verticalLayout.addLayout(self.formLayout)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.btn_beginning = QtWidgets.QPushButton(configuration_form)
        self.btn_beginning.setObjectName("btn_beginning")
        self.gridLayout.addWidget(self.btn_beginning, 0, 0, 1, 1)
        self.btn_previous = QtWidgets.QPushButton(configuration_form)
        self.btn_previous.setObjectName("btn_previous")
        self.gridLayout.addWidget(self.btn_previous, 0, 1, 1, 1)
        self.btn_next = QtWidgets.QPushButton(configuration_form)
        self.btn_next.setObjectName("btn_next")
        self.gridLayout.addWidget(self.btn_next, 0, 3, 1, 1)
        self.btn_end = QtWidgets.QPushButton(configuration_form)
        self.btn_end.setObjectName("btn_end")
        self.gridLayout.addWidget(self.btn_end, 0, 4, 1, 1)
        self.current_index = QtWidgets.QLineEdit(configuration_form)
        self.current_index.setAlignment(QtCore.Qt.AlignCenter)
        self.current_index.setObjectName("current_index")
        self.gridLayout.addWidget(self.current_index, 0, 2, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.btn_new = QtWidgets.QPushButton(configuration_form)
        self.btn_new.setObjectName("btn_new")
        self.gridLayout_2.addWidget(self.btn_new, 0, 0, 1, 1)
        self.btn_delete = QtWidgets.QPushButton(configuration_form)
        self.btn_delete.setObjectName("btn_delete")
        self.gridLayout_2.addWidget(self.btn_delete, 1, 0, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout_2)
        self.horizontalLayout_2.addLayout(self.horizontalLayout)

        self.retranslateUi(configuration_form)
        QtCore.QMetaObject.connectSlotsByName(configuration_form)

    def retranslateUi(self, configuration_form):
        _translate = QtCore.QCoreApplication.translate
        configuration_form.setWindowTitle(_translate("configuration_form", "Room configurations"))
        self.label.setText(_translate("configuration_form", "Configuration"))
        self.btn_beginning.setText(_translate("configuration_form", "<|"))
        self.btn_previous.setText(_translate("configuration_form", "<"))
        self.btn_next.setText(_translate("configuration_form", ">"))
        self.btn_end.setText(_translate("configuration_form", ">|"))
        self.btn_new.setText(_translate("configuration_form", "New"))
        self.btn_delete.setText(_translate("configuration_form", "Delete"))

import pms_rc
