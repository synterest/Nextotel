# -*- coding: utf-8 -*-

# Created By: Dave Kelly
# Copyright 2018 Synterest Limited (www.synterest.co.uk)
#
# This software is licensed under the "GPLv3" License as described in the "LICENSE" file,
# which should be included with this package. The terms are also available at
# http://www.gnu.org/licenses/gpl-3.0.html

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtSql import *
from PyQt5.QtWidgets import *
import sys

app=QApplication(sys.argv)

splash_pix = QPixmap('Nextotel.png')
splash = QSplashScreen(splash_pix)
splash.setWindowFlags(Qt.FramelessWindowHint)
splash.setEnabled(False)
splash.show()
splash.showMessage("<p>Licensed under GPL V3<br/>Copyright 2018 Synterest Limited<br/>"
                   "See Help:About for more details.<br/>Version 0.8<br/><br/></p>", 
                   Qt.AlignTop | Qt.AlignCenter, Qt.black)


#Reports modules
from PollyReports import *
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from textwrap import fill

#database modules
import psycopg2
import psycopg2.extras

#General modules
from datetime import datetime, timedelta, time
import time
import os
import subprocess
import csv
import random
import webbrowser
if sys.platform == 'win32':
    import win32com.client
import yagmail


#form modules
from ci_co_table import *
from guest_info_form8 import *
from posting5 import *
from current_accounts import *
from check_out_form import *
from pms_users import *
from login import *
from email_box import *
from booking_find import *
from rates_sheet import *
from newspapers import *
from room_chart1 import *
from rate_select import *
from date_dialog import *
from companies import *
from guest_correspondence import *
from rate_bulk_edit import *
from guests import *
from rooms import *
from room_types import *
from configurations import *
from charge_codes import *
from about import *
from book_history import *
from rooming_list import *

#pms specific modules
from config import *
import pms_reports
import printed_forms
import update_availability


BOOKING_ID = 0
CHECKED_IN = 1
CHECKED_OUT = 2
HOTEL_ID = 3
ID = 4
LABEL = 5
DEPARTURE_DATE = 6
ARRIVAL_DATE = 7
NUM_ADULTS = 8
NUM_CHILDREN = 9
ROOM_ID = 10
ROOM_TYPE_ID = 11
ADDRESS = 12
REFERENCE = 13
NOTES = 14
EMAIL = 15
CORPORATION_ID = 16
CANCELLED = 17
ADD = 18
POSTCODE = 19
COUNTRY = 20
COMPANY_ID = 21
LEDGER_ID = 22
FOLIO1_CHECKED_OUT = 23
FOLIO2_CHECKED_OUT = 24
FOLIO3_CHECKED_OUT = 25
FOLIO4_CHECKED_OUT = 26
TELEPHONE = 27
CHANNEL_REF = 28
BOOKER_REF = 29
BOOKED_AT = 30
VOUCHER = 31
NEWSPAPER = 32
ROOM_TYPE = 33
ROOM = 34
GUEST_ID = 35
BABIES = 36
ROOM_MOVES = 37

user = ''
hotdate = None
hotdate_string = ''




class Ci_Co(QMainWindow):
    """Check in and check out module"""

    def __init__(self, parent=None):
        global hotdate
        global hotdate_string
        global folio_num
        QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        qry_params = QSqlQuery()
        qry_params.exec_("select param_date from params where id = 1")
        param_date = 0
        while qry_params.next():
            hotdate_string  = qry_params.value(param_date).toString()
            hotdate = qry_params.value(param_date)
            print(hotdate_string)

        self.ui.hotdate_lbl.setText(hotdate.toString("dddd dd/MM/yyyy"))
        self.populate_mainform()
        self.arrivals_view.clicked.connect(self.ci_clicked)
        self.in_house_view.clicked.connect(self.ih_clicked)
        self.departures_view.clicked.connect(self.dd_clicked)
        self.departed_view.clicked.connect(self.co_clicked)
        self.ui.actionUpdate_days_business.triggered.connect(self.nightly_update)
        self.ui.actionManage_users.triggered.connect(self.manage_users)
        self.ui.actionCancellations.triggered.connect(lambda: self.cancellations())
        self.ui.actionDeposits.triggered.connect(lambda: self.deposits())
        self.ui.actionGuest_history.triggered.connect(lambda: self.guest_history())
        self.ui.actionArrivals.triggered.connect(self.arrivals_report)
        self.ui.actionBoard_Basis.triggered.connect(self.board_basis_report)
        self.ui.actionBackup.triggered.connect(self.backup_db)
        self.ui.actionQuick_rates.triggered.connect(self.rates_sheet)
        self.ui.actionCash_up.triggered.connect(self.cash_up_report)
        self.ui.actionRevenue.triggered.connect(self.revenue_report)
        self.ui.actionTransaction_detail.triggered.connect(self.transaction_detail_report)
        self.ui.actionBreakfast_list.triggered.connect(self.breakfast_list)
        self.ui.actionNewspaper_prices.triggered.connect(self.newspaper_prices)
        self.ui.actionReservations_made.triggered.connect(self.reservations_made)
        self.ui.actionRooms_chart.triggered.connect(self.rooms_chart)
        self.ui.actionRegCardsKeyCards.triggered.connect(self.reg_key_cards)
        self.ui.actionRegCardsKeyCards_2.triggered.connect(self.reg_key_cards)
        self.ui.actionComment_cards.triggered.connect(self.comment_cards)
        self.ui.actionHousekeeping_list.triggered.connect(self.housekeeping_list)
        self.ui.actionAll_housekeeping_reports.triggered.connect(self.all_housekeeping_rpts)
        self.ui.actionSettlements.triggered.connect(self.settlements)
        self.ui.actionIn_House.triggered.connect(self.rgl_report)
        self.ui.actionDeposits_3.triggered.connect(self.deposits_report)
        self.ui.actionGuest_correspondence.triggered.connect(self.guest_correspondence)
        self.ui.actionBulk_edit.triggered.connect(self.bulk_rates)
        self.ui.actionSales_ledger_invoices.triggered.connect(self.sales_ledger_invoices)
        self.ui.actionRe_sync.triggered.connect(self.resync_avail)
        self.ui.actionRe_sync_rates.triggered.connect(self.resync_rates)
        self.ui.actionBook.triggered.connect(self.book)
        self.ui.actionRooms_2.triggered.connect(self.setup_rooms)
        self.ui.actionAbout.triggered.connect(self.about)
        self.ui.actionRoom_types_2.triggered.connect(self.setup_room_types)
        self.ui.actionRoom_configurations_2.triggered.connect(self.setup_room_configurations)
        self.ui.actionCharge_codes.triggered.connect(self.setup_charge_codes)
        self.ui.actionFuture_bookings_with_no_rate.triggered.connect(self.future_no_rate)
        self.ui.actionFuture_bookings_with_low_rates.triggered.connect(self.future_low_rate)


        self.set_status_bar()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.populate_mainform)
        self.timer.start(60000) 

        self.setStyleSheet("background-color: " + bg_colour)

    def future_no_rate(self):
        rpt = pms_reports.Bookings_without_rates(user=user, 
                                        pword=pword, 
                                        database=database, 
                                        server=server, 
                                        trans_date=hotdate.toPyDate())    

    
    def future_low_rate(self):
        rpt = pms_reports.Low_rate_bookings(user=user, 
                                pword=pword, 
                                database=database, 
                                server=server, 
                                trans_date=hotdate.toPyDate())    

    
    def about(self):
        a_file = open('GPL v3.txt', encoding='utf-8')
        the_text = a_file.read()
        a_file.close()
        amount_message = About()
        amount_message.ui.license_text.setReadOnly(True)
        amount_message.ui.license_text.setPlainText(the_text)
        amount_message.exec_()

    def manage_users(self):
        self.user_form = User_management_form(self)
        self.user_form.show()
    
    def setup_charge_codes(self):
        setup_codes = Setup_charge_codes()
        setup_codes.show()
    
    def setup_room_configurations(self):
        setup_confs = Setup_room_configurations()
        setup_confs.show()

    def setup_room_types(self):
        setup_room_types = Setup_room_types()
        setup_room_types.show()

    def setup_rooms(self):
        setup_rooms = Setup_rooms()
        setup_rooms.show()


    def book(self):
        new_res = New_reservation(self, None)


    def resync_rates(self):
        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        if a.exec_():
            start_date = a.ui.start_date.date().toPyDate()
            end_date = a.ui.end_date.date().toPyDate()

        else:
            return

        query = QSqlQuery()
        SQL = ("INSERT INTO updateinternetrates "
               "(availdate, roomtype, adults, rateamt, mlos, rate_code) "
               "SELECT avail_date, room_type, adults, rate, mlos, rate_code "
               "FROM rates_daily "
               "WHERE avail_date >= ? "
               "AND avail_date <= ?")
        query.prepare(SQL)
        query.addBindValue(start_date.strftime('%Y-%m-%d'))
        query.addBindValue(end_date.strftime('%Y-%m-%d'))
        query.exec_()
        if not query.isActive():
            print(query.lastError().text())
            QMessageBox.critical(self, "Re-sync Error! \n", query.lastError().text())
        else:
            QMessageBox.information(self, "Re-sync...", "Re-sync of rates started!")



    def resync_avail(self):

        a = Date_dialog()
        if a.exec_():
            start_date = a.ui.start_date.date().toPyDate()
            end_date = a.ui.end_date.date().toPyDate()

        else: 
            return

        SQL = ("INSERT INTO availability_update (avail_date) "
               "SELECT THE_DATE "
               "FROM generate_series(?::date, ?::date, interval '1 day') the_date")
        query = QSqlQuery()
        query.prepare(SQL)
        query.addBindValue(start_date.strftime('%Y-%m-%d'))
        query.addBindValue(end_date.strftime('%Y-%m-%d'))
        query.exec_()
        if not query.isActive():
            print(query.lastError().text())
            QMessageBox.critical(self, "Re-sync Error! \n", query.lastError().text())
        else:
            QMessageBox.information(self, "Re-sync...", "Re-sync of availability started!")





    def sales_ledger_invoices(self):

        a = Date_dialog()
        if a.exec_():
            start_date = a.ui.start_date.date().toPyDate()
            end_date = a.ui.end_date.date().toPyDate()


            rpt = pms_reports.Sales_ledger_invoices(user=user, 
                                                    pword=pword, 
                                                    database=database, 
                                                    server=server, 
                                                    trans_date=start_date)    



    def bulk_rates(self):
        self.rates_form = Bulk_rates(self)
        self.rates_form.show()        

    def all_housekeeping_rpts(self):
        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            dep_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return
        
        #dep_date, ok = DateDialog(title="Housekeeping").getDateTime()
        #dep_date = dep_date.toPyDate()
        #if not ok:
            #return
        rpt = pms_reports.Housekeeping_checklist(dep_date)
        rpt = pms_reports.Housekeeping(user=user, 
                                       pword=pword, 
                                       server=server, 
                                       trans_date=dep_date)
        rpt = pms_reports.Deep_cleans(user=user, 
                                      pword=pword,
                                     server=server,
                                     trans_date=dep_date)
        rpt = pms_reports.Maintenance_dockets()

    def guest_correspondence(self):
        self.guest_corres_form = Guest_correspondence()
        self.guest_corres_form.exec_()

    def housekeeping_list(self):
        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            dep_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return
        
        rpt = pms_reports.Housekeeping(user=user, 
                                       pword=pword, 
                                      server=server, 
                                      trans_date=dep_date)

    def comment_cards(self):        
        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            dep_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return

        departures = QSqlQuery()
        SQL = ("SELECT id, bookings.room "
               "FROM bookings "
               "INNER JOIN rooms on bookings.room = rooms.room "
               "WHERE departure_date = '{0}' and checked_out is NULL "
               "AND cancelled is NULL AND rooms.sleeping = TRUE".format(dep_date))
        departures.exec_(SQL)

        while departures.next():
            a = printed_forms.Comment_card(room_no=departures.value('room'), booking_id=departures.value('id'))



    def reg_key_cards(self):
        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            arr_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return

        arr_date = arr_date.strftime('%Y-%m-%d')

        arrivals = QSqlQuery()
        SQL = ("SELECT bookings_guests.id, arrival_date, label, guests.address, "
               "guests.postcode, guests.country, departure_date, bookings_guests.room, guests.email, num_adults, "
               "num_children, babies "
               "FROM bookings_guests "
               "JOIN guests on bookings_guests.guest_id = guests.id "
               "JOIN rooms on bookings_guests.room = rooms.room "
               "WHERE arrival_date = '{0}' " 
               "AND checked_in IS NULL "
               "AND cancelled IS NULL "
               "AND sleeping IS TRUE "
               "ORDER BY to_number(bookings_guests.room, '9999')"
               ).format(arr_date)
        arrivals.exec_(SQL)

        while arrivals.next():
            
            room = arrivals.value('room')
            start_date = arrivals.value("arrival_date").toPyDate()
            dep_date = arrivals.value("departure_date").toPyDate()
            end_date = datetime.combine(dep_date, datetime.min.time()) + timedelta(hours=13)


            a = printed_forms.Key_card(start_date=start_date, room=room, end_date=end_date, password=arrivals.value("id"))

            guest_details = []
            guest_details.append(arrivals.value("label"))
            for x in arrivals.value("address").splitlines():
                guest_details.append(x)
            guest_details.append(arrivals.value("postcode"))
            guest_details.append(arrivals.value("country"))

            email_add = ""
            if "booking.com" not in arrivals.value("email"):
                if "unknown" not in arrivals.value("email"):
                    if "example.com" not in arrivals.value("email"):
                        email_add = arrivals.value("email")

            ro = QSqlQuery()
            ro.exec_("select * from rates_stay where id_bookings = '{0}' and board_basis = 'RO'".format(arrivals.value("id")))
            room_only = False
            if ro.size() > 0:
                room_only = True


            reg_card = printed_forms.Reg_card(guest_details=guest_details, 
                                              room_no=arrivals.value("room"), 
                                           start_date=arrivals.value("arrival_date").toPyDate().strftime('%d/%m/%Y'), 
                                           end_date=arrivals.value("departure_date").toPyDate().strftime('%d/%m/%Y'), 
                                           adults=arrivals.value("num_adults"), 
                                           kids=arrivals.value("num_children"), 
                                           email=email_add, room_only=room_only)



    def rooms_chart(self):
        self.rc = Rooms_chart1()
        self.rc.showMaximized()

    def rates_sheet(self):
        self.rates_set = Rates_sheet()
        self.rates_set.show()


    def backup_db(self):

        if os.environ['COMPUTERNAME'] == 'MERCURY':
            prgrm = "C:\Program Files (x86)\pgAdmin III\\1.22\pg_dump"
        else:
            prgrm = "C:\Program Files\pgAdmin III\\1.22\pg_dump"
        
        QApplication.setOverrideCursor(Qt.WaitCursor)
        


        usr_flag = "-U" 
        usr = "postgres"
        host_flag = "-h" # don't forget server
        db_flag = "-d" #don't forget database
        format_flag = "-F" 
        format_thing = "c"
        output_file_flag = "-f" 
        output_file = backup_loc + hotdate.toString("dddd") + "-" +database + ".backup"
        tc_output_file  = backup_loc + hotdate.toString("dddd") + "_tc.backup"
        encrypt = ["C:\Program Files\7-Zip\7z.exe", 'a', '-p6TKufcx68awN', output_file, output_file + 'zip']

        pms_backup = [prgrm, usr_flag,  usr, host_flag, server, db_flag, database, format_flag, format_thing, output_file_flag, output_file]
        timeclock_backup = [prgrm, usr_flag, usr, host_flag, "192.168.0.6", db_flag, "time_clock", format_flag, format_thing, output_file_flag, tc_output_file]

        try:
            a = subprocess.check_output(pms_backup, shell=True)
        except subprocess.CalledProcessError as e:
            print("Calledprocerr", e)
            QMessageBox.critical(self, "Backup", "Backup NOT successful!  Please don't ignore this!")
            QApplication.restoreOverrideCursor()
            
        else:
            QApplication.restoreOverrideCursor()
            QMessageBox.information(self, "Backup", "Backup completed successfully to " + output_file)

        try:
            a = subprocess.check_output(timeclock_backup, shell=True)
        except subprocess.CalledProcessError as e:
            print("Calledprocerr", e)
            QMessageBox.critical(self, "Backup", "Timeclock Backup NOT successful!  Please don't ignore this!")
            QApplication.restoreOverrideCursor()
            
        else:
            QApplication.restoreOverrideCursor()
            QMessageBox.information(self, "Backup", "Timeclock backup completed successfully to " + tc_output_file)

    def arrivals_report(self):
        """Print arrivals for a given date"""

        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            arr_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return
        rpt = pms_reports.Arrivals(arrival_date=arr_date, user=user, 
                                   pword=pword, 
                                  database=database, 
                                  server=server)
    def breakfast_list(self):
        """Print breakfast list for a given date"""

        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            arr_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return
        rpt = pms_reports.Breakfast_list(trans_date=arr_date, user=user, 
                                         pword=pword, 
                                  database=database, 
                                  server=server)    

    def cash_up_report(self):
        rpt = pms_reports.Cash_up(user=user, pword=pword, 
                                  database=database, 
                                 server=server)

    def transaction_detail_report(self):


        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            trans_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return

        rpt = pms_reports.Audit_trail(user=user, 
                                      pword=pword, 
                                     database=database, 
                                     server=server, 
                                     trans_date=trans_date)    

    def rgl_report(self):


        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            trans_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return

        rpt = pms_reports.In_house(user=user, 
                                   pword=pword, 
                                     database=database, 
                                     server=server, 
                                     trans_date=trans_date)    
    def deposits_report(self):


        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            trans_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return

        rpt = pms_reports.Deposits_ledger(user=user, 
                                          pword=pword, 
                                     database=database, 
                                     server=server, 
                                     trans_date=trans_date)    


    def revenue_report(self):


        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            trans_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return
        rpt = pms_reports.Revenue(user=user, 
                                  pword=pword, 
                                  database=database, 
                                  server=server, 
                                  trans_date=trans_date)


    def settlements(self):


        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            trans_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return

        rpt = pms_reports.Settlements(user=user, 
                                      pword=pword, 
                                      database=database, 
                                      server=server, 
                                      trans_date=trans_date)


    def reservations_made(self):
        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            trans_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return
        rpt = pms_reports.Res_made(res_made_date=trans_date, user=user, 
                                   pword=pword,
                                  database=database,
                                  server=server)

    def board_basis_report(self):
        """Print a report which shows whether it is B&B or DBB"""
        a = Date_dialog(start_date=hotdate, end_date=hotdate.addDays(365))
        a.ui.end_date.setVisible(False)
        if a.exec_():
            trans_date = a.ui.start_date.date().toPyDate()
            #end_date = a.ui.end_date.date().toPyDate()
        else:
            return
        rpt = pms_reports.Board_basis(run_date=trans_date, user=user, 
                                      pword=pword, 
                                  database=database, 
                                  server=server)

    def nightly_update(self):
        global hotdate
        global hotdate_string

        reply = QMessageBox.question(self, 'Check out',
                                     "This action will post room charges to all checked in rooms and change the hotel date to the next day. Are you sure you wish to continue?", QMessageBox.Yes | 
            QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.No:
            return

        QSqlDatabase.database().transaction()
        not_checked_in = QSqlQuery()
        not_checked_in.exec_("SELECT * FROM bookings where arrival_date <= '{0}' AND checked_in is NULL AND cancelled is NULL".format(hotdate.toString('yyyy-MM-dd')))

        not_checked_in.first()
        if not_checked_in.isValid():
            QMessageBox.critical(None, "Nightly Update", "Not all rooms are checked in!  Please either check-in rooms or cancel reservations.")
            return

        not_allocated = QSqlQuery()
        not_allocated.exec_("SELECT id FROM bookings WHERE checked_in is NOT NULL AND checked_out is NULL AND room is NULL")
        not_allocated.first()
        if not_allocated.isValid():
            QMessageBox.critical(None, "Nightly Update", "Not all bookings are allocated!  Please make sure all rooms are allocated to a room number.  Don't forget to check dummy/managment rooms")
            return


        room_charges = QSqlQuery()
        room_charges.exec_("SELECT "
                           "rates_stay.rate_date, "
                           "rates_stay.amount, "
                           "rates_stay.id_bookings, "
                           "rates_stay.rate_code, "
                           "rates.rate_description, "
                           "bookings.checked_in, "
                           "bookings.checked_out "
                           "FROM (rates_stay INNER JOIN rates ON rates_stay.rate_code = rates.rate_code) INNER JOIN bookings ON rates_stay.id_bookings = bookings.id "
                           "WHERE bookings.checked_in Is Not NULL AND bookings.checked_out Is NULL AND "
                           "rates_stay.rate_date = '{0}'".format(hotdate.toString('yyyy-MM-dd')))

        while room_charges.next():

            non_room_charges = QSqlQuery()
            non_room_charges.exec_("SELECT "
                                   "bookings.id, "
                                   "bookings.num_adults, "
                                   "rates_stay.rate_date, "
                                   "rates_stay.amount, "
                                   "rates_stay.rate_code, "
                                   "rate_components.charge_code, "
                                   "rate_components.amount AS rate_comp_amt, "
                                   "rate_components.notes, "
                                   "rate_components.multiplier "
                                   "FROM " 
                                   "public.rate_components, "
                                   "public.rates_stay, "
                                   "public.bookings "
                                   "WHERE "
                                   "rates_stay.rate_code = rate_components.rate_code AND "
                                   "bookings.id = rates_stay.id_bookings AND "
                                   "rate_components.charge_code <> 'ACCOM' AND "
                                   "rates_stay.rate_date = '{0}' AND "
                                   "id_bookings = '{1}'".format(hotdate.toString('yyyy-MM-dd'), room_charges.value("id_bookings").strip()))

            non_rooms = 0

            #Post non accommodation elements to the charge codes
            while non_room_charges.next():
                if non_room_charges.value("multiplier") == "person":
                    amt_to_post = non_room_charges.value("rate_comp_amt")*non_room_charges.value("num_adults")
                else:
                    amt_to_post = non_room_charges.value("rate_comp_amt")


                if amt_to_post < 0:
                    amt_to_post = 0

                non_rooms = non_rooms + amt_to_post

                #figure out the tax
                tax_code = QSqlQuery()
                tax_code.exec_("SELECT tax_code from charge_codes WHERE charge_code = '{0}'".format(non_room_charges.value("charge_code")))
                tax_code.first()
                if not tax_code.isActive():
                    tax = taxes["T1"]
                else:
                    tax = taxes[tax_code.value(0)]+1.0

                query = QSqlQuery()
                query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, folio, account, hotdate, ledger_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
                t = datetime.now()
                trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
                query.addBindValue(trans_time)
                query.addBindValue(user)
                query.addBindValue(non_room_charges.value("id").strip())
                query.addBindValue("Autopost: " + non_room_charges.value("notes"))
                query.addBindValue(amt_to_post)
                query.addBindValue(amt_to_post-(amt_to_post/tax))
                query.addBindValue("1")
                query.addBindValue(non_room_charges.value("charge_code"))
                query.addBindValue(hotdate.toString("yyyy-MM-dd"))
                query.addBindValue("RGL")
                query.exec_()
                if not query.isActive():
                    print(query.lastError().text())
                    QSqlDatabase.database().rollback()
                    QMessageBox.critical(None, "Posting Error!", query.lastError().text())
                    self.close()
                    return


            #post the accommodation to the charge code
            tax_code = QSqlQuery()
            tax_code.exec_("SELECT tax_code from charge_codes WHERE charge_code = 'ACCOM'")
            tax_code.first()

            if not tax_code.isActive():
                tax = taxes["T1"]
            else:
                tax = taxes[tax_code.value(0)]+1.0
            query = QSqlQuery()

            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, folio, account, hotdate, ledger_id)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            query.addBindValue(trans_time)
            query.addBindValue(user)
            query.addBindValue(room_charges.value("id_bookings").strip())
            query.addBindValue("Autopost: " + room_charges.value("rate_description"))
            query.addBindValue(room_charges.value("amount") - non_rooms)
            query.addBindValue((room_charges.value("amount") - non_rooms)-((room_charges.value("amount") - non_rooms)/tax))
            query.addBindValue(1)
            query.addBindValue("ACCOM")
            query.addBindValue(hotdate)
            query.addBindValue("RGL")
            query.exec_()
            if not query.isActive():
                print(query.lastError().text())
                QSqlDatabase.database().rollback()
                QMessageBox.critical(None, "Posting Error!", query.lastError().text())
                self.close()
                return

            #Post the accommodation to the folio
            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, dr, dr_tax, folio, account, hotdate, ledger_id)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            query.addBindValue(trans_time)
            query.addBindValue(user)
            query.addBindValue("ACCOM")
            query.addBindValue("Autopost: " + room_charges.value("rate_description"))
            query.addBindValue(room_charges.value("amount"))
            query.addBindValue("{:.2f}".format(float(room_charges.value("amount"))-float(room_charges.value("amount"))/tax))
            query.addBindValue(1)
            query.addBindValue(room_charges.value("id_bookings").strip())
            query.addBindValue(hotdate)
            query.addBindValue("RGL")
            query.exec_()
            if not query.isActive():
                print(query.lastError().text())
                QSqlDatabase.database().rollback()
                QMessageBox.critical(None, "Posting Error!", query.lastError().text())
                self.close()
                return



        QSqlDatabase.database().commit()
        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
        QSqlDatabase.database().transaction()
        update_all_data = QSqlQuery()
        update_all_data.exec_("UPDATE transactions SET trans_updated = '{0}' WHERE trans_updated IS NULL".format(trans_time))
        if not update_all_data.isActive():
            print(update_all_data.lastError().text())
            QMessageBox.critical(None, "Update error! ", "Update Error! Please seek support before continuing! <br />" + update_all_data.lastError().text())
            self.close()
            return
        QSqlDatabase.database().commit()


        new_hotdate = hotdate.addDays(1)
        change_hotdate = QSqlQuery()
        change_hotdate.exec_("UPDATE params SET param_date = '{0}' WHERE id = 1".format(new_hotdate.toString('yyyy-MM-dd')))
        if not change_hotdate.isActive():
            print(change_hotdate.lastError().text())
            QMessageBox.critical(None, "Update error! ", "Date change Error! Please seek support before continuing! <br />" + change_hotdate.lastError().text())
            self.close()
            return
        else:
            #global hotdate
            rpt = pms_reports.Sales_ledger_invoices(user=user, 
                                                    pword=pword, 
                                                    database=database, 
                                                    server=server, 
                                                    trans_date=hotdate.toPyDate())              
            hotdate = new_hotdate
            global hotdate_string
            hotdate_string = new_hotdate.toString()
            self.ui.hotdate_lbl.setText(hotdate_string)
            self.populate_mainform()
            QMessageBox.information(None, "Update", "Update completed!")





            SQL_max = ("SELECT MAX(departure_date) FROM bookings")
            SQL = ("INSERT INTO availability_update (avail_date) "
                   "SELECT THE_DATE "
                   "FROM generate_series(?::date, ?::date, interval '1 day') the_date")
            query = QSqlQuery()
            query.exec_(SQL_max)
            query.first()
            if query.size() > 0:
                end_date = query.value(0)


            query.prepare(SQL)
            query.addBindValue(hotdate.toString(Qt.ISODate))
            query.addBindValue(end_date.toString(Qt.ISODate))
            query.exec_()
            if not query.isActive():
                print(query.lastError().text())
                QMessageBox.critical(self, "Re-sync Error! \n", query.lastError().text())
            else:
                QMessageBox.information(self, "Re-sync...", "Re-sync of availability started!")







    def populate_mainform(self):

        self.arrivals_model = QSqlQueryModel()
        self.arrivals_model.setQuery("select room, label, id "
                                     " from bookings_guests "
                                " where arrival_date <= '{0}' and checked_in is NULL and "
                                " cancelled is NULL order by to_number(room, text(9999))".format(hotdate_string))
        self.arrivals_view = self.ui.arrivals
        self.arrivals_view.setModel(self.arrivals_model)    
        self.arrivals_view.setColumnHidden(2, True)
        self.arrivals_view.setColumnWidth(0,40)
        self.arrivals_view.setColumnWidth(1,200)
        self.arrivals_view.setSelectionBehavior(QTableView.SelectRows)


        self.in_house_model = QSqlQueryModel()
        self.in_house_model.setQuery("select room, label, id "
                                     " from bookings_guests "
                                " where departure_date > '{0}' and "
                                " checked_in is not NULL and checked_out is NULL "
                                "order by to_number(room, text(9999))".format(hotdate_string))
        self.in_house_view = self.ui.stayovers
        self.in_house_view.setModel(self.in_house_model) 
        self.in_house_view.setColumnHidden(2, True)
        self.in_house_view.setColumnWidth(0,40)
        self.in_house_view.setColumnWidth(1,200)
        self.in_house_view.setSelectionBehavior(QTableView.SelectRows)



        self.departures_model = QSqlQueryModel()
        self.departures_model.setQuery("select room, label, id "
                                       " from bookings_guests "
                                " where departure_date <= '{0}' and checked_out is NULL "
                                  " and cancelled is NULL order by to_number(room, text(9999))".format(hotdate_string))
        self.departures_view = self.ui.departures
        self.departures_view.setModel(self.departures_model)
        self.departures_view.setColumnHidden(2, True)
        self.departures_view.setColumnWidth(0,40)
        self.departures_view.setColumnWidth(1,200)
        self.departures_view.setSelectionBehavior(QTableView.SelectRows)

        self.departed_model = QSqlQueryModel()
        self.departed_model.setQuery("select room, label, id "
                                     " from bookings_guests "
                                " where departure_date  = '{0}' "
                                " and checked_out is not NULL order by to_number(room, text(9999))".format(hotdate_string))
        self.departed_view = self.ui.checked_out
        self.departed_view.setModel(self.departed_model)
        self.departed_view.setColumnHidden(2, True)
        self.departed_view.setColumnWidth(0,40)
        self.departed_view.setColumnWidth(1,200)
        self.departed_view.setSelectionBehavior(QTableView.SelectRows)
        self.set_status_bar()

        #Check to see if the channel manager interface is running
        iface_query = QSqlQuery()
        SQL = ("SELECT time_stamp FROM params "
               "WHERE id = 7")
        iface_query.exec_(SQL)
        iface_query.first()
        last_time = iface_query.value("time_stamp")
        if last_time.secsTo(QDateTime.currentDateTime()) > 600:
            if database == 'pms':
                if os.environ.get('COMPUTERNAME') != 'MERCURY':
                
                    QMessageBox.critical(self, "Channel Manager Interface", "The connection to the channel manager is not running! "
                                         "You must start the interface or availability for Booking.com, our website and others "
                                         "will not be updated.  DO NOT IGNORE THIS WARNING!")

        #print(QDateTime.currentDateTime())






    def ci_clicked(self, model_index):
        self.res_id = self.arrivals_view.model().index(model_index.row(), 2).data()
        self.gst_form = Guest_form(self.res_id, parent=self)
        self.gst_form.show()

    def ih_clicked(self, model_index):
        self.res_id = self.in_house_view.model().index(model_index.row(), 2).data()
        #print((self.res_id))
        self.gst_form = Guest_form(self.res_id, self)
        self.gst_form.show()

    def dd_clicked(self, model_index):
        self.res_id = self.departures_view.model().index(model_index.row(), 2).data()
        #print((self.res_id))
        self.gst_form = Guest_form(self.res_id, self)
        self.gst_form.show()

    def co_clicked(self, model_index):

        self.res_id = self.departed_view.model().index(model_index.row(), 2).data()
        #print((self.res_id))
        self.gst_form = Guest_form(self.res_id, self)
        self.gst_form.show()

    def set_status_bar(self):
        query = QSqlQuery()
        query.exec_("SELECT SUM(dr) AS dr, SUM(cr) AS cr, SUM(dr_tax) AS dr_tax, SUM(cr_tax) AS cr_tax FROM transactions WHERE trans_updated is NULL")
        query.first()
        status = self.statusBar()
        status.showMessage("dr: " + str(query.value("dr")) +
                           " cr: " + str(query.value("cr")) +
                           " dr tax: " + str(query.value("dr_tax")) +
                           " cr tax: " + str(query.value("cr_tax")) +
                           " Postings balance: " + str(query.value("dr") - query.value("cr")) +
                           " Tax balance: " + str(query.value("dr_tax") - query.value("cr_tax")) +
                           " | " + user + " | " + database
                           ,msecs=50000)






    def guest_history(self):
        self.guest_find = Guest_find_form(self)
        self.guest_find.show()




    def newspaper_prices(self):
        self.newspapers_set = Newspapers_sheet()
        self.newspapers_set.show()

class About(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.ui = Ui_about()
        self.ui.setupUi(self)
        


class Bulk_rates(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.ui = Ui_update_rates()
        self.ui.setupUi(self)
        self.ui.start_date.setMinimumDate(hotdate)
        self.ui.end_date.setMinimumDate(hotdate)

        self.rate_name_model = QSqlTableModel(self)
        self.rate_name_model.setTable("rates")
        self.rate_name_model.select()
        self.rate_name_model.setFilter("derived_from = rate_code")
        self.ui.rate_name.setModel(self.rate_name_model)
        self.ui.rate_name.setModelColumn(self.rate_name_model.fieldIndex("rate_code"))

        #self.room_types_model = QSqlTableModel(self)
        self.room_types_model = QSqlQueryModel(self)
        self.room_types_model.setQuery("SELECT base_type FROM room_types WHERE base_type IS NOT NULL")
        self.ui.room_type.setModel(self.room_types_model)
        self.ui.room_type.setModelColumn(0)

        #self.spin_rates = {}
        #for i in range(1,8):
            #my_spin_box = QDoubleSpinBox()
            #my_spin_box.setValue(99.00)
            #self.spin_rates[i] = my_spin_box
            #self.ui.rates_grid.addWidget(self.spin_rates[i],1,i)

        #self.spin_mlos = {}
        #for i in range(1,8):
            #my_spin_box = QSpinBox()
            #my_spin_box.setValue(1)
            #self.spin_mlos[i] = my_spin_box
            #self.ui.rates_grid.addWidget(self.spin_mlos[i],2,i)


        self.ui.pb_update_rates.clicked.connect(self.update_rates)

        self.setStyleSheet("background-color: " + bg_colour)

    def update_rates(self):
        self.start_date = self.ui.start_date.date().toPyDate()
        self.end_date = self.ui.end_date.date().toPyDate()
        if self.start_date > self.end_date:
            QMessageBox.warning(self, "Rates...", "Start date must be before end date!")
            return

        if (self.end_date - self.start_date).days > 400:
            QMessageBox.warning(self, "Rates...", "Wow! Too many days! Not updated")
            return


        SQL = """SELECT * from rates where rate_code = ?
        UNION
        SELECT * from rates where derived_from = ?
        ORDER BY derived_from DESC"""
        query_rates = QSqlQuery()
        query_rates.prepare(SQL)
        query_rates.addBindValue(self.ui.rate_name.currentText())
        query_rates.addBindValue(self.ui.rate_name.currentText())
        query_rates.exec_()
        QApplication.setOverrideCursor(Qt.WaitCursor)

        while query_rates.next():
            d = self.start_date - timedelta(days=1)
            delta = timedelta(days=1)
            while d <= self.end_date - timedelta(days=1):
                d += delta        
                #QMessageBox.information(self, "Test", d.strftime('%Y-%m-%d'))
                SQL = "SELECT * from rates_daily WHERE rate_code = ? AND room_type = ? AND avail_date = ? AND adults = ?"
                query_rates_daily = QSqlQuery()
                query_rates_daily.prepare(SQL)
                query_rates_daily.addBindValue(query_rates.value("rate_code"))
                query_rates_daily.addBindValue(self.ui.room_type.currentText())
                query_rates_daily.addBindValue(d.strftime('%Y-%m-%d'))
                query_rates_daily.addBindValue(self.ui.adults.text())
                query_rates_daily.exec_()
                query_rates_daily.first()
                qry_update = QSqlQuery()

                new_rate = (getattr(self.ui, 'rate_' + str(d.weekday())).value() + 
                            ((query_rates.value("uplift_adults") * self.ui.adults.value()) +
                             (query_rates.value("uplift_children") * self.ui.adults.value()) +
                             (query_rates.value("uplift_room") * self.ui.adults.value())) )

                if query_rates.value("follow_mlos"):
                    new_mlos = getattr(self.ui, 'mlos_' + str(d.weekday())).value()
                else:
                    new_mlos = 1



                if query_rates_daily.size() > 0: 
                    SQL = "UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?"
                    qry_update.prepare(SQL)
                    #qry_update.addBindValue(self.spin_rates[d.weekday()+1].value())
                    #qry_update.addBindValue(self.spin_mlos[d.weekday()+1].value())
                    qry_update.addBindValue(new_rate)
                    qry_update.addBindValue(new_mlos)
                    qry_update.addBindValue(query_rates_daily.value("id"))

                else:
                    SQL = """INSERT INTO rates_daily (rate_code, room_type, avail_date, adults, rate, mlos) 
                    VALUES (?, ?, ?, ?, ?, ?)"""
                    qry_update.prepare(SQL)
                    qry_update.addBindValue(query_rates.value("rate_code"))
                    qry_update.addBindValue(self.ui.room_type.currentText())
                    qry_update.addBindValue(d.strftime('%Y-%m-%d'))
                    qry_update.addBindValue(self.ui.adults.value())
                    #qry_update.addBindValue(self.spin_rates[d.weekday()+1].value())
                    #qry_update.addBindValue(self.spin_mlos[d.weekday()+1].value())
                    qry_update.addBindValue(new_rate)
                    qry_update.addBindValue(new_mlos)


                qry_update.exec_()

                if not qry_update.isActive():
                    QMessageBox.warning(self, "Failure!", "Update failed! " + " ".join(qry_update.lastError().text().split()))
                    return

        QtWidgets.qApp.processEvents()

        QApplication.restoreOverrideCursor()
        QMessageBox.information(self, "Rates...", "Rates updated!")



class Rate_select(QDialog):
    def __init__(self, arrival_date="", departure_date="", room_type="DBL", rate="RAC", room=None, parent=None, adults=2, nights=1):
        QDialog.__init__(self, parent)
        self.arrival_date = arrival_date
        if departure_date != '':
            self.departure_date = departure_date
        else:
            self.departure_date = self.arrival_date + timedelta(days=1)
        self.room_type = room_type
        self.rate = rate
        self.room = room
        self.adults = adults
        if self.adults < 1:
            self.adults = 1
        self.nights = nights
        if self.nights < 1:
            self.nights = 1
        self.ui = Ui_rates_select()
        self.ui.setupUi(self)
        self.ui.arrival_date.setMinimumDate(hotdate)
        self.ui.arrival_date.setDate(self.arrival_date)
        self.ui.departure_date.setDate(self.departure_date)
        self.setStyleSheet("background-color: " + bg_colour)
        self.ui.adults.setMinimum(1)
        self.ui.adults.setValue(self.adults)
        self.ui.nights.setMinimum(1)
        self.ui.rate.setCurrentText(rate)
        self.ui.room_type.setCurrentText(room_type)
        index = self.ui.room_type.findText(room_type, Qt.MatchFixedString)
        if index >= 0:
            self.ui.room_type.setCurrentIndex(index)        
        self.populate_room_types()
        self.populate_rooms()
        self.populate_rates()
        self.show_rates()
        self.validate_arrival_date()
        self.ui.arrival_date.dateChanged.connect(self.validate_arrival_date)
        self.ui.departure_date.dateChanged.connect(self.validate_departure_date)
        self.ui.nights.valueChanged.connect(self.validate_nights)
        self.ui.room_type.currentIndexChanged.connect(self.show_rates)
        self.ui.adults.valueChanged.connect(self.show_rates)
        self.ui.rate.currentIndexChanged.connect(self.show_rates)
        self.ui.book_button.clicked.connect(self.book_button)
        self.ui.num_rooms.valueChanged.connect(self.validate_room)

    def validate_room(self):
        self.update_totals()
        if self.ui.num_rooms.value() > 1:
            self.ui.room_number.setCurrentIndex(-1)
            self.ui.room_number.setEnabled(False)

        else:
            self.ui.room_number.setEnabled(True)
            self.ui.room_number.setCurrentIndex(0)


    def book_button(self):
        rooms_booked = []
        if self.ui.num_rooms.value() < 1:
            QMessageBox.information(self, "Rate selection...", "Sorry, can't book zero rooms!")
            return
        if self.rates_select_model.rowCount() < 1:
            QMessageBox.warning(self, "Rates selection...", "Sorry, I can't make a booking with no rates.  Perhaps you can check the number of people.")
            return
        if self.ui.room_number.currentText() == '':
            self.room = None
        for i in range(self.ui.num_rooms.value()):
            rooms_booked.append([self.room, self.ui.arrival_date.date().toPyDate(), self.ui.departure_date.date().toPyDate(), self.ui.room_type.currentText(), None])
        avail = Rooms_avail(rooms_booked, self)
        if avail.room_available:
            self.accept()
        else:
            QMessageBox.warning(self, "Booking", "Room is not available!")
            return


    def validate_nights(self):
        self.ui.departure_date.setDate(self.ui.arrival_date.date().addDays(self.ui.nights.value()))
        self.show_rates()

    def validate_arrival_date(self):
        self.ui.departure_date.setDate(self.ui.arrival_date.date().addDays(1))
        self.ui.nights.setValue(self.ui.arrival_date.date().daysTo(self.ui.departure_date.date()))
        self.show_rates()

    def validate_departure_date(self):
        self.ui.nights.setValue(self.ui.arrival_date.date().daysTo(self.ui.departure_date.date()))
        self.show_rates()

    def populate_room_types(self, default_room_type=""):
        self.room_types_model = QSqlQueryModel(self)
        self.room_types_model.setQuery("SELECT base_type FROM room_types WHERE base_type IS NOT NULL AND room_type <> 'TWN'")
        self.room_types_view = self.ui.room_type
        self.room_types_view.setModel(self.room_types_model)
        index = self.ui.room_type.findText(self.room_type, Qt.MatchFixedString)
        if index >=0:
            self.ui.room_type.setCurrentIndex(index)

    def populate_rates(self, default_rate="RAC"):
        """Puts the rate codes into the rate select combo box"""
        self.rates_model = QSqlQueryModel(self)
        self.rates_model.setQuery("SELECT rate_code FROM rates WHERE rate_code <> '1DBB' "
                                  "ORDER BY rate_description")
        self.rates_view = self.ui.rate
        self.rates_view.setModel(self.rates_model)
        index = self.ui.rate.findText(default_rate, Qt.MatchFixedString)
        if index >=0:
            self.rates_view.setCurrentIndex(index)
        else:
            self.rates_view.setCurrentIndex(-1)


    def populate_rooms(self):
        """Puts the rooms in the combo box"""
        self.rooms_model = QSqlQueryModel(self)
        SQL = """SELECT room FROM rooms 
        WHERE room_type = '{0}'      
	AND room_type is not NUll      
	AND room not in    
		(SELECT room from bookings     
		WHERE arrival_date <= '{1}'  --Later date       
		AND departure_date > '{2}'   --Earlier date   
		AND cancelled IS NULL      
		AND checked_out IS NULL    
		group by room)
	ORDER BY to_number(room, text(9999))""".format(self.ui.room_type.currentText(), self.ui.departure_date.date().addDays(-1).toString(Qt.ISODate), self.ui.arrival_date.date().toString(Qt.ISODate))
        self.rooms_model.setQuery(SQL)
        self.rooms_view = self.ui.room_number
        self.rooms_view.setModel(self.rooms_model)
        print(self.rooms_model.query().lastError().text())
        self.ui.room_number.setCurrentText(self.room)



    def show_rates(self):
        """Displays the rates in the table view"""
        self.rates_select_model = QSqlQueryModel()
        depdate = self.ui.departure_date.date().addDays(-1)
        nights = self.ui.arrival_date.date().daysTo(self.ui.departure_date.date())
        self.rates_select_model.setQuery("SELECT rate_code, room_type, avail_date as Date, to_char(avail_date, 'Day') as Day, rate FROM rates_daily "
                                         "WHERE rate_code = '{0}' "
                                         "AND room_type = '{1}' "
                                         "AND adults = {2} "
                                         "AND avail_date >= '{3}' "
                                         "AND avail_date <= '{4}' "
                                         "AND MLOS <= {5}"
                                         .format(self.ui.rate.currentText(),
                                                 self.ui.room_type.currentText(),
                                                 self.ui.adults.value(),
                                                 self.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'),
                                                 depdate.toPyDate().strftime('%Y-%m-%d'),
                                                 nights))

        self.rates_select_view = self.ui.rates_select_2
        self.rates_select_view.setModel(self.rates_select_model)    
        self.rates_select_view.setSelectionBehavior(QTableView.SelectRows)
        self.rates_select_view.setItemDelegateForColumn(4, FloatDelegate(2))

        self.rates_select_view.resizeColumnsToContents()

        total_rows = self.rates_select_model.rowCount()
        room_rate = 0

        self.update_totals()

        #find availability
        SQL = """SELECT min(available) as rooms_avail FROM (SELECT avail_date::date, rt.room_type, rt.base_num - COUNT(bookings.id) as available
        FROM generate_series(?::date, ?::date, interval '1 day') avail_date
        CROSS JOIN (SELECT room_type, base_num 
			FROM room_types
			WHERE base_num is not null
                        AND room_type = ?) rt
        LEFT JOIN (SELECT arrival_date, id, departure_date, room_type
		FROM bookings 
		WHERE cancelled IS NULL
		AND checked_out IS NULL)
		bookings
	ON avail_date BETWEEN bookings.arrival_date AND (bookings.departure_date - INTERVAL '1 DAY')
	AND bookings.room_type = rt.room_type
        GROUP BY avail_date, rt.room_type, base_num
		--HAVING count(bookings.id) > rt.base_num
        ORDER BY avail_date, rt.room_type) availability"""
        query_avail = QSqlQuery()
        query_avail.prepare(SQL)
        query_avail.addBindValue(self.ui.arrival_date.date().toString(Qt.ISODate))
        query_avail.addBindValue(self.ui.departure_date.date().addDays(-1).toString(Qt.ISODate))
        query_avail.addBindValue(self.ui.room_type.currentText())
        query_avail.exec_()
        query_avail.first()
        if not query_avail.isActive():
            print(query_avail.lastError().text())
            QMessageBox.warning(self, "Select rooms...", "Couldn't find availability!\n" + query_avail.lastError().text())
            return
        avail_rooms = query_avail.value('rooms_avail')
        self.ui.num_rooms.setMaximum(avail_rooms)
        if avail_rooms > 0 and self.ui.num_rooms.value() == 0:
            self.ui.num_rooms.setValue(1)

        self.ui.availability.setText(str(avail_rooms) + " available rooms")

    def update_totals(self):
        total_rows = self.rates_select_model.rowCount()
        room_rate = 0
        for row in xrange(0, total_rows):
            room_rate += self.rates_select_model.index(row, 4).data()

        total_rate = self.ui.num_rooms.value() * room_rate
        self.ui.room_rate.setText("{:.2f}".format(room_rate))
        self.ui.average_rate.setText("{:.2f}".format(room_rate/self.ui.nights.value()))
        self.ui.total_rate.setText("{:.2f}".format(total_rate))





class Pms_rooms_chart(QTableWidget):
    """Make custom room chart so drag and drop works"""
    def __init__(self, *args, **kwargs):
        QTableWidget.__init__(rows, columns, parent)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, e):
        e.accept()
        #print ('Pms_rooms_chart dragEnterEvent')


    def dropEvent(self, e):
        position = e.pos()

        #print('Pms_rooms_chart dropEvent')

        self.butObject.move(position)

        e.setDropAction(QtCore.Qt.MoveAction)
        e.accept()

    def dragMoveEvent(self, e):
        e.accept()
        #print('pms_rooms_chart dragMoveEvent')



    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton: #Release event only if done with left button, you can remove if necessary
            indexSelection = []

            for item in self.selectedIndexes(): 
                indexSelection.append( str(item.row())+ "-" + str(item.column()) )

            #print('pms_rooms_chart mouseReleaseEvent')







class Rooms_chart1(QWidget):
    "Builds the room chart"

    def __init__(self):
        super(Rooms_chart1, self).__init__()
        QDialog.__init__(self)
        self.ui = Ui_rooms_chart()
        self.ui.setupUi(self)
        self.timer_interval = 30000
        self.setStyleSheet("background-color: " + bg_colour)
        self.setWindowTitle('Rooms chart')
        self.ui.num_days.setKeyboardTracking(False)

        self.ui.room_chart = Pms_table(0, 0, None, self)
        self.ui.room_chart.setObjectName("room_chart")
        self.ui.verticalLayout.addWidget(self.ui.room_chart)        


        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.build_chart)
        self.timer.start(self.timer_interval)


        self.build_chart()
        self.button.clicked.connect(self.button_clicked)
        self.ui.btn_forward.clicked.connect(self.btn_forward)
        self.ui.btn_backward.clicked.connect(self.btn_backward)
        self.ui.btn_big_forward.clicked.connect(self.btn_big_forward)
        self.ui.btn_big_backward.clicked.connect(self.btn_big_backward)
        self.ui.start_date.dateChanged.connect(self.build_chart)
        self.ui.btn_today.clicked.connect(self.btn_today)
        self.ui.num_days.valueChanged.connect(self.build_chart)
        self.ui.room_chart.cellClicked.connect(self.new_reservation)
        self.ui.find_unallocated.clicked.connect(self.find_unallocated)



    def find_unallocated(self):
        query = QSqlQuery()
        SQL = ("SELECT MIN(arrival_date) as the_date FROM bookings "
               "WHERE (room IS NULL or room !~ '^([0-9]+[.]?[0-9]*|[.][0-9]+)$') "
               "AND cancelled IS NULL "
               "AND checked_out IS NULL "
               "AND arrival_date >= ?")
        query.prepare(SQL)
        query.addBindValue(hotdate.toString(Qt.ISODate))
        query.exec_()
        print(query.lastError().text())
        query.first()
        if query.size() > 0:
            self.ui.start_date.setDate(query.value("the_date"))
        else:
            QMessageBox.information(self, "Find unallocated...", "No more unallocated bookings to find.")

    def build_chart(self):
        #Check for overbookings
        query = QSqlQuery()
        query.exec_("select max(departure_date) as max_date from bookings ")
        if not query.isActive():
            QMessageBox.warning(self, 'Build Chart', "Something is wrong with finding the maximum date of bookings!\n" + query.lastError().text())

        query.first()
        if query.size() > 0:
            max_date = query.value('max_date')
        else:
            max_date = hotdate


        SQL = """SELECT avail_date::date, rt.room_type, rt.base_num - COUNT(bookings.id) as available
        FROM generate_series(?::date, ?::date, interval '1 day') avail_date
        CROSS JOIN (SELECT room_type, base_num 
			FROM room_types
			WHERE base_num is not null
                        AND room_type <> 'DUM') rt
        LEFT JOIN (SELECT arrival_date, id, departure_date, room_type
		FROM bookings 
		WHERE cancelled IS NULL
		AND checked_out IS NULL)
		bookings
	ON avail_date BETWEEN bookings.arrival_date AND (bookings.departure_date - INTERVAL '1 DAY')
	AND bookings.room_type = rt.room_type
        GROUP BY avail_date, rt.room_type, base_num
		HAVING count(bookings.id) > rt.base_num
        ORDER BY avail_date, rt.room_type"""
        query.prepare(SQL)
        query.addBindValue(hotdate.toString(Qt.ISODate))
        query.addBindValue(max_date.toString(Qt.ISODate))
        query.exec_()
        if not query.isActive():
            QMessageBox.warning(self, 'Build Chart', "Something is wrong with finding overbookings!\n" + 
                                query.lastError().text())
        if query.size() > 0:
            self.ui.overbooking_message.show()
            self.ui.overbooking_message.setText('OVERBOOKINGS: ')
            while query.next():
                self.ui.overbooking_message.setText(self.ui.overbooking_message.text() + 
                                                    query.value('avail_date').toString() + ' ' + 
                                                    query.value('room_type') + ' | ')
        else:
            self.ui.overbooking_message.hide( )


        #Set up chart
        indices = self.ui.room_chart.selectedIndexes()          #retain the indices to reselect after rebuild

        self.ui.room_chart.setRowCount(0)
        self.ui.room_chart.setColumnCount(0)        
        base_date = self.ui.start_date.date().toPyDate()
        chartdays = self.ui.num_days.value()

        start_date = base_date.strftime('%Y-%m-%d')
        if start_date == '2000-01-01':
            start_date = hotdate.toPyDate().strftime('%Y-%m-%d')
            base_date = hotdate.toPyDate()
            self.ui.start_date.setDate(hotdate)
        end_date = (base_date + timedelta(days=chartdays)).strftime('%Y-%m-%d')

        rooms = QSqlQuery()
        rooms.exec_("select room, description, room_type, hls_code from rooms where hls_code is not NULL order by to_number(rooms.room, text(9999))")

        real_room_types = QSqlQuery()
        real_room_types.exec_("SELECT real_room_type, room_type "
                              "FROM room_types "
                    "GROUP BY real_room_type, room_type"
                    )

        room_types = QSqlQuery()
        room_types.exec_("SELECT room_type_id, real_room_type, room_type "
                         "FROM room_types"
                         )

        reservations = QSqlQuery()
        reservations.exec_("SELECT label, id, arrival_date, departure_date, room_id, checked_in, checked_out, room_type_id, room, room_type "
                           "FROM bookings_guests " 
                       "WHERE departure_date > '{}' and arrival_date < '{}' AND cancelled IS NULL --AND room_id IS NOT NULL".format(start_date, end_date)
                       )


        #Column header labels
        date_labels= []
        for x in range(chartdays):
            current_day = base_date + timedelta(days=x)
            date_labels.append(current_day.strftime('%a') + '\n' + current_day.strftime('%d/%m/%y'))
        
        self.ui.room_chart.setColumnCount(len(date_labels))
        self.ui.room_chart.setHorizontalHeaderLabels(date_labels)
        #self.ui.room_chart.horizontalHeaderItem(1).setToolTip("There is a big event today\n Make sure you advise guests that the car park will be full!")
        #for i in range(self.ui.num_days.value()):
            #self.ui.room_chart.setColumnWidth(i, (2125/self.ui.num_days.value()))
        self.ui.room_chart.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        #Row header labels
        rooms_xref = {}
        rooms_labels = []
        while rooms.next():
            rooms_labels.append(rooms.value("room"))
            rooms_xref[rooms.value("room")] = rooms.at()+1

        while real_room_types.next():
            rooms_labels.append(real_room_types.value("real_room_type"))
            rooms_xref[real_room_types.value("real_room_type")] = rooms.size() + real_room_types.at() + 1

        self.ui.room_chart.setRowCount(len(rooms_labels))
        self.ui.room_chart.setVerticalHeaderLabels(rooms_labels)

        rooms.first()
        rooms.previous()
        while rooms.next():
            self.ui.room_chart.verticalHeaderItem(rooms.at()).setToolTip(rooms.value("room_type") + '\n' + rooms.value("description"))   #, rooms.value("hls_code"))


        real_room_types.first()
        real_room_types.previous()
        while real_room_types.next():
            self.ui.room_chart.verticalHeaderItem(real_room_types.at()+rooms.size()).setToolTip(real_room_types.value("room_type"))



        #Put the reservataions on    
        while reservations.next():
            self.button = pms_button(self.ui.room_chart)
            self.button.setText(reservations.value("Label"))
            self.button.setObjectName(reservations.value("id"))
            self.button.setToolTip(reservations.value("label") + "\n" + reservations.value("arrival_date").toString('d MMM yyyy') + " to " + reservations.value("departure_date").toString('d MMM yyyy'))

            #Build context menu
            #self.button.setContextMenuPolicy(Qt.CustomContextMenu)
            #self.button.customContextMenuRequested.connect(self.on_context_menu)

            #self.popMenu = QMenu(self)
            #self.popMenu.addAction(QAction('Move', self))
            #self.popMenu.addAction(QAction('Cancel', self))



            if bool(reservations.value("checked_out")):
                self.button.setStyleSheet("background-color: AliceBlue")
            elif bool(reservations.value("checked_in")):
                self.button.setStyleSheet("background-color: DarkKhaki")
            else:
                self.button.setStyleSheet("background-color: lightsteelblue")

            arr_date = reservations.value("arrival_date").toPyDate()
            if arr_date < base_date:
                arr_date = base_date
            delta =  arr_date - base_date
            col_position = delta.days
            delta = reservations.value("departure_date").toPyDate() - arr_date
            los = delta.days

            if reservations.value("room") != '':
                row_position = rooms_xref[reservations.value("room")]-1
            else:   #unallocated reservations
                room_types.first()
                room_types.previous()
                while room_types.next():
                    if room_types.value("room_type") == reservations.value("room_type"):
                        row_position = rooms_xref[room_types.value("real_room_type")]-1
                        break


                insert_row = False
                avail = False
                while not avail:
                    if insert_row:
                        break
                    for x in range(los):
                        if self.ui.room_chart.cellWidget(row_position, col_position + x):
                            avail = False
                            row_position +=1
                            if self.ui.room_chart.verticalHeaderItem(row_position-1).text() != self.ui.room_chart.verticalHeaderItem(row_position).text():
                                avail = False
                                insert_row = True
                                row_position += 1
                            break
                        else:
                            avail = True
                    if avail:
                        if self.ui.room_chart.verticalHeaderItem(row_position-1).text() != self.ui.room_chart.verticalHeaderItem(row_position).text():
                            avail = False
                            insert_row = True
                            row_position += 1
                        else:
                            avail = True

                if insert_row:           

                    self.ui.room_chart.insertRow(row_position)
                    header_item = QTableWidgetItem(room_types.value("room_type"))
                    self.ui.room_chart.setVerticalHeaderItem(row_position, header_item) 
                    self.ui.room_chart.verticalHeaderItem(row_position).setText(room_types.value("real_room_type"))
                    self.ui.room_chart.verticalHeaderItem(row_position).setToolTip(room_types.value("room_type"))
                    #self.ui.room_chart.verticalHeaderItem(rooms.at()).setToolTip(rooms.value("room_type") + '\n' + rooms.value("description")) 

            self.ui.room_chart.setCellWidget(row_position, col_position, self.button)
            if los > 1:
                self.ui.room_chart.setSpan(row_position, col_position, 1, los)
                #print(self.button.text(), self.button.objectName(), row_position, col_position)

            self.button.clicked.connect(self.button_clicked)

        #for p_index in indices:
            #index = QModelIndex(p_index)
            #if index.isValid():
                #self.ui.room_chart.selectionModel().select(index, QItemSelectionModel.Select)

        for x in range(len(indices)):
            if indices[x].isValid():
                self.ui.room_chart.selectionModel().select(indices[x], QItemSelectionModel.Select)
        self.ui.room_chart.setFocus()





    def on_context_menu(self, point):
        # show context menu
        self.popMenu.exec_(self.button.mapToGlobal(point))     
        self.button.mapToGlobal





    def btn_forward(self):
        self.ui.room_chart.clearSelection()
        self.ui.start_date.setDate(self.ui.start_date.date().addDays(1))

    def btn_backward(self):
        self.ui.room_chart.clearSelection()
        self.ui.start_date.setDate(self.ui.start_date.date().addDays(-1))

    def btn_big_forward(self):
        self.ui.room_chart.clearSelection()
        self.ui.start_date.setDate(self.ui.start_date.date().addDays(self.ui.num_days.value()))

    def btn_big_backward(self):
        self.ui.room_chart.clearSelection()
        self.ui.start_date.setDate(self.ui.start_date.date().addDays(self.ui.num_days.value()*-1))

    def btn_today(self):
        self.ui.room_chart.clearSelection()
        self.ui.start_date.setDate(hotdate)

    def new_reservation(self, row, col):
        modifiers = QApplication.keyboardModifiers()
        if modifiers == Qt.ControlModifier:
            return

        room = self.ui.room_chart.verticalHeaderItem(row).text()
        arr_date = self.ui.start_date.date() 
        arr_date = arr_date.toPyDate() + timedelta(days=col)
        #if arr_date < 
        room_type = self.ui.room_chart.verticalHeaderItem(row).toolTip().split()
        room_type = room_type[0]
        new_res = Rate_select(arrival_date=arr_date, room_type=room_type, room=room, parent=self)

        if new_res.exec_():      #&&&
            #QMessageBox.information(self, new_res.ui.arrival_date.date().toString(), new_res.ui.average_rate.text())
            SQL = "select nextval('reference_seq')"
            query = QSqlQuery()
            query.exec_(SQL)
            query.first()
            reference = hotel_code + "-" + str(query.value(0))
            for i in range(new_res.ui.num_rooms.value()):
                if new_res.ui.num_rooms.value() < 2:
                    if new_res.ui.room_number.currentText():
                        room = new_res.ui.room_number.currentText()
                    else:
                        room = None
                else:
                    room = None

                SQL = ("INSERT INTO bookings (hotel_id, departure_date, arrival_date, num_adults, "
                       "reference, channel_ref, room_type, room, booked_at) "
                       "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) "
                       "RETURNING id")   

                query = QSqlQuery()
                query.prepare(SQL)
                query.addBindValue(hotel_code)
                query.addBindValue(new_res.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
                query.addBindValue(new_res.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
                query.addBindValue(new_res.ui.adults.value())
                query.addBindValue(reference)
                query.addBindValue('Direct')
                query.addBindValue(new_res.ui.room_type.currentText())
                query.addBindValue(room)
                query.addBindValue(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                query.exec_()
                query.first()
                self.res_id = query.value("id")
                #self.res_id = query.lastInsertId()
                print(query.lastError().text())
                if not query.isActive():
                    print(query.lastError().text()) 
                    QMessageBox.warning(self,"Reservaton creation failed!",   query.lastError().text())
                    return  


                SQL = ("INSERT INTO rates_stay (rate_date, amount, id_bookings, rate_code) "
                       "SELECT avail_date as rate_date, rate as amount, ? as id_bookings, rate_code "
                       "FROM rates_daily "
                       "WHERE rate_code = ? "
                       "AND room_type = ? "
                       "AND adults = ? "
                       "AND avail_date >= ? "
                       "AND avail_date < ? ")

                query.prepare(SQL)
                query.addBindValue(self.res_id)
                query.addBindValue(new_res.ui.rate.currentText())
                query.addBindValue(new_res.ui.room_type.currentText())
                query.addBindValue(new_res.ui.adults.value())
                query.addBindValue(new_res.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
                query.addBindValue(new_res.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
                query.exec_()
                if not query.isActive():
                    print(query.lastError().text()) 
                    QMessageBox.warning(self,"Rates creation failed!",   query.lastError().text())





            self.gst_form = Guest_form(self.res_id, parent=self)
            self.gst_form.show()
            #self.build_chart()



    def button_clicked(self):
        sender = self.sender().objectName()
        gst_form = Guest_form(sender)
        gst_form.show()
        self.build_chart()

class Setup_rooms(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.ui = Ui_rooms_setup()
        self.ui.setupUi(self)

        self.rooms_model = QSqlRelationalTableModel(self)
        self.rooms_model.setTable('rooms')
        self.rooms_model.setRelation(self.rooms_model.fieldIndex('room_type'), QSqlRelation('room_types1', 'room_type', 'room_type'))
        self.rooms_model.setSort(int(self.rooms_model.fieldIndex("room")), Qt.AscendingOrder)
        self.rooms_model.select()

        self.config_model = QSqlRelationalTableModel(self)
        self.config_model.setTable('room_configurations')
        self.config_model.setSort(self.config_model.fieldIndex('sort_order'), Qt.AscendingOrder)
        self.config_model.setRelation(self.config_model.fieldIndex("configuration"), QSqlRelation('configurations', 'configuration', 'configuration'))
        #print(self.config_model.lastError().text())
        self.config_model.select()
       
        
        self.config_view = my_tableview(self.ui.configurations)
        self.config_view.setModel(self.config_model)
        self.config_model.setHeaderData(self.config_model.fieldIndex("sort_order"), Qt.Horizontal, "sort order")
        self.config_view.setItemDelegate(View_room_config(self))
        self.config_view.setItemDelegate(QSqlRelationalDelegate(self.config_view))
        self.config_view.resizeColumnsToContents()
        self.config_view.horizontalHeader().setStretchLastSection(True)
        
        self.config_view.setColumnHidden(self.config_model.fieldIndex('room'), True)
        self.config_view.horizontalHeader().setStretchLastSection(True)
        self.config_view.setEditTriggers(QAbstractItemView.AllEditTriggers)
 

        self.rooms_mapper = QDataWidgetMapper(self)
        self.rooms_mapper.setSubmitPolicy(QDataWidgetMapper.AutoSubmit)
        self.rooms_mapper.setModel(self.rooms_model)
        self.rooms_mapper.setItemDelegate(QSqlRelationalDelegate(self))
        self.rooms_mapper.addMapping(self.ui.room_num, self.rooms_model.fieldIndex("room"))
        self.rooms_mapper.addMapping(self.ui.room_description, self.rooms_model.fieldIndex("description"))
        self.rooms_mapper.addMapping(self.ui.deep_clean_date, self.rooms_model.fieldIndex("deepcleandate"))
        self.rooms_mapper.addMapping(self.ui.deep_clean_cycle, self.rooms_model.fieldIndex("deepcleancycle"))
        
        room_types_model = self.rooms_model.relationModel(self.rooms_model.fieldIndex('room_type'))
        self.ui.room_type.setModel(room_types_model)
        self.ui.room_type.setModelColumn(room_types_model.fieldIndex('room_type'))
        self.rooms_mapper.addMapping(self.ui.room_type, self.rooms_model.fieldIndex('room_type'))
        self.rooms_mapper.toFirst()

        self.ui.sleeping.setChecked(self.rooms_model.record(0).value("sleeping"))
        
        self.ui.sleeping.stateChanged.connect(self.sleeping_chk_box)
        self.ui.btn_beginning.clicked.connect(lambda: self.save_record('first'))
        self.ui.btn_end.clicked.connect(lambda: self.save_record('last'))
        self.ui.btn_next.clicked.connect(lambda: self.save_record('next'))
        self.ui.btn_previous.clicked.connect(lambda: self.save_record('prev'))
        self.ui.btn_new.clicked.connect(self.add_record)
        self.ui.btn_delete.clicked.connect(self.delete_record)
        self.ui.add_config.clicked.connect(self.add_config)
        self.rooms_mapper.currentIndexChanged.connect(lambda: self.room_changed(self.rooms_mapper.currentIndex()))
        self.ui.current_index.setText('Record ' + str(self.rooms_mapper.currentIndex()+1) + ' of ' + str(self.rooms_model.rowCount())) 
        self.setStyleSheet("background-color: " + bg_colour)
        if self.rooms_model.rowCount() == 0:
            self.add_record()
        else:
            self.room_changed(self.rooms_mapper.currentIndex())


    def sleeping_chk_box(self):
        if self.ui.sleeping.isChecked():
            sleeping = True
        else:
            sleeping = False
            
        index = QModelIndex(self.rooms_model.index(self.rooms_mapper.currentIndex(), self.rooms_model.fieldIndex('sleeping')))     
        #self.config_model.setData(self.index, self.ui.room_num.text(), Qt.EditRole)
        self.rooms_model.setData(index, sleeping, Qt.EditRole)
        self.rooms_model.submitAll()
     
        
        #SQL = ('UPDATE rooms '
               #'SET sleeping = ? '
               #'WHERE room = ?')
        #chk_query = QSqlQuery()
        #chk_query.prepare(SQL)
        #chk_query.addBindValue(sleeping)
        #chk_query.addBindValue(self.ui.room_num.text())
        #chk_query.exec_()
        #if not chk_query.isActive():
            #print(chk_query.lastError().text()) 
    
    
    
    def config_lost_focus(self, event):
        print("I've lost focus!")
        self.config_model.submit()
    
    
    def add_config(self):
        row=self.config_model.rowCount()
        self.config_model.insertRow(row) 
        self.index = QModelIndex(self.config_model.index(row, self.config_model.fieldIndex('room'))) 
        self.config_model.setData(self.index, self.ui.room_num.text(), Qt.EditRole)
        self.config_view.edit(self.index)
         
        print(row, self.index.row())
        print(self.config_model.lastError().text())

    def room_changed(self, index):
        if index >= 0:

            room = self.rooms_model.record(index).value('room')
            self.config_model.setFilter("room = '{0}'".format(room))
            self.ui.sleeping.setChecked(self.rooms_model.record(index).value("sleeping"))
            
            #self.config_model.setFilter("room = '1'")
        else:
            self.config_model.setFilter("room = 'xxx'")

    def save_record(self, param='next'):
        row = self.rooms_mapper.currentIndex()
        self.rooms_mapper.submit()
        if param == 'first':
            row = 0
        elif param == 'prev':
            if row <= 1:
                row = 0
            else:
                row = row - 1

        elif param == 'next':
            row += 1
            if row >= self.rooms_model.rowCount():
                row = self.rooms_model.rowCount() -1
        elif param == 'last':
            row = self.rooms_model.rowCount() - 1
        #self.ui.sleeping.blockSignals(True)
        self.rooms_mapper.setCurrentIndex(row)
        self.ui.sleeping.setChecked(self.rooms_model.record(row).value("sleeping"))
        #self.ui.sleeping.blockSignals(False)
        self.ui.current_index.setText(str(self.rooms_mapper.currentIndex()+1))

    def add_record(self):
        row = self.rooms_model.rowCount()
        self.rooms_mapper.submit()
        self.rooms_model.insertRow(row)
        self.rooms_mapper.setCurrentIndex(row)
        self.ui.room_num.setFocus()

    def delete_record(self):
        room_num = self.ui.room_num.text()
        if QMessageBox.question(self, "Rooms editing...", "Do you want to delete room " + room_num + "?", 
                                QMessageBox.Yes|QMessageBox.No) == QMessageBox.No:
            return
        row = self.rooms_mapper.currentIndex()
        self.rooms_model.removeRow(row)
        self.rooms_model.submitAll()
        if row + 1 >= self.rooms_model.rowCount():
            row = self.rooms_model.rowCount() -1
        self.rooms_mapper.setCurrentIndex(row)

class my_tableview(QTableView):
        
        
    def closeEditor(self, editor, hint):
        # The problem we're trying to solve here is the edit-and-go-away problem. 
        # When ending the editing with submit or return, there's no problem, the
        # model's submit()/revert() is correctly called. However, when ending
        # editing by clicking away, submit() is never called. Fortunately,
        # closeEditor is called and, AFAIK, it's the only case where it's called
        # with NoHint (0). So, in these cases, we want to call model.submit()
        if hint == QAbstractItemDelegate.NoHint:
            QTableView.closeEditor(self, editor,
                QAbstractItemDelegate.SubmitModelCache)
    


class Setup_room_types(QDialog):
    """Setup the room types"""

    def __init__(self):
        QDialog.__init__(self)
        self.ui = Ui_room_types()
        self.ui.setupUi(self)

        self.room_types_model = QSqlTableModel(self)
        self.room_types_model.setTable('room_types1')
        self.room_types_model.setSort(int(self.room_types_model.fieldIndex("room_type")), Qt.AscendingOrder)
        self.room_types_model.select()

        self.room_types_mapper = QDataWidgetMapper(self)
        self.room_types_mapper.setSubmitPolicy(QDataWidgetMapper.AutoSubmit)
        self.room_types_mapper.setModel(self.room_types_model)
        self.room_types_mapper.addMapping(self.ui.room_type, self.room_types_model.fieldIndex("room_type"))
        self.room_types_mapper.addMapping(self.ui.description, self.room_types_model.fieldIndex("description"))
        self.room_types_mapper.addMapping(self.ui.base_type, self.room_types_model.fieldIndex("base_type"))
        self.room_types_mapper.addMapping(self.ui.base_num, self.room_types_model.fieldIndex("base_num"))
        self.room_types_mapper.addMapping(self.ui.real_room_type, self.room_types_model.fieldIndex("real_room_type"))
        self.room_types_mapper.toFirst()

        self.ui.btn_beginning.clicked.connect(lambda: self.save_record('first'))
        self.ui.btn_end.clicked.connect(lambda: self.save_record('last'))
        self.ui.btn_next.clicked.connect(lambda: self.save_record('next'))
        self.ui.btn_previous.clicked.connect(lambda: self.save_record('prev'))
        self.ui.btn_new.clicked.connect(self.add_record)
        self.ui.btn_delete.clicked.connect(self.delete_record)
        self.ui.current_index.setText('Record ' + str(self.room_types_mapper.currentIndex()+1) + ' of ' + str(self.room_types_model.rowCount()) + ' records')
        self.setStyleSheet("background-color: " + bg_colour)

        if self.room_types_model.rowCount() == 0:
            self.add_record()

    def closeEvent(self, event):
        a = self.room_types_mapper.submit()
        event.accept() 

    def save_record(self, param='next'):
        row = self.room_types_mapper.currentIndex()
        self.room_types_mapper.submit()
        if param == 'first':
            row = 0
        elif param == 'prev':
            if row <= 1:
                row = 0
            else:
                row = row - 1

            #row = 0 if row <= 1 else row -=1
        elif param == 'next':
            row += 1
            if row >= self.room_types_model.rowCount():
                row = self.room_types_model.rowCount() -1
        elif param == 'last':
            row = self.room_types_model.rowCount() - 1
        self.room_types_mapper.setCurrentIndex(row)
        self.ui.current_index.setText('Record ' + str(self.room_types_mapper.currentIndex()+1) + ' of ' + str(self.room_types_model.rowCount()) + ' records')

    def add_record(self):
        row = self.room_types_model.rowCount()
        self.room_types_mapper.submit()
        self.room_types_model.insertRow(row)
        self.room_types_mapper.setCurrentIndex(row)
        self.ui.room_type.setFocus()
        self.ui.current_index.setText('Record ' + str(self.room_types_mapper.currentIndex()+1) + ' of ' + str(self.room_types_model.rowCount()) + ' records')
        

    def delete_record(self):
        
        room_type = self.ui.room_type.text()
        if QMessageBox.question(self, "Rooms types editing...", "Do you want to delete the room type " + room_type + "?", QMessageBox.Yes|QMessageBox.No) == QMessageBox.No:
            return
        row = self.room_types_mapper.currentIndex()
        self.room_types_model.removeRow(row)
        self.room_types_model.submitAll()
        self.room_types_model.select()
        if row + 1 >= self.room_types_model.rowCount():
            row = self.room_types_model.rowCount() -1
        self.room_types_mapper.setCurrentIndex(row)
        self.ui.current_index.setText('Record ' + str(self.room_types_mapper.currentIndex()+1) + ' of ' + str(self.room_types_model.rowCount()) + ' records')
        


class Setup_charge_codes(QDialog):
    """Setup the room types"""

    def __init__(self):
        QDialog.__init__(self)
        self.ui = Ui_charge_codes()
        self.ui.setupUi(self)

        self.charge_codes_model = QSqlRelationalTableModel(self)
        self.charge_codes_model.setTable('charge_codes')
        self.charge_codes_model.setRelation(self.charge_codes_model.fieldIndex("tax_code"), QSqlRelation('tax', 'tax_code', 'tax_code'))
        self.charge_codes_model.setSort(int(self.charge_codes_model.fieldIndex("charge_code")), Qt.AscendingOrder)
        self.charge_codes_model.select()

        self.charge_codes_mapper = QDataWidgetMapper(self)
        self.charge_codes_mapper.setSubmitPolicy(QDataWidgetMapper.AutoSubmit)
        self.charge_codes_mapper.setModel(self.charge_codes_model)
        
        self.charge_codes_mapper.addMapping(self.ui.charge_code, self.charge_codes_model.fieldIndex("charge_code"))
        self.charge_codes_mapper.addMapping(self.ui.description, self.charge_codes_model.fieldIndex("description"))
        
        self.tax_codes_model = self.charge_codes_model.relationModel(self.charge_codes_model.fieldIndex('tax_code'))
        self.ui.tax_code.setModel(self.tax_codes_model)
        self.ui.tax_code.setModelColumn(self.tax_codes_model.fieldIndex('tax_code'))
        self.charge_codes_mapper.addMapping(self.ui.tax_code, self.charge_codes_model.fieldIndex('tax_code'))
        self.charge_codes_mapper.addMapping(self.ui.nominal_code, self.charge_codes_model.fieldIndex("nominal_code"))
        self.charge_codes_mapper.addMapping(self.ui.charge_group, self.charge_codes_model.fieldIndex("charge_group"))
        self.charge_codes_mapper.addMapping(self.ui.user_ok, self.charge_codes_model.fieldIndex("user_ok"))
        self.charge_codes_mapper.addMapping(self.ui.z_read, self.charge_codes_model.fieldIndex("z_read"))
        self.charge_codes_mapper.setItemDelegate(QSqlRelationalDelegate(self))
        
        self.charge_codes_mapper.toFirst()

        self.ui.btn_beginning.clicked.connect(lambda: self.save_record('first'))
        self.ui.btn_end.clicked.connect(lambda: self.save_record('last'))
        self.ui.btn_next.clicked.connect(lambda: self.save_record('next'))
        self.ui.btn_previous.clicked.connect(lambda: self.save_record('prev'))
        self.ui.btn_new.clicked.connect(self.add_record)
        self.ui.btn_delete.clicked.connect(self.delete_record)
        self.ui.current_index.setText(str(self.charge_codes_mapper.currentIndex()))
        self.setStyleSheet("background-color: " + bg_colour)

        if self.charge_codes_model.rowCount() == 0:
            self.add_record()


    def save_record(self, param='next'):
        row = self.charge_codes_mapper.currentIndex()
        self.charge_codes_mapper.submit()
        if param == 'first':
            row = 0
        elif param == 'prev':
            if row <= 1:
                row = 0
            else:
                row = row - 1

            #row = 0 if row <= 1 else row -=1
        elif param == 'next':
            row += 1
            if row >= self.charge_codes_model.rowCount():
                row = self.charge_codes_model.rowCount() -1
        elif param == 'last':
            row = self.charge_codes_model.rowCount() - 1
        self.charge_codes_mapper.setCurrentIndex(row)
        self.ui.current_index.setText(str(self.charge_codes_mapper.currentIndex()))

    def add_record(self):
        row = self.charge_codes_model.rowCount()
        self.charge_codes_mapper.submit()
        self.charge_codes_model.insertRow(row)
        self.charge_codes_mapper.setCurrentIndex(row)
        self.ui.charge_code.setFocus()

    def delete_record(self):
        room_num = self.ui.room_num.text()
        if QMessageBox.question(self, "Rooms editing...", "Do you want to delete room " + room_num + "?", 
                                QMessageBox.Yes|QMessageBox.No) == QMessageBox.No:
            return
        row = self.charge_codes_mapper.currentIndex()
        self.charge_codes_model.removeRow(row)
        self.charge_codes_model.submitAll()
        if row + 1 >= self.charge_codes_model.rowCount():
            row = self.charge_codes_model.rowCount() -1
        self.charge_codes_mapper.setCurrentIndex(row)
    
class Setup_room_configurations(QDialog):
    """Set up the room bedtype and equipment configurations"""

    def __init__(self):
        QDialog.__init__(self)
        self.ui = Ui_configuration_form()
        self.ui.setupUi(self)

        self.configurations_model = QSqlTableModel(self)
        self.configurations_model.setTable('configurations')
        #self.configurations_model.setSort(int(self.configurations_model.fieldIndex("room_type")), Qt.AscendingOrder)
        self.configurations_model.select()

        self.configurations_mapper = QDataWidgetMapper(self)
        self.configurations_mapper.setSubmitPolicy(QDataWidgetMapper.AutoSubmit)
        self.configurations_mapper.setModel(self.configurations_model)
        self.configurations_mapper.addMapping(self.ui.configuration, self.configurations_model.fieldIndex("configuration"))

        self.configurations_mapper.toFirst()

        self.ui.btn_beginning.clicked.connect(lambda: self.save_record('first'))
        self.ui.btn_end.clicked.connect(lambda: self.save_record('last'))
        self.ui.btn_next.clicked.connect(lambda: self.save_record('next'))
        self.ui.btn_previous.clicked.connect(lambda: self.save_record('prev'))
        self.ui.btn_new.clicked.connect(self.add_record)
        self.ui.btn_delete.clicked.connect(self.delete_record)
        self.ui.current_index.setText('Record ' + str(self.configurations_mapper.currentIndex()+1) + ' of ' + str(self.configurations_model.rowCount()))
        self.setStyleSheet("background-color: " + bg_colour)

        if self.configurations_model.rowCount() == 0:
            self.add_record()


    def save_record(self, param='next'):
        row = self.configurations_mapper.currentIndex()
        submit = self.configurations_mapper.submit()
        if submit == False:
            print(self.configurations_model.lastError().text())
            QMessageBox.critical(None, "Saving Error!", "Sorry, I couldn't save your changes.  The computer stuff is: " + self.configurations_model.lastError().text())
            return

        if param == 'first':
            row = 0
        elif param == 'prev':
            if row <= 1:
                row = 0
            else:
                row = row - 1

            #row = 0 if row <= 1 else row -=1
        elif param == 'next':
            row += 1
            if row >= self.configurations_model.rowCount():
                row = self.configurations_model.rowCount() -1
        elif param == 'last':
            row = self.configurations_model.rowCount() - 1
        self.configurations_mapper.setCurrentIndex(row)
        self.ui.current_index.setText('Record ' + str(self.configurations_mapper.currentIndex()+1) + ' of ' + str(self.configurations_model.rowCount()))

    def add_record(self):
        row = self.configurations_model.rowCount()
        submit = self.configurations_mapper.submit()
        self.configurations_model.insertRow(row)
        self.configurations_mapper.setCurrentIndex(row)
        self.ui.configuration.setFocus()
        self.ui.current_index.setText('Record ' + str(self.configurations_mapper.currentIndex()+1) + ' of ' + str(self.configurations_model.rowCount()))


    def delete_record(self):
        confirm_text = self.ui.configuration.text()
        if QMessageBox.question(self, "Rooms editing...", "Do you want to delete  " + confirm_text + "?", 
                                QMessageBox.Yes|QMessageBox.No) == QMessageBox.No:
            return
        row = self.configurations_mapper.currentIndex()
        self.configurations_model.removeRow(row)
        self.configurations_model.submitAll()
        if row + 1 >= self.configurations_model.rowCount():
            row = self.configurations_model.rowCount() -1
        self.configurations_mapper.setCurrentIndex(row)
        self.ui.current_index.setText('Record ' + str(self.configurations_mapper.currentIndex()+1) + ' of ' + str(self.configurations_model.rowCount()))

    def closeEvent(self, event):
        a = self.configurations_mapper.submit()
        print(a)
        event.accept() # let the window close        
                   



class New_reservation():
    def __init__(self, parent, guest_id):
        new_res = Rate_select(arrival_date=hotdate.toPyDate(), parent=parent)

        if new_res.exec_():      #&&&
            room_type = new_res.ui.room_type.currentText()
            SQL = "select nextval('reference_seq')"
            query = QSqlQuery()
            query.exec_(SQL)
            query.first()
            reference = hotel_code + "-" + str(query.value(0))
            
            for i in range(new_res.ui.num_rooms.value()):
                if new_res.ui.num_rooms.value() < 2:
                    if new_res.ui.room_number.currentText():
                        room = new_res.ui.room_number.currentText()
                    else:
                        room = None
                else:
                    room = None
                
                


                SQL = ("INSERT INTO bookings (hotel_id, departure_date, arrival_date, num_adults, "
                       "reference, channel_ref, room_type, room, booked_at, guest_id) "
                       "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
                       "RETURNING id")   
    
                query = QSqlQuery()
                query.prepare(SQL)
                query.addBindValue(hotel_code)
                query.addBindValue(new_res.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
                query.addBindValue(new_res.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
                query.addBindValue(new_res.ui.adults.value())
                query.addBindValue(reference)
                query.addBindValue('Direct')
                query.addBindValue(new_res.ui.room_type.currentText())
                query.addBindValue(None)
                query.addBindValue(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                query.addBindValue(guest_id)
                query.exec_()
                query.first()
                self.res_id = query.value("id")
                #self.res_id = query.lastInsertId()
                print(query.lastError().text())
                if not query.isActive():
                    print(query.lastError().text()) 
                    QMessageBox.warning(self,"Reservaton creation failed!",   query.lastError().text())
                    return  


                SQL = ("INSERT INTO rates_stay (rate_date, amount, id_bookings, rate_code) "
                       "SELECT avail_date as rate_date, rate as amount, ? as id_bookings, rate_code "
                       "FROM rates_daily "
                       "WHERE rate_code = ? "
                       "AND room_type = ? "
                       "AND adults = ? "
                       "AND avail_date >= ? "
                       "AND avail_date < ? ")

                query.prepare(SQL)
                query.addBindValue(self.res_id)
                query.addBindValue(new_res.ui.rate.currentText())
                query.addBindValue(new_res.ui.room_type.currentText())
                query.addBindValue(new_res.ui.adults.value())
                query.addBindValue(new_res.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
                query.addBindValue(new_res.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
                query.exec_()
                if not query.isActive():
                    print(query.lastError().text()) 
                    QMessageBox.warning(self,"Rates creation failed!",   query.lastError().text())

            self.gst_form = Guest_form(self.res_id, parent=self)
            self.gst_form.show()
            #self.build_chart()


class pms_button(QPushButton):
    """The button which represents a booking on the rooms chart.
    QPushButton overridden to add drag and drop functionality."""

    def __init__(self, parent):
        super(pms_button, self).__init__(parent)



    def mouseMoveEvent(self, e):
        myapp.rc.timer = None

        mimeData = QMimeData()
        mimeData.setText(self.objectName())


        #pixmap = QPixmap.grab(self)
        pixmap = self.grab()


        # below makes the pixmap half transparent
        painter = QPainter(pixmap)
        painter.setCompositionMode(painter.CompositionMode_DestinationIn)
        painter.fillRect(pixmap.rect(), QColor(0, 0, 0, 127))
        painter.end()

        drag = QDrag(self)
        drag.setMimeData(mimeData)
        drag.setPixmap(pixmap)
        drag.setHotSpot(e.pos() - self.rect().topLeft())
        dropAction = drag.exec_()


    def mousePressEvent(self, e):
        #print('pms_button mousePressEvent')2001118


        QPushButton.mousePressEvent(self, e)



class Newspapers_sheet(QDialog, Ui_newspapers):
    """Displays a form for setting newspaper prices"""

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""
        QDialog.__init__(self)
        self.ui = Ui_newspapers()
        self.ui.setupUi(self)
        self.newspapers_model = QSqlTableModel()
        self.newspapers_model.setTable("newspapers")
        self.newspapers_model.select()
        self.newspapers_view = self.ui.newspaper_sheet
        self.newspapers_view.setModel(self.newspapers_model)
        self.newspapers_view.setColumnHidden(8, True)
        self.ui.newspaper_sheet.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        


class Pms_table(QTableWidget):

    def __init__(self, rows, columns, butObject, parent):
        super(Pms_table, self).__init__(rows, columns, parent)
        self.setAcceptDrops(True)    
        self.moved = 0
        self.start_row = 0
        self.start_col = 0


    def dragEnterEvent(self, e):
        e.accept()
        print('Pms_table dragEnterEvent')
        position = e.pos()
        self.start_row = self.rowAt(position.y())
        self.start_col = self.columnAt(position.x())
        print(self.start_row, self.start_col)



    def dropEvent(self, e):
        print('Pms_table dropEvent')
        position = e.pos()
        row = self.rowAt(position.y())
        if row < 0:
            return
        column = self.columnAt(position.x())
        #print(row, column)
        if self.start_row - row == 0 and self.start_col-column == 0:
        #sender = self.sender().objectName()
            gst_form = Guest_form(e.mimeData().text())
            gst_form.show()
            return
        self.moved = 0
        booking_id = e.mimeData().text()
        if booking_id == '':
            return

        #Get the booking information
        SQL = ('SELECT id, arrival_date, departure_date, room_type, no_room_moves '
               'FROM bookings '
               'WHERE id = ?')

        query = QSqlQuery()
        query.prepare(SQL)
        query.addBindValue(booking_id)
        query.exec_()
        query.first()
        if not query.isActive():
            print(query.lastError().text()) 
            QMessageBox.warning(self,"Failure!",   query.lastError().text())
            return            
        room = self.verticalHeaderItem(row).text()
        room = None if not room.isnumeric() else room

        print(room)
        print('beep')
        arr_date = query.value("arrival_date").toPyDate()
        dep_date = query.value("departure_date").toPyDate()
        rt = query.value("room_type")

        #Check if no_room_moves is ticked
        if query.value("no_room_moves"):
            QMessageBox.warning(self, "Room move...", "Room cannot be moved.  "
                                "Booking is marked for no room moves.  Booking will not be updated!")
            return


        #Check if there is a room type change
        room_type = self.verticalHeaderItem(row).toolTip().split()
        room_type = room_type[0]
        if room_type != rt:
            reply = QMessageBox.question(self, 'Message', 
                                         "You are moving this booking to a different room type.  "
                             "Do you really want to do this?", QMessageBox.Yes, QMessageBox.No)

            if reply == QMessageBox.No:
                return


        #Check if room is available
        avail = Rooms_avail([[room, arr_date, dep_date, room_type, booking_id]], self)


        #Go ahead and move it then
        if avail.room_available:
            u_query = QSqlQuery()
            SQL = ('UPDATE bookings '
                   'SET room = ?, '
                   'room_type = ?'
                   'WHERE id = ?')
            u_query.prepare(SQL)
            u_query.addBindValue(room)
            u_query.addBindValue(room_type)
            u_query.addBindValue(booking_id)
            u_query.exec_()
            if u_query.isActive():
                self.setCellWidget(row,column,e.source())
                e.setDropAction(QtCore.Qt.MoveAction)
                myapp.rc.build_chart()

            else:
                QMessageBox.warning(self, "Room move...", "Room move not successful! \n"
                                    "Computer stuff: " + u_query.lastError().text())
        else:
            QMessageBox.warning(self, "Room move...", "Room is not available.  Booking will not be updated!")

        myapp.rc.timer = QtCore.QTimer()
        myapp.rc.timer.timeout.connect(myapp.rc.build_chart)
        myapp.rc.timer.start(myapp.rc.timer_interval)



    def dragMoveEvent(self, e):
        #self.moved += 1
        e.accept()
        #print('pms_table dragMoveEvent', self.moved)




class Rates_sheet(QDialog, Ui_rates_sheet):
    """Displays rates for the next x days for quick changes"""

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""
        QDialog.__init__(self)
        self.ui = Ui_rates_sheet()
        self.ui.setupUi(self)
        self.ui.num_days.setValue(45)
        self.ui.get_avail.clicked.connect(self.make_rate_sheet)
        self.ui.publish_avail.clicked.connect(self.publish_rates)
        self.populate()
        self.setStyleSheet("background-color: " + bg_colour)


    def populate(self):
        """Put all rates on the page"""
        self.rates_model = MySqlTableModel()
        self.rates_model.setTable("rates_sheet")
        self.rates_model.select()
        self.rates_view = my_tableview(self.ui.rates_grid)
        #self.ui.rates_grid.setRowCount(0)
        #self.ui.rates_grid.setColumnCount(0)        

        self.rates_view.setModel(self.rates_model)
        self.rates_view.resizeColumnsToContents()
        self.rates_view.setSortingEnabled(True)
        #col_count = self.rates_model.columnCount()
        #for x in range(col_count):
            #self.rates_view.setColumnHidden(x, False)
        self.rates_view.setColumnHidden(0, True)
        for x in range(7,11):
            self.rates_view.setColumnHidden(x, True)
        self.rates_view.setColumnHidden(12, True)
        self.rates_view.horizontalHeader().moveSection(3,10)
        self.rates_view.horizontalHeader().moveSection(3,12)
        self.rates_view.horizontalHeader().moveSection(13,2)
        self.rates_view.setItemDelegate(Tmodel(self))
        self.rates_model.setHeaderData(1, Qt.Horizontal, "Date")
        self.rates_model.sort(1, Qt.AscendingOrder)



    def make_rate_sheet(self):
        SQL = "DELETE FROM rates_sheet"

        #print("about to make rates")
        a = update_availability.Make_rates(database=database, user=user, password=pword, 
                                           server=server)
        print("rates made")
        query = QSqlQuery()

        query.exec_(SQL)

        SQL = ("INSERT INTO rates_sheet (hotdate, dtw_avail, fam_avail, mlos, dow) "
               "SELECT availability.availdate, 13 - (availability.roomssold + availability_2.roomssold), 1-availability_1.roomssold, availability.mlos, to_char(availability.availdate, 'DY') "
               "FROM availability INNER JOIN availability AS availability_1 ON availability.availdate = availability_1.availdate "
               "INNER JOIN availability AS availability_2 ON availability.availdate = availability_2.availdate "
               "WHERE (((availability.availdate)>=? And (availability.availdate)<=?) "
               "AND ((availability.roomtype)='DBL') "
               "AND ((availability_1.roomtype)='FAM') "
               "AND ((availability_2.roomtype)='TWN'))")   

        query = QSqlQuery()
        query.prepare(SQL)
        query.addBindValue(datetime.date(datetime.now()).strftime('%Y-%m-%d'))
        query.addBindValue(datetime.date(datetime.now() + timedelta(days=self.ui.num_days.value())).strftime('%Y-%m-%d'))
        query.exec_()
        if not query.isActive():
            print(query.lastError().text()) 
            QMessageBox.warning(self,"Failure!",   query.lastError().text())
            return

        self.rates_view = None
        self.rates_model = None
        self.populate()
        self.ui.get_avail.setEnabled(False)

    def publish_rates(self):
        SQL = ("update rates_sheet SET "
               "dbl_d_2 = dbl_b_2 + 40, "
               "dbl_d_1 = dbl_b_1 + 20, "
               "twn_b_2 = dbl_b_2, "
               "twn_d_2 = dbl_b_2 + 40, "
               "fam_d_2 = fam_b_2 + 40")
        query = QSqlQuery()
        query.exec_(SQL)

        SQL = ("SELECT * from rates_sheet ORDER BY hotdate")
        query.exec_(SQL)
        insert_query = QSqlQuery()
        select_query = QSqlQuery()
        a=0
        while query.next():
            a+=1

            print(a)


            #Double B&B 2 pax
            select_query.prepare("SELECT id FROM rates_daily WHERE avail_date = ? AND room_type = ? AND adults = ? AND rate_code = ?")
            select_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
            select_query.addBindValue("DBL")
            select_query.addBindValue(2)
            select_query.addBindValue('RAC')
            select_query.exec_()
            select_query.first()
            if select_query.isActive():
                insert_query.prepare("UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?")
                insert_query.addBindValue(query.value("dbl_b_2"))
                insert_query.addBindValue(query.value("mlos"))
                insert_query.addBindValue(select_query.value("id"))
            else:
                insert_query.prepare("insert into rates_daily (avail_date, room_type, adults, rate_code, rate, mlos) "
                                     "VALUES (?, ?, ?, ?, ?, ?)")
                insert_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
                insert_query.addBindValue("DBL")
                insert_query.addBindValue(2)
                insert_query.addBindValue('RAC')
                insert_query.addBindValue(query.value("dbl_b_2"))
                insert_query.addBindValue(query.value("mlos"))
            insert_query.exec_()
            print(insert_query.lastError().text())

            #Double B&B 1 pax
            select_query.prepare("SELECT id FROM rates_daily WHERE avail_date = ? AND room_type = ? AND adults = ? AND rate_code = ?")
            select_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
            select_query.addBindValue("DBL")
            select_query.addBindValue(1)
            select_query.addBindValue('RAC')
            select_query.exec_()
            select_query.first()
            if select_query.isActive():
                insert_query.prepare("UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?")
                insert_query.addBindValue(query.value("dbl_b_1"))
                insert_query.addBindValue(query.value("mlos"))
                insert_query.addBindValue(select_query.value("id"))
            else:
                insert_query.prepare("insert into rates_daily (avail_date, room_type, adults, rate_code, rate, mlos) "
                                     "VALUES (?, ?, ?, ?, ?, ?)")
                insert_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
                insert_query.addBindValue("DBL")
                insert_query.addBindValue(1)
                insert_query.addBindValue('RAC')
                insert_query.addBindValue(query.value("dbl_b_1"))
                insert_query.addBindValue(query.value("mlos"))
            insert_query.exec_()
            print(insert_query.lastError().text())

            #twin B&B 2 pax
            select_query.prepare("SELECT id FROM rates_daily WHERE avail_date = ? AND room_type = ? AND adults = ? AND rate_code = ?")
            select_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
            select_query.addBindValue("TWN")
            select_query.addBindValue(2)
            select_query.addBindValue('RAC')
            select_query.exec_()
            select_query.first()
            if select_query.isActive():
                insert_query.prepare("UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?")
                insert_query.addBindValue(query.value("twn_b_2"))
                insert_query.addBindValue(query.value("mlos"))
                insert_query.addBindValue(select_query.value("id"))
            else:
                insert_query.prepare("insert into rates_daily (avail_date, room_type, adults, rate_code, rate, mlos) "
                                     "VALUES (?, ?, ?, ?, ?, ?)")
                insert_query.addBindValue(query.value("hotdate"))
                insert_query.addBindValue("TWN")
                insert_query.addBindValue(2)
                insert_query.addBindValue('RAC')
                insert_query.addBindValue(query.value("twn_b_2"))
                insert_query.addBindValue(query.value("mlos"))
            insert_query.exec_()
            print(insert_query.lastError().text())

            #Double DBB 2 pax
            select_query.prepare("SELECT id FROM rates_daily WHERE avail_date = ? AND room_type = ? AND adults = ? AND rate_code = ?")
            select_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
            select_query.addBindValue("DBL")
            select_query.addBindValue(2)
            select_query.addBindValue('DBB')
            select_query.exec_()
            select_query.first()
            if select_query.isActive():
                insert_query.prepare("UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?")
                insert_query.addBindValue(query.value("dbl_D_2"))
                insert_query.addBindValue(1)
                insert_query.addBindValue(select_query.value("id"))
            else:
                insert_query.prepare("insert into rates_daily (avail_date, room_type, adults, rate_code, rate, mlos) "
                                     "VALUES (?, ?, ?, ?, ?, ?)")
                insert_query.addBindValue(query.value("hotdate"))
                insert_query.addBindValue("DBL")
                insert_query.addBindValue(2)
                insert_query.addBindValue('DBB')
                insert_query.addBindValue(query.value("dbl_d_2"))
                insert_query.addBindValue(1)
            insert_query.exec_()
            print(insert_query.lastError().text())

            #Double DBB 1 pax
            select_query.prepare("SELECT id FROM rates_daily WHERE avail_date = ? AND room_type = ? AND adults = ? AND rate_code = ?")
            select_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
            select_query.addBindValue("DBL")
            select_query.addBindValue(1)
            select_query.addBindValue('DBB')
            select_query.exec_()
            select_query.first()
            if select_query.isActive():
                insert_query.prepare("UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?")
                insert_query.addBindValue(query.value("dbl_d_1"))
                insert_query.addBindValue(1)
                insert_query.addBindValue(select_query.value("id"))
            else:
                insert_query.prepare("insert into rates_daily (avail_date, room_type, adults, rate_code, rate, mlos) "
                                     "VALUES (?, ?, ?, ?, ?, ?)")
                insert_query.addBindValue(query.value("hotdate"))
                insert_query.addBindValue("DBL")
                insert_query.addBindValue(1)
                insert_query.addBindValue('DBB')
                insert_query.addBindValue(query.value("dbl_d_1"))
                insert_query.addBindValue(1)
            insert_query.exec_()
            print(insert_query.lastError().text())

            #Twin DBB 2 pax
            select_query.prepare("SELECT id FROM rates_daily WHERE avail_date = ? AND room_type = ? AND adults = ? AND rate_code = ?")
            select_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
            select_query.addBindValue("TWN")
            select_query.addBindValue(2)
            select_query.addBindValue('DBB')
            select_query.exec_()
            select_query.first()
            if select_query.isActive():
                insert_query.prepare("UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?")
                insert_query.addBindValue(query.value("twn_d_2"))
                insert_query.addBindValue(1)
                insert_query.addBindValue(select_query.value("id"))
            else:            
                insert_query.prepare("insert into rates_daily (avail_date, room_type, adults, rate_code, rate, mlos) "
                                     "VALUES (?, ?, ?, ?, ?, ?)")
                insert_query.addBindValue(query.value("hotdate"))
                insert_query.addBindValue("TWN")
                insert_query.addBindValue(2)
                insert_query.addBindValue('DBB')
                insert_query.addBindValue(query.value("twn_d_2"))
                insert_query.addBindValue(1)
            insert_query.exec_()
            print(insert_query.lastError().text())

            #Fam B&B 2 pax
            select_query.prepare("SELECT id FROM rates_daily WHERE avail_date = ? AND room_type = ? AND adults = ? AND rate_code = ?")
            select_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
            select_query.addBindValue("FAM")
            select_query.addBindValue(2)
            select_query.addBindValue('RAC')
            select_query.exec_()
            select_query.first()
            if select_query.isActive():
                insert_query.prepare("UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?")
                insert_query.addBindValue(query.value("fam_b_2"))
                insert_query.addBindValue(query.value("mlos"))
                insert_query.addBindValue(select_query.value("id"))
            else:
                insert_query.prepare("insert into rates_daily (avail_date, room_type, adults, rate_code, rate, mlos) "
                                     "VALUES (?, ?, ?, ?, ?, ?)")
                insert_query.addBindValue(query.value("hotdate"))
                insert_query.addBindValue("FAM")
                insert_query.addBindValue(2)
                insert_query.addBindValue('RAC')
                insert_query.addBindValue(query.value("fam_b_2"))
                insert_query.addBindValue(query.value("mlos"))
            insert_query.exec_()
            print(insert_query.lastError().text())

            #FAM DBB 2 pax
            select_query.prepare("SELECT id FROM rates_daily WHERE avail_date = ? AND room_type = ? AND adults = ? AND rate_code = ?")
            select_query.addBindValue(query.value("hotdate").toPyDate().strftime('%Y-%m-%d'))
            select_query.addBindValue("FAM")
            select_query.addBindValue(2)
            select_query.addBindValue('DBB')
            select_query.exec_()
            select_query.first()
            if select_query.isActive():
                insert_query.prepare("UPDATE rates_daily SET rate = ?, mlos = ? WHERE id = ?")
                insert_query.addBindValue(query.value("fam_D_2"))
                insert_query.addBindValue(2)
                insert_query.addBindValue(select_query.value("id"))
            else:
                insert_query.prepare("insert into rates_daily (avail_date, room_type, adults, rate_code, rate, mlos) "
                                     "VALUES (?, ?, ?, ?, ?, ?)")
                insert_query.addBindValue(query.value("hotdate"))
                insert_query.addBindValue("FAM")
                insert_query.addBindValue(2)
                insert_query.addBindValue('DBB')
                insert_query.addBindValue(query.value("fam_d_2"))
                insert_query.addBindValue(1)
            insert_query.exec_()
            print(insert_query.lastError().text())





    def update_i(self):
        a = update_internet.Form.update_avail(self)


class MySqlTableModel(QSqlTableModel):
    def __init__(self):
        QSqlTableModel.__init__(self)

    def setData(self, index, value, role=Qt.EditRole):
        if role == Qt.EditRole:
            value = value.strip() if type(value) == str else value

        return super(MySqlTableModel, self).setData(index, value, role)

    def flags(self, index):
        flags = super(MySqlTableModel, self).flags(index)

        if index.column() in (4, 5, 6, 11):
            flags |= Qt.ItemIsEditable
        else:
            flags &= Qt.ItemIsSelectable
        return flags

class Tmodel(QItemDelegate):
    """Remplement rates_sheet table"""

    def __init__(self, parent=None):
        QItemDelegate.__init__(self)


    def createEditor(self, parent, option, index):
        if index.column() == 4:
            spinbox = QSpinBox(parent)
            spinbox.setRange(1,4)
            return spinbox
        elif index.column() in [5,6,11,12]:
            spinbox = QSpinBox(parent)
            spinbox.setRange(49,499)
            return spinbox


    def default_rate(self):
        """Set the default rate for spinboxes to the rack rate"""
        pass


class Company_add(QDialog):
    def __init__(self, res_id=0, parent=None, flags=0):
        QDialog.__init__(self)
        self.res_id = res_id
        self.ui = Ui_company_form()
        self.ui.setupUi(self)


class Book_history(QDialog):
    """Presents the booking history"""

    #----------------------------------------------------------------------
    def __init__(self, res_id=None):
        """Constructor"""
        QDialog.__init__(self)
        self.res_id = res_id
        self.ui = Ui_book_history()
        self.ui.setupUi(self)
        self.book_model = QSqlQueryModel()
        SQL = ("SELECT session_user_name AS User, action_tstamp_tx as When, changed_fields AS Changes "
               "from audit.logged_actions "
               "WHERE row_data->'id' = '{0}'".format(self.res_id))
        self.book_model.setQuery(SQL)
        self.book_view = self.ui.history
        self.book_view.setModel(self.book_model)
        self.book_view.resizeColumnsToContents()
        #self.book_view.resizeRowsToContents()
        

class Rooming_list(QDialog):
    def __init__(self,  res_id=None, reference=None, parent=None):
        QDialog.__init__(self)
        self.res_id = res_id
        self.reference = reference
        self.parent = parent
        self.ui = Ui_rooming_list()
        self.ui.setupUi(self)
        self.ui.reference.setText(self.reference)
        self.ui.reference.setReadOnly(True)
        self.ui.room_list.cellChanged.connect(self.update_row)
        
        rl_query = QSqlQuery()
        SQL = ("SELECT b.id, b.reference, b.room, g.title, g.last_name, g.first_name, g.address "
               "FROM bookings b "
               "JOIN guests g ON g.id = b. guest_id "
               "WHERE b.reference = ? "
               "ORDER BY b.room::int")
        rl_query.prepare(SQL)
        rl_query.addBindValue(self.reference)
        rl_query.exec_()
        if not rl_query.isActive():
            QMessageBox.warning(self, "Rooming list", "Couldn't get rooming list data!\n" 
                                + rl_query.lastError().text())
            return
        if rl_query.size() < 2:
            return
        
        header = ['Room', 'Search','New', 'Title', 'Last Name', 'First name', 'Booking ID']
        self.ui.room_list.setColumnCount(len(header))
        self.ui.room_list.setHorizontalHeaderLabels(header)
        
        #Guest completer
        self.guest_completer = QCompleter()
        self.surname_comp_model = QSqlQueryModel(self)
        self.surname_comp_model.setQuery("SELECT id, CONCAT(last_name, ', ', first_name, ', ', postcode) AS names, company_id FROM guests ORDER BY names")
        self.guest_completer.setModel(self.surname_comp_model)
        self.guest_completer.setCompletionColumn(1)
        self.guest_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.guest_completer.setFilterMode(Qt.MatchContains)
        self.guest_completer.activated[QModelIndex].connect(self.change_guest)
        icon2 = QIcon()
        icon2.addPixmap(QPixmap(":/icons/icons/add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        icon2

        
        
        self.ui.room_list.blockSignals(True)
        while rl_query.next():
            row = rl_query.at()
            self.ui.room_list.insertRow(row)
            
            #Put rooms on
            ln = QTableWidgetItem(rl_query.value('room'))
            self.ui.room_list.setItem(row, 0, ln)
            self.ui.room_list.item(row, 0).setFlags(Qt.ItemIsSelectable)
            
            #search with completer
            gst_srch = QLineEdit()
            gst_srch.setProperty('row', row)
            gst_srch.setCompleter(self.guest_completer)
            self.ui.room_list.setCellWidget(row, 1, gst_srch)
            
            #Put new guest button on
            btn_update = QPushButton()
            btn_update.setIcon(icon2)
            btn_update.setIconSize(QSize(25, 25))
            btn_update.setProperty('row', row)
            btn_update.clicked.connect(self.add_new)
            self.ui.room_list.setCellWidget(row, 2, btn_update)
            
            #Title
            title = QTableWidgetItem(rl_query.value('title'))
            self.ui.room_list.setItem(row, 3, title)
            self.ui.room_list.item(row, 3).setFlags(Qt.ItemIsSelectable)

            #Last name
            ln = QTableWidgetItem(rl_query.value('last_name'))
            self.ui.room_list.setItem(row, 4, ln)
            self.ui.room_list.item(row, 4).setFlags(Qt.ItemIsSelectable)
            

            #First name
            fn = QTableWidgetItem(rl_query.value('first_name'))
            self.ui.room_list.setItem(row, 5, fn)
            self.ui.room_list.item(row, 5).setFlags(Qt.ItemIsSelectable)
            
            #Book id
            fn = QTableWidgetItem(rl_query.value('id'))
            self.ui.room_list.setItem(row, 6, fn)
            self.ui.room_list.item(row, 6).setFlags(Qt.ItemIsSelectable)
            
           
          
        #self.ui.room_list.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ui.room_list.setColumnHidden(6, True)
        self.ui.room_list.setColumnWidth(0, 50)
        self.ui.room_list.setColumnWidth(1, 200)
        self.ui.room_list.setColumnWidth(2, 45)
        self.ui.room_list.setColumnWidth(3, 150)
        self.ui.room_list.setColumnWidth(4, 150)
        self.ui.room_list.setColumnWidth(5, 150)
        
        self.ui.room_list.blockSignals(False)
        
    
    def update_row(self, row, col):
        incomplete = False
        for i in range(3, 6):
            if self.ui.room_list.item(row, i).text() == '':
                incomplete = True
        if incomplete:
            return
        else:
            SQL = ("INSERT INTO guests "
                 "(title, last_name, first_name) "
                 "VALUES (?, ?, ?) "
                 "RETURNING id")
            query_gst = QSqlQuery()
            query_gst.prepare(SQL)
            query_gst.addBindValue(self.ui.room_list.item(row, 3).text())
            query_gst.addBindValue(self.ui.room_list.item(row, 4).text())
            query_gst.addBindValue(self.ui.room_list.item(row, 5).text())
            query_gst.exec_()
            if not query_gst.isActive():
                QMessageBox.critical(self, "Create guest", "Guest creation failed!\n" +
                                     query_gst.lastError().text())
                return
            query_gst.first()
            new_gst_id = query_gst.value('id')
            book_id = self.ui.room_list.item(row, 6).text()
            SQL = ("UPDATE bookings SET guest_id = ? "
                   "WHERE id = ?")
            query_book = QSqlQuery()
            query_book.prepare(SQL)
            query_book.addBindValue(new_gst_id)
            query_book.addBindValue(book_id)
            query_book.exec_()
            if not query_book.isActive():
                QMessageBox.critical(self, "Create guest", "Guest add to booking failed!\n" +
                                     query_book.lastError().text())
                return
            else:
                self.parent.group_model.clear()
                self.parent.group_model.setQuery("SELECT room, label, id, arrival_date, departure_date from bookings_guests "
                                                 "WHERE cancelled IS NULL "
                                                 "AND reference = '{0}'".format(self.reference))

                
    
    
    
    def add_new(self):
        self.ui.room_list.blockSignals(True)
        focused = self.ui.room_list.focusWidget()
        row = int(focused.property('row'))
        
        for i in range(3, 6):
            item = self.ui.room_list.item(row, i)
            if item is not None:
                item.setText('')
                item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable)
        
        self.ui.room_list.blockSignals(False) 
        
            
     
    def change_guest(self, index):
        the_index = self.guest_completer.completionModel().mapToSource(index)

        #if the_index.isValid():
        row = the_index.row()
        focused = self.ui.room_list.focusWidget()
        wrow = int(focused.property('row'))
        book_id = self.ui.room_list.item(wrow, 6).text()
        key = self.surname_comp_model.index(row,0).data()
        self.guest_id = key
        guest_query = QSqlQuery()
        guest_query.prepare("UPDATE bookings SET guest_id = ? WHERE id = ?")
        guest_query.addBindValue(key)
        guest_query.addBindValue(book_id)
        guest_query.exec_()
        if not guest_query.isActive():
            QMessageBox.warning(self, "Guest update", "Guest update was not successful" + guest_query.lastError())
            self.guest_id = 0
            return
        names = self.surname_comp_model.index(row,1).data()
        names = [x.strip() for x in names.split(',')]    
        ln = QTableWidgetItem(names[0])
        ln.setFlags(Qt.ItemIsSelectable)
        fn = QTableWidgetItem(names[1])
        fn.setFlags(Qt.ItemIsSelectable)
        blank = QTableWidgetItem('')
        blank.setFlags(Qt.ItemIsSelectable)
        self.ui.room_list.blockSignals(True)
        self.ui.room_list.setItem(wrow, 3, blank)
        self.ui.room_list.setItem(wrow, 4, ln)
        self.ui.room_list.setItem(wrow, 5, fn)
        self.ui.room_list.blockSignals(False)
        self.parent.group_model.clear()
        self.parent.group_model.setQuery("SELECT room, label, id, arrival_date, departure_date from bookings_guests "
                                  "WHERE cancelled IS NULL "
                                  "AND reference = '{0}'".format(self.reference))
      
        




class Guest_form(QDialog):
    def __init__(self, res_id,  parent=None):

        QDialog.__init__(self)
        self.res_id = res_id
        self.ui = Ui_guest_info_form()
        self.ui.setupUi(self)

        self.guest_model = QSqlTableModel(self)
        self.guest_model.setTable("bookings")
        self.guest_model.setFilter("id = '{0}'".format(self.res_id))
        self.guest_model.select()
        if self.guest_model.rowCount() == 0:
            return

        self.mapper = QDataWidgetMapper(self)
        self.mapper.setSubmitPolicy(QDataWidgetMapper.ManualSubmit)
        self.mapper.setModel(self.guest_model)
        self.mapper.addMapping(self.ui.booking_id, self.guest_model.fieldIndex("id"))
        self.mapper.addMapping(self.ui.booking_ref, self.guest_model.fieldIndex("reference"))
        self.mapper.addMapping(self.ui.arrival_date,ARRIVAL_DATE)
        self.mapper.addMapping(self.ui.departure_date,DEPARTURE_DATE)
        self.mapper.addMapping(self.ui.adults,NUM_ADULTS)
        self.mapper.addMapping(self.ui.kids,NUM_CHILDREN)
        self.mapper.addMapping(self.ui.book_comments, NOTES)
        self.mapper.addMapping(self.ui.channel_ref, CHANNEL_REF)
        self.mapper.addMapping(self.ui.booker_ref, BOOKER_REF)
        self.mapper.addMapping(self.ui.newspaper, NEWSPAPER)
        self.mapper.addMapping(self.ui.babies, self.guest_model.fieldIndex("babies"))
        #self.mapper.addMapping(self.ui.company_id, self.guest_model.fieldIndex("company_id"))
        self.mapper.addMapping(self.ui.ledger_account, self.guest_model.fieldIndex("ledger_id"))
        self.mapper.toFirst()

        num_nights = self.guest_model.record(0).value('arrival_date').daysTo(self.guest_model.record(0).value('departure_date'))
        self.ui.nights.setText(str(num_nights))

        if self.guest_model.record(0).value("cancelled").toString() == '':
            self.ui.ci_co.setText(self.guest_model.record(0).value(CHECKED_IN).toString("dd/MM/yyyy hh:mm"))
            self.ui.co.setText(self.guest_model.record(0).value(CHECKED_OUT).toString("dd/MM/yyyy hh:mm"))
        else:
            self.ui.ci_co.setText(self.guest_model.record(0).value("cancelled").toString("dd/MM/yyyy hh:mm"))
            self.ui.ci_co_label.setText("Cancelled")

        self.ui.room_type.setReadOnly(True)
        self.ui.room_type.setText(self.guest_model.record(0).value("room_type"))
        self.ui.booked_at.setReadOnly(True)
        self.ui.booked_at.setText(self.guest_model.record(0).value("booked_at").toString("dd/MM/yyyy hh:mm"))
        self.ui.channel_ref.setReadOnly(True)
        self.ui.channel_ref.setText(self.guest_model.record(0).value("channel_ref"))
        self.ui.room_num.setReadOnly(True)
        self.ui.room_num.setText(self.guest_model.record(0).value("room"))
        self.ui.room_moves.setChecked(self.guest_model.record(0).value("no_room_moves"))

        self.ui.folio1_co.setText(self.guest_model.record(0).value("folio1_checked_out").toString("dd/MM/yyyy hh:mm"))
        self.ui.folio2_co.setText(self.guest_model.record(0).value("folio2_checked_out").toString("dd/MM/yyyy hh:mm"))
        self.ui.folio3_co.setText(self.guest_model.record(0).value("folio3_checked_out").toString("dd/MM/yyyy hh:mm"))
        self.ui.folio4_co.setText(self.guest_model.record(0).value("folio4_checked_out").toString("dd/MM/yyyy hh:mm"))
        self.ui.folio1_balance.setText("0")
        self.ui.folio2_balance.setText("0") 
        self.ui.folio3_balance.setText("0") 
        self.ui.folio4_balance.setText("0")

        self.setup_group_view()
        self.setup_correspondence_view()
        self.setup_rates_view()
        self.populate_ledgers()
        self.populate_newspapers()
        self.populate_guest_letters()
        self.ui.folios.setCurrentIndex(0)
        self.ui.booker_info_tab_widget.setCurrentIndex(0)
 
        #Ledger stuff
        index = self.ui.ledger_account.findText(self.guest_model.record(0).value('ledger_id'), QtCore.Qt.MatchFixedString)
        if index >= 0:
            self.ui.ledger_account.setCurrentIndex(index)
        self.ledger_info()


        #Populate guest tab
        if self.guest_model.record(0).value("guest_id") != 0:
            self.guest_id = self.guest_model.record(0).value("guest_id")
            self.populate_gst(self.guest_model.record(0).value("guest_id"))
            self.ui.abandon.setEnabled(False)
        else:
            self.guest_id = 0
            self.ui.guest_title.setEnabled(False)
            self.ui.guest_first_name.setEnabled(False)
            self.ui.guest_surname.setEnabled(False)
            self.ui.guest_address.setEnabled(False)
            self.ui.guest_postcode.setEnabled(False)
            self.ui.guest_country.setEnabled(False)
            self.ui.guest_phone.setEnabled(False)
            self.ui.guest_email.setEnabled(False)
            self.ui.guest_comments.setEnabled(False)
            self.ui.abandon.setEnabled(True)

        #Guest completer
        self.guest_completer = QCompleter()
        self.surname_comp_model = QSqlQueryModel(self)
        self.surname_comp_model.setQuery("SELECT id, CONCAT(last_name, ', ', first_name, ', ', postcode) AS names, company_id FROM guests ORDER BY names")
        self.guest_completer.setModel(self.surname_comp_model)
        self.guest_completer.setCompletionColumn(1)
        self.guest_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.guest_completer.setFilterMode(Qt.MatchContains)
        self.guest_completer.activated[QModelIndex].connect(self.change_guest)
        self.ui.guest_search.setCompleter(self.guest_completer)

        #Populate company tab
        self.company_id = self.guest_model.record(0).value("company_id")
        self.populate_companies(self.company_id)

        if self.guest_model.record(0).value("company_id") != None and self.guest_model.record(0).value("company_id").strip() != '':
            pass

        else:
            self.company_id = None
            self.ui.company_search.setEnabled(True)
            self.ui.company_id.setEnabled(False)
            self.ui.booker_name.setEnabled(False)
            self.ui.company_name.setEnabled(False)
            self.ui.company_address.setEnabled(False)
            self.ui.company_postcode.setEnabled(False)
            self.ui.company_country.setEnabled(False)
            self.ui.company_phone.setEnabled(False)
            self.ui.company_email.setEnabled(False)
            self.ui.company_comments.setEnabled(False)


        #company completer
        self.company_completer = QCompleter()
        self.company_comp_model = QSqlQueryModel(self)
        self.company_comp_model.setQuery("SELECT company_id, CONCAT(company_id, ', ', company_name) AS names FROM companies ORDER BY names")
        self.company_completer.setModel(self.company_comp_model)
        self.company_completer.setCompletionColumn(1)
        self.company_completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.company_completer.setFilterMode(Qt.MatchContains)
        self.company_completer.activated[QModelIndex].connect(self.change_company)
        self.ui.company_search.setCompleter(self.company_completer)


        self.setup_folio1()
        self.setup_folio2()
        self.setup_folio3()
        self.setup_folio4()


        #Set up events
        self.ui.room_moves.stateChanged.connect(self.room_move_chk_box)
        self.ui.send_guest_letter.clicked.connect(self.send_correspondence)
        self.ui.check_out_all.clicked.connect(lambda: self.check_out_now(folio_num=0))
        self.ui.check_out_1.clicked.connect(lambda: self.check_out_now(folio_num=1))
        self.ui.check_out_2.clicked.connect(lambda: self.check_out_now(folio_num=2))
        self.ui.check_out_3.clicked.connect(lambda: self.check_out_now(folio_num=3))
        self.ui.check_out_4.clicked.connect(lambda: self.check_out_now(folio_num=4))
        self.ui.save_record.clicked.connect(self.update_record)
        self.ui.check_in.clicked.connect(self.check_in)
        self.ui.post_charge1.clicked.connect(lambda: self.post_charges(1))
        self.ui.post_charge2.clicked.connect(lambda: self.post_charges(2))
        self.ui.post_charge3.clicked.connect(lambda: self.post_charges(3))
        self.ui.post_charge4.clicked.connect(lambda: self.post_charges(4))
        self.ui.post_payment1.clicked.connect(lambda: self.post_payments(1))
        self.ui.post_payment2.clicked.connect(lambda: self.post_payments(2))
        self.ui.post_payment3.clicked.connect(lambda: self.post_payments(3))
        self.ui.post_payment4.clicked.connect(lambda: self.post_payments(4))
        self.ui.void_item1.clicked.connect(lambda: self.void_transactions(folio_num=1, model_indexes=self.folio1_view.selectedIndexes(), folio_view=self.folio1_view))
        self.ui.void_item2.clicked.connect(lambda: self.void_transactions(folio_num=2, model_indexes=self.folio2_view.selectedIndexes(), folio_view=self.folio2_view))
        self.ui.void_item3.clicked.connect(lambda: self.void_transactions(folio_num=3, model_indexes=self.folio3_view.selectedIndexes(), folio_view=self.folio3_view))
        self.ui.void_item4.clicked.connect(lambda: self.void_transactions(folio_num=4, model_indexes=self.folio4_view.selectedIndexes(), folio_view=self.folio4_view))
        self.ui.folio_move1.clicked.connect(lambda: self.move_transactions(folio_num=1, model_indexes=self.folio1_view.selectedIndexes(), folio_view=self.folio1_view))
        self.ui.folio_move2.clicked.connect(lambda: self.move_transactions(folio_num=2, model_indexes=self.folio2_view.selectedIndexes(), folio_view=self.folio2_view))
        self.ui.folio_move3.clicked.connect(lambda: self.move_transactions(folio_num=3, model_indexes=self.folio3_view.selectedIndexes(), folio_view=self.folio3_view))
        self.ui.folio_move4.clicked.connect(lambda: self.move_transactions(folio_num=4, model_indexes=self.folio4_view.selectedIndexes(), folio_view=self.folio4_view))
        self.ui.transfer1.clicked.connect(lambda: self.transfer_transactions(folio_num=1, model_indexes=self.folio1_view.selectedIndexes(), folio_view=self.folio1_view))
        self.ui.transfer2.clicked.connect(lambda: self.transfer_transactions(folio_num=2, model_indexes=self.folio2_view.selectedIndexes(), folio_view=self.folio2_view))
        self.ui.transfer3.clicked.connect(lambda: self.transfer_transactions(folio_num=3, model_indexes=self.folio3_view.selectedIndexes(), folio_view=self.folio3_view))
        self.ui.transfer4.clicked.connect(lambda: self.transfer_transactions(folio_num=4, model_indexes=self.folio4_view.selectedIndexes(), folio_view=self.folio4_view))
        #self.ui.company_id.currentIndexChanged.connect(self.company_info)
        self.ui.ledger_account.currentIndexChanged.connect(self.ledger_info)
        self.ui.print1.clicked.connect(lambda: self.print_fol(person=user, folio_num=1, trans_date=hotdate, res_ref=self.res_id))
        self.ui.print2.clicked.connect(lambda: self.print_fol(person=user, folio_num=2, trans_date=hotdate, res_ref=self.res_id))
        self.ui.print3.clicked.connect(lambda: self.print_fol(person=user, folio_num=3, trans_date=hotdate, res_ref=self.res_id))
        self.ui.print4.clicked.connect(lambda: self.print_fol(person=user, folio_num=4, trans_date=hotdate, res_ref=self.res_id))
        self.ui.print_all_folios.clicked.connect(lambda: self.print_fol(person=user, folio_num=0, trans_date=hotdate, res_ref=self.res_id))
        self.ui.email1.clicked.connect(lambda: self.email_fol(person=user, folio_num=1, trans_date=hotdate, res_ref=self.res_id))
        self.ui.email2.clicked.connect(lambda: self.email_fol(person=user, folio_num=2, trans_date=hotdate, res_ref=self.res_id))
        self.ui.email3.clicked.connect(lambda: self.email_fol(person=user, folio_num=3, trans_date=hotdate, res_ref=self.res_id))
        self.ui.email4.clicked.connect(lambda: self.email_fol(person=user, folio_num=4, trans_date=hotdate, res_ref=self.res_id))
        self.ui.email_all_folios.clicked.connect(lambda: self.email_fol(person=user, folio_num=0, trans_date=hotdate, res_ref=self.res_id))
        self.group_view.clicked.connect(self.group_member_clicked)
        self.corres_view.clicked.connect(self.correspondence_clicked)
        self.ui.reg_card.clicked.connect(self.reg_card_print)
        self.ui.key_card.clicked.connect(self.key_card_print)
        self.ui.company_add.clicked.connect(self.company_new)
        self.ui.cancel_booking.clicked.connect(self.cancel_booking)
        self.ui.u_check_out.clicked.connect(self.undo_check_out)
        self.ui.u_cancellation.clicked.connect(self.undo_cancellation)
        self.ui.new_guest.clicked.connect(self.new_guest)
        self.ui.abandon.clicked.connect(self.abandon)
        self.ui.arrival_date.dateChanged.connect(self.date_change)
        self.ui.departure_date.dateChanged.connect(self.date_change)
        self.ui.ledger_account.currentIndexChanged.connect(self.update_bookings_ledger)
        self.ui.copy_res.clicked.connect(self.copy_res)
        self.ui.group_move.clicked.connect(self.group_move)
        self.ui.book_history.clicked.connect(self.book_history)
        self.ui.group_split.clicked.connect(self.group_split)
        self.ui.group_list.clicked.connect(self.group_list)
        self.ui.rates_select.clicked.connect(self.update_rates)
        


        self.setStyleSheet("background-color: " + bg_colour)

        if self.guest_model.record(0).value("checked_in").toString() != '':
            self.ui.cancel_booking.setEnabled(False)

        if self.ui.ci_co.text() != '':
            self.ui.arrival_date.setEnabled(False)
        else:
            self.ui.arrival_date.setEnabled(True)
            self.ui.arrival_date.setMinimumDate(hotdate)

        if self.ui.ci_co.text() == '':
            self.disable_posting(self.ui)
            self.ui.cancel_booking.setEnabled(True)

        if self.ui.co.text() != '':
            self.disable_payment(self.ui)
            self.disable_posting(self.ui)
            self.ui.u_check_out.setEnabled(True)
            self.ui.cancel_booking.setEnabled(False)
        if self.guest_model.record(0).value("cancelled").toString() != '':
            self.disable_payment(self.ui)
            self.disable_posting(self.ui)
            self.ui.u_cancellation.setEnabled(True)
            self.ui.cancel_booking.setEnabled(False)

    def update_rates(self):
        query = QSqlQuery()
        SQL = ("SELECT  rate_code FROM rates_stay "
               "WHERE id_bookings = ? "
               "AND rate_date >= ? "
               "ORDER BY rate_date")
        query.prepare(SQL)
        query.addBindValue(self.res_id)
        query.addBindValue(hotdate.toString(Qt.ISODate))
        query.exec_()
        query.first()
        rate = query.value('rate_code')
        
        
        
        update_res = Rate_select(arrival_date=self.ui.arrival_date.date().toPyDate(), departure_date=self.ui.departure_date.date().toPyDate(), 
                                 parent=self, rate=rate, adults=int(self.ui.adults.text()), nights=int(self.ui.nights.text()))
        if update_res.exec_():      #&&&
            
            room = None
                
                


            SQL = ("UPDATE bookings SET "
                   "departure_date = ?, "
                   "arrival_date = ?, "
                   "num_adults = ?, "
                   "room_type = ?, "
                   "room = ? "
                   "WHERE id = ? "
                   )   

            query = QSqlQuery()
            query.prepare(SQL)
            query.addBindValue(update_res.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(update_res.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(update_res.ui.adults.value())
            query.addBindValue(update_res.ui.room_type.currentText())
            query.addBindValue(None)
            query.addBindValue(self.res_id)
            query.exec_()

            if not query.isActive():
                print(query.lastError().text()) 
                QMessageBox.warning(self,"Reservaton creation failed!",   query.lastError().text())
                return  
            
            SQL = ("DELETE FROM rates_stay "
                   "WHERE (rate_date < ? or rate_date > ?) "
                   "AND id_bookings = ?")

            query.prepare(SQL)
            query.addBindValue(update_res.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(update_res.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(self.res_id)
            query.exec_()
            if not query.isActive():
                print(query.lastError().text()) 
                QMessageBox.warning(self,"Rates creation failed!",   query.lastError().text())


            SQL = ("INSERT INTO rates_stay (rate_date, amount, id_bookings, rate_code) "
                   "SELECT avail_date as rate_date, rate as amount, ? as id_bookings, rate_code "
                   "FROM rates_daily "
                   "WHERE rate_code = ? "
                   "AND room_type = ? "
                   "AND adults = ? "
                   "AND avail_date >= ? "
                   "AND avail_date < ? ")

            query.prepare(SQL)
            query.addBindValue(self.res_id)
            query.addBindValue(update_res.ui.rate.currentText())
            query.addBindValue(update_res.ui.room_type.currentText())
            query.addBindValue(update_res.ui.adults.value())
            query.addBindValue(update_res.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(update_res.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.exec_()
            if not query.isActive():
                print(query.lastError().text()) 
                QMessageBox.warning(self,"Rates creation failed!",   query.lastError().text())
            
            self.rates_model.select()
           

        
    
    def group_list(self):
        if self.group_model.rowCount() < 2:
            QMessageBox.warning(self, "Rooming list", "You don't need a rooming list for just one reservation. "
                                "Therefore, I won't do it.")
            return
        rl = Rooming_list(res_id=self.res_id, reference=self.ui.booking_ref.text(), parent=self)
        rl.exec_()
        
        
    
        
    def group_split(self):
        retval = QMessageBox.question(self, "Split from group...", 
                             "Do you want to split this booking from it's group "
                             "and creat its own group?", QMessageBox.Yes | QMessageBox.No)
        if retval == QMessageBox.Yes:
            SQL = "select nextval('reference_seq')"
            query = QSqlQuery()
            query.exec_(SQL)
            query.first()
            reference = hotel_code + "-" + str(query.value(0))
            grp_query = QSqlQuery()
            SQL = ("UPDATE bookings "
                   "SET reference = ? "
                   "WHERE id = ?")
            grp_query.prepare(SQL)
            grp_query.addBindValue(reference)
            grp_query.addBindValue(self.res_id)
            grp_query.exec_()
            if not grp_query.isActive():
                QMessageBox.warning(self, "Group split...", "Cannot split from group! \n" + 
                                    grp_query.lastError().text())
            else:
                self.ui.booking_ref.setText(reference)
                self.group_model.clear()
                self.group_model.setQuery("SELECT room, label, id, arrival_date, departure_date from bookings_guests "
                                          "WHERE cancelled IS NULL "
                                          "AND reference = '{0}'".format(reference))
            
            
       
        
    def book_history(self):
        self.book_hist = Book_history(self.res_id)
        self.book_hist.show()

    
    def group_move(self):
        find_new_group = Guest_find_form(self, search_box=True)
        if find_new_group.exec_():
            grp_query = QSqlQuery()
            SQL = ("UPDATE bookings "
                   "SET reference = ? "
                   "WHERE id = ?")
            grp_query.prepare(SQL)
            grp_query.addBindValue(find_new_group.reference)
            grp_query.addBindValue(self.res_id)
            
            grp_query.exec_()
            if not grp_query.isActive():
                QMessageBox.warning(self, "Group add...", "Cannot add to group! \n" + 
                                    grp_query.lastError().text())
            else:
                self.ui.booking_ref.setText(find_new_group.reference)
                self.group_model.clear()
                self.group_model.setQuery("SELECT room, label, id, arrival_date, departure_date from bookings_guests "
                                          "WHERE cancelled IS NULL "
                                          "AND reference = '{0}'".format(find_new_group.reference))
                
                
                
      
        
        
    def copy_res(self):
        new_copy = New_reservation(self, self.guest_id)

    def update_bookings_ledger(self):
        led_query = QSqlQuery()
        led_query.prepare("UPDATE bookings SET ledger_id = ? "
                          " WHERE id = ?")
        led_query.addBindValue(self.ui.ledger_account.currentText())
        led_query.addBindValue(self.res_id)
        led_query.exec_()
        if not led_query.isActive():
            print(led_query.lastError().text()) 
        self.ledger_info()



    def room_move_chk_box(self):
        if self.ui.room_moves.isChecked():
            room_moves = True
        else:
            room_moves = False
        SQL = ('UPDATE bookings '
               'SET no_room_moves = ? '
               'WHERE id = ?')
        chk_query = QSqlQuery()
        chk_query.prepare(SQL)
        chk_query.addBindValue(room_moves)
        chk_query.addBindValue(self.res_id)
        chk_query.exec_()
        if not chk_query.isActive():
            print(chk_query.lastError().text()) 




    def date_change(self):


        if self.ui.departure_date.date() <= self.ui.arrival_date.date():
            self.ui.departure_date.setDate(self.ui.arrival_date.date().addDays(1))

        avail = Rooms_avail(rooms_to_check=[[self.ui.room_num.text(), self.ui.arrival_date.date().toPyDate(), self.ui.departure_date.date().toPyDate(), self.ui.room_type.text(), self.res_id]], parent=self)
        if avail.room_available:
            SQL = ("INSERT INTO rates_stay (rate_date, amount, id_bookings, rate_code) "
                   "SELECT avail_date as rate_date, rate as amount, ? as id_bookings, rate_code "
                   "FROM rates_daily "
                   "WHERE rate_code = ? "
                   "AND room_type = ? "
                   "AND adults = ? "
                   "AND avail_date >= ? "
                   "AND avail_date < ? "
                   "AND avail_date NOT IN (SELECT rate_date from rates_stay WHERE id_bookings = ? )"
                   )

            query.prepare(SQL)
            query.addBindValue(self.res_id)
            query.addBindValue('RAC')
            query.addBindValue(self.guest_model.record(0).value("room_type"))
            query.addBindValue(self.ui.adults.text())
            query.addBindValue(self.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(self.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(self.res_id)
            query.exec_()
            if not query.isActive():
                print(query.lastError().text()) 
                QMessageBox.warning(self,"Rates creation failed!",   query.lastError().text())

            SQL = ("DELETE FROM rates_stay "
                   "WHERE (rate_date < ? or rate_date > ?) "
                   "AND id_bookings = ?")

            query.prepare(SQL)
            query.addBindValue(self.ui.arrival_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(self.ui.departure_date.date().toPyDate().strftime('%Y-%m-%d'))
            query.addBindValue(self.res_id)
            query.exec_()
            if not query.isActive():
                print(query.lastError().text()) 
                QMessageBox.warning(self,"Rates creation failed!",   query.lastError().text())



            self.rates_model.select()
            try:
                myapp.rc.build_chart()
            except:
                pass

        else:
            QMessageBox.warning(self, "Booking", "Room is not avaiable!")
            self.ui.departure_date.blockSignals(True)
            self.ui.arrival_date.blockSignals(True)
            self.ui.departure_date.setDate(self.guest_model.record(0).value("departure_date"))
            self.ui.arrival_date.setDate(self.guest_model.record(0).value("arrival_date"))
            self.ui.departure_date.blockSignals(False)
            self.ui.arrival_date.blockSignals(False)


        num_nights = self.ui.arrival_date.date().daysTo(self.ui.departure_date.date())
        self.ui.nights.setText(str(num_nights))


    def abandon(self):
        """Remove a reservation which was started but not provided with guest details"""
        if self.guest_id == 0:
            SQL = ("DELETE FROM rates_stay "
                   "WHERE id_bookings = ?")
            query = QSqlQuery()
            query.prepare(SQL)
            query.addBindValue(self.ui.booking_id.text())
            query.exec_()
            if not query.isActive():
                QMessageBox.warning(self, "Bookings...", "Abandon was unsuccessful! \n" 
                                    "Computer says: rates_stay " + query.lastError().text())
                return

            SQL = ("DELETE FROM bookings "
                   "WHERE id = ?")
            query = QSqlQuery()
            query.prepare(SQL)
            query.addBindValue(self.ui.booking_id.text())
            query.exec_()
            if not query.isActive():
                QMessageBox.warning(self, "Bookings...", "Abandon was unsuccessful! \n" 
                                    "Computer stuff: guests" + query.lastError().text())
            else:
                QMessageBox.information(self, "Bookings...", "Poof! This booking is gone.")
                self.done(0)

        else:
            QMessageBox.warning(self, "Bookings...", "Too late! guest already created.\n "
                                "Cancel this booking instead. ")



    def new_guest(self):
        """Create a new guest and enable fields"""
        old_guest_id = self.guest_id
        SQL = ("INSERT INTO guests DEFAULT VALUES")   

        query = QSqlQuery()
        query.exec_(SQL)
        self.guest_id = query.lastInsertId()
        #self.guest_model.record(0).value("guest_id")
        if not query.isActive():
            print(query.lastError().text()) 
            QMessageBox.warning(self,"Guest creation failed!\n",   query.lastError().text())
            self.guest_id = 0
            return  
        if old_guest_id == 0:
            SQL = ("UPDATE bookings SET guest_id = ? "
                   "WHERE reference = ? "
                   "AND guest_id IS NULL")
    
            query = QSqlQuery()
            query.prepare(SQL)
            query.addBindValue(self.guest_id)
            query.addBindValue(self.ui.booking_ref.text())
            query.exec_()
            if not query.isActive():
                print(query.lastError().text()) 
                QMessageBox.warning(self,"Guest creation failed!\n",   query.lastError().text())
                self.guest_id = 0
                return 
        else:
            SQL = ("UPDATE bookings SET guest_id = ? "
                   "WHERE id = ? ")
            query = QSqlQuery()
            query.prepare(SQL)
            query.addBindValue(self.guest_id)
            query.addBindValue(self.ui.booking_id.text())
            query.exec_()
            if not query.isActive():
                print(query.lastError().text()) 
                QMessageBox.warning(self,"Guest addition to the booking failed!\n",   query.lastError().text())
                self.guest_id = 0
                return 
            
            
            
        
        self.ui.guest_title.setEnabled(True)
        self.ui.guest_first_name.setEnabled(True)
        self.ui.guest_surname.setEnabled(True)
        self.ui.guest_address.setEnabled(True)
        self.ui.guest_postcode.setEnabled(True)
        self.ui.guest_country.setEnabled(True)
        self.ui.guest_phone.setEnabled(True)
        self.ui.guest_email.setEnabled(True)
        self.ui.guest_comments.setEnabled(True)
        self.populate_gst(self.guest_id)
        self.group_model.setQuery("SELECT room, label, id, arrival_date, departure_date from bookings_guests "
                                  "WHERE cancelled IS NULL "
                                  "AND reference = '{0}'".format(self.ui.booking_ref.text()))



    def change_guest(self, index):
        the_index = self.ui.guest_search.completer().completionModel().mapToSource(index)

        if the_index.isValid():
            row = the_index.row()
            key = self.surname_comp_model.index(row,0).data()
            self.guest_id = key
            guest_query = QSqlQuery()
            guest_query.prepare("UPDATE bookings SET guest_id = ? WHERE id = ?")
            guest_query.addBindValue(key)
            guest_query.addBindValue(self.ui.booking_id.text())
            guest_query.exec_()
            if not guest_query.isActive():
                QMessageBox.warning(self, "Guest update", "Guest update was not successful" + guest_query.lastError())
                self.guest_id = 0

            guest_query.prepare("UPDATE bookings "
                                "SET guest_id = ? "
                                "WHERE reference = ? "
                                "AND guest_id IS NULL")
            guest_query.addBindValue(key)
            guest_query.addBindValue(self.ui.booking_ref.text())
            guest_query.exec_()
            if not guest_query.isActive():
                QMessageBox.warning(self, "Guest update", "Guest update for the group was not successful" + guest_query.lastError())
                self.guest_id = 0
            self.group_model.setQuery("SELECT room, label, id, arrival_date, departure_date from bookings_guests "
                                      "WHERE cancelled IS NULL "
                                      "AND reference = '{0}'".format(self.ui.booking_ref.text()))
            self.group_view.resizeColumnsToContents()




            self.populate_gst(key)
            self.ui.guest_title.setEnabled(True)
            self.ui.guest_first_name.setEnabled(True)
            self.ui.guest_surname.setEnabled(True)
            self.ui.guest_address.setEnabled(True)
            self.ui.guest_postcode.setEnabled(True)
            self.ui.guest_country.setEnabled(True)
            self.ui.guest_phone.setEnabled(True)
            self.ui.guest_email.setEnabled(True)
            self.ui.guest_comments.setEnabled(True)

    def change_company(self, index):
        the_index = self.ui.company_search.completer().completionModel().mapToSource(index)

        if the_index.isValid():
            row = the_index.row()
            key = self.company_comp_model.index(row,0).data()
            self.company_id = key
            company_query = QSqlQuery()
            company_query.prepare("UPDATE bookings SET company_id = ? WHERE id = ?")
            company_query.addBindValue(key)
            company_query.addBindValue(self.ui.booking_id.text())
            company_query.exec_()
            if not company_query.isActive():
                QMessageBox.warning(self, "Company update", "Company update was not successful! The computer stuff is: \n" + company_query.lastError())
                self.company = 0
            guest_query = QSqlQuery()
            guest_query.prepare("UPDATE guests SET company_id = ? WHERE id = ?")
            guest_query.addBindValue(key)
            guest_query.addBindValue(self.res_id)
            guest_query.exec_()

            self.populate_companies(key)
            self.ui.company_name.setEnabled(True)
            self.ui.booker_name.setEnabled(True)
            self.ui.company_address.setEnabled(True)
            self.ui.company_postcode.setEnabled(True)
            self.ui.company_country.setEnabled(True)
            self.ui.company_phone.setEnabled(True)
            self.ui.company_email.setEnabled(True)
            self.ui.company_comments.setEnabled(True)


    def cancel_booking(self):
        if self.guest_model.record(0).value("checked_in").toString() == '': 
            cxl_query = QSqlQuery()
            cxl_query.prepare("UPDATE bookings SET cancelled = ? WHERE id = ?")
            cxl_query.addBindValue(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            cxl_query.addBindValue(self.ui.booking_id.text())
            cxl_query.exec_()
            if cxl_query.isActive():
                QMessageBox.information(self, "Cancellations", "Booking cancelled!" )
                self.done(0)
        else:
            QMessageBox.information(self, "Cancellations", "Booking cannot be cancelled after check in.  Check out instead")


    def undo_cancellation(self):
        cxl_query = QSqlQuery()
        cxl_query.prepare("UPDATE bookings SET cancelled = NULL WHERE id = ?")
        cxl_query.addBindValue(self.ui.booking_id.text())
        cxl_query.exec_()
        if cxl_query.isActive():
            QMessageBox.information(self, "Cancellations", "Cancellation reversed.  Close the form and re-open" )



    def undo_check_out(self):

        cd = self.ui.co.text()

        co_date = QDate.fromString(cd[: cd.find(" ")], "dd/MM/yyyy")
        if co_date != hotdate:
            QMessageBox.warning(self, "Undo check-out...", "You can only undo "
                                "a check-out on the day it is checked out. \n Even then, you need "
                                "to be really sure you know what you are doing.")
            return
        uco_query = QSqlQuery()
        uco_query.prepare("UPDATE bookings SET checked_out = NULL, folio1_checked_out = NULL, folio2_checked_out = NULL, folio3_checked_out = NULL, folio4_checked_out = NULL WHERE id = ?")
        uco_query.addBindValue(self.ui.booking_id.text())
        uco_query.exec_()
        if uco_query.isActive():
            QMessageBox.information(self, "check-out", "Check out reversed.  Close the form and re-open" )

    def send_correspondence(self):
        self.update_record()

        #set up number of people
        num_grownups = {
            '0': 'no adults',
        '1': 'one adult',
        '2': 'two adults',
        '3': 'three adults',
        '4': 'four adults',
        '5': 'five adults',
        '6': 'six adults',
        '7': 'seven adults',
        '8': 'eight adults',
        '9': 'nine adults'
        }

        num_kids = {
            '0': '',
            '1': ' and one child',
            '2': ' and two children',
            '3': ' and three children',
            '4': ' and four children',
            '5': ' and five children',
            '6': ' and six children',
            '7': ' and seven children',
            '8': ' and eight children',
            '9': ' and nine children'
        }

        nums = {
            '0': 'no',
            '1': 'one',
            '2': 'two',
            '3': 'three',
            '4': 'four',
            '5': 'five',
            '6': 'six',
            '7': 'seven',
            '8': 'eight',
            '9': 'nine'
        }



        people = num_grownups[self.ui.adults.text()] + num_kids[self.ui.kids.text()]

        #format arrival date and departure date
        arr_date_py = datetime.strptime(self.ui.arrival_date.text(), '%d/%m/%Y')
        dep_date_py = datetime.strptime(self.ui.departure_date.text(), '%d/%m/%Y')
        arr_date = arr_date_py.strftime("%A, %d %B %Y").lstrip("0").replace(" 0", " ")
        dep_date = dep_date_py.strftime("%A, %d %B %Y").lstrip("0").replace(" 0", " ")


        #Sunday text
        n=dep_date_py-arr_date_py
        nights = n.days

        sunday_text = ''
        for day in range(0, nights):
            if (arr_date_py + timedelta(days = day)).weekday() > 5:
                sunday_text = """As you are staying with us on a Sunday, you will be able to enjoy 
                some live acoustic music in our restaurant as we have a different singer/songwriter 
                playing each Sunday night between 8:00 pm and10:00 pm."""

        #Room types
        #room_type_query = QSqlQuery()
        #room_type_query.prepare("SELECT id, room_types.room_type_id, room_types.description FROM bookings "
        #LEFT JOIN room_types ON bookings.room_type_id = room_types.room_type_id "
        #WHERE id=?" )


        #Room types
        room_type_query = QSqlQuery()
        room_type_query.prepare("SELECT Count(bookings.id) AS numrooms, bookings.reference, room_types.description "
                                "FROM bookings INNER JOIN room_types ON bookings.room_type_id = room_types.room_type_id "
                                "WHERE bookings.cancelled IS NULL "
                                "GROUP BY bookings.reference, room_types.description "
                                "HAVING bookings.reference=?;")


        room_type_query.addBindValue(self.ui.booking_ref.text())
        room_type_query.exec_()
        tot_num_rows = room_type_query.size()
        if self.guest_model.record(0).value("cancelled").toString("dd/MM/yyyy hh:mm") != '' and tot_num_rows > 0:
            QMessageBox.warning(self, 'Send correspondence', 'This cancelled booking is part of a group! '
                                'You must send an updated confirmation from a group member who is not cancelled!')
            return
        cur_num_rows = 0
        room_type = ""
        while room_type_query.next():
            cur_num_rows += 1
            if str(room_type_query.value("numrooms")) in nums:
                room_type = room_type + nums[str(room_type_query.value("numrooms"))] + " "
            else:
                room_type = room_type + str(room_type_query.value("numrooms")) + " "

            room_type = room_type + room_type_query.value("description")
            if room_type_query.value("numrooms") != 1:
                room_type = room_type + " rooms"
            else:
                room_type = room_type + " room"

            if tot_num_rows > cur_num_rows:
                room_type = room_type + " and "



        #Cancellation text

        no_rooms_query = QSqlQuery()
        no_rooms_query.prepare("select count(bookings.id) as numrooms FROM bookings WHERE bookings.reference=? AND cancelled IS NULL;")
        no_rooms_query.addBindValue(self.ui.booking_ref.text())
        no_rooms_query.exec_()
        no_rooms_query.first()
        if no_rooms_query.value("numrooms") > 2:
            cxl_lead_time = "pm thirty days"
        else:
            cxl_lead_time = "two days"



        if nights > 1:
            if arr_date_py.weekday() > 5:
                cxl_text = "the first two nights' rate"
            else:
                cxl_text = "the first night's rate"
        else:
            cxl_text = "the first night's rate"



        #set up rates table
        room_table_query = QSqlQuery()
        room_table_query.prepare("SELECT bookings_guests.id, bookings_guests.reference, bookings_guests.label, bookings_guests.arrival_date, bookings_guests.departure_date, "
                                 "bookings_guests.num_adults, bookings_guests.num_children, rates_stay.rate_date, rates.board_basis, "
                                 "rates_stay.amount, rates_stay.rate_code, rates.rate_description, room_types1.description "
                                 "FROM bookings_guests "
                                 "LEFT JOIN rates_stay ON bookings_guests.id = rates_stay.id_bookings "
                                 "LEFT JOIN rates ON rates_stay.rate_code = rates.rate_code "
                                 "LEFT JOIN room_types1 ON bookings_guests.room_type = room_types1.room_type "
                                 "WHERE bookings_guests.reference = ? "
                                 "AND bookings_guests.cancelled IS NULL "
                                 "ORDER BY bookings_guests.id, rates_stay.rate_date")
        room_table_query.addBindValue(self.ui.booking_ref.text())
        room_table_query.exec_()




        table = """<table border="1">
        <tr><td>Guest</td><td>Date</td><td>Adults</td><td>Children</td><td>Meal<br />plan</td><td>Room<br />type</td><td>Price</td></tr>
        """
        room_only = False
        last_guest = ""
        times = 0
        while room_table_query.next():
            times += 1
            if room_table_query.value("board_basis") == 'ROOM':
                room_only = True

            table = table + "<tr>"
            if last_guest != room_table_query.value("id"):
                if times > 1:
                    table = table + '<td colspan="7">&nbsp;</td><tr />'
                    table = table + "<tr><td>" + room_table_query.value("label") + "</td>"
                else:
                    table = table + "<td>" + room_table_query.value("label") + "</td>"
            else:
                table = table + "<td> </td>"



            last_guest = room_table_query.value("id")

            table = table + "<td>" + room_table_query.value("rate_date").toPyDate().strftime("%A, %d %B %Y").lstrip("0").replace(" 0", " ") + "</td>"
            table = table + "<td>" + str(room_table_query.value("num_adults")) + "</td>"
            table = table + "<td>" + str(room_table_query.value("num_children")) + "</td>"
            table = table + "<td>" + room_table_query.value("board_basis") + "</td>"
            table = table + "<td>" + room_table_query.value("description") + "</td>"
            table = table + "<td>" +'£' + '{0:.2f}'.format(room_table_query.value("amount")) + "</td>"
            table = table + "</tr>"

        table = table + "</table>"

        #Meal plan text
        if room_only:
            room_only_txt = """<p>You have selected a room only rate meaning breakfast is not included.  We offer an excellent cooked to 
            order breakfast with ingredients sourced from our local farmers.  You can see our menu by 
            <a href="http://theprioryinn.co.uk/restaurant/breakfast-menu.aspx">clicking here.</a>  If you would like to start you day with a proper breakfast,
            please let us know when you check-in or simply stop by the dining room in the morning.</p>"""
        else:
            room_only_txt = ""

        #Subject
        subject = "Reservation confirmation: %{confirmation}% from The Priory Inn".format(confirmation=self.ui.booking_ref.text())
        label = self.ui.guest_title.text() + " " + self.ui.guest_first_name.text() + " " + self.ui.guest_surname.text()
        label = label.strip()

        message_text = self.letter_view.model().index(self.letter_view.currentIndex(),1).data()
        message_text = message_text.format(label = label, roomtype=room_type, people=people, arrival_date=arr_date, 
                                           departure_date=dep_date, rates_table=table, 
                                           cxl_time=cxl_lead_time, cxl_policy=cxl_text,
                                           room_only_text=room_only_txt, sunday_text=sunday_text)


        email_addresses, message_text, subject, result = Email_document.get_email_stuff(self, self.res_id, message_text, subject)




        if result:

            #try:
            if sys.platform != 'linux':
                try:
                    olMailItem = 0x0
                    obj = win32com.client.Dispatch("Outlook.Application")
                    newMail = obj.CreateItem(olMailItem)
                    newMail.Subject = subject
                    newMail.HTMLBody = message_text
                    newMail.To = ';'.join(email_addresses)
                    newMail.Save()
                    msg_path = os.path.normpath("J:/documents/guests/" + self.ui.booking_ref.text() + "-" +newMail.EntryID + ".msg")
                    newMail.SaveAs(msg_path, 3)
                    newMail.Send()                
                except:
                    QMessageBox.critical(None, "Email", "An error has occurred. No confirmation was sent!")
                    return


            else:
                yag = yagmail.SMTP(user='info@theprioryinn.co.uk', password='sCSr54BoU7@', host='smtp.livemail.co.uk', smtp_starttls=False)
                yag.send(email_addresses, subject, message_text)
                msg_path = os.path.normpath(self.ui.booking_ref.text() + "-" + str(random.randint(1,99999)) + ".html")

                f = open(msg_path,'w')
                f.write(message_text)
                f.close()


            add_email_query = QSqlQuery()
            add_email_query.prepare('INSERT INTO guest_correspondence (id_bookings, datetime, "user", document_link, notes, reference) '
                                    'VALUES (?, ?, ?, ?, ?, ?)')
            add_email_query.addBindValue(self.ui.booking_id.text())
            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            add_email_query.addBindValue(trans_time)
            add_email_query.addBindValue(user)
            add_email_query.addBindValue(msg_path)
            add_email_query.addBindValue(subject)
            add_email_query.addBindValue(self.ui.booking_ref.text())
            add_email_query.exec_()
            QMessageBox.information(None, "Email", "Email sent :)")


            #except:
                #QMessageBox.critical(None, "Email Error", "Error: your email was not sent. Sorry :(")
                #return
        else:
            QMessageBox.critical(None, "Email", "No confirmation was sent!")

        self.setup_correspondence_view()




    def company_new(self):
        self.ui.company_search.setEnabled(False)
        self.ui.company_id.setEnabled(True)
        self.ui.company_id.setText('')
        self.ui.company_name.setEnabled(True)
        self.ui.company_name.setText('')
        self.ui.booker_name.setEnabled(True)
        self.ui.booker_name.setText('')
        self.ui.company_address.setEnabled(True)
        self.ui.company_address.setPlainText('')
        self.ui.company_postcode.setEnabled(True)
        self.ui.company_postcode.setText('')
        self.ui.company_country.setEnabled(True)
        self.ui.company_country.setText('')
        self.ui.company_phone.setEnabled(True)
        self.ui.company_phone.setText('')
        self.ui.company_email.setEnabled(True)
        self.ui.company_email.setText('')
        self.ui.company_comments.setEnabled(True)
        self.ui.company_comments.setPlainText('')
        row = self.companies_mod.rowCount()
        self.company_mapper.submit()
        self.companies_mod.insertRow(row)
        self.company_mapper.setCurrentIndex(row)
        self.ui.company_id.setFocus()
        




    def reg_card_print(self):


        guest_details = []
        guest_name = ''
        if self.ui.guest_title.text() != '':
            guest_name += (self.ui.guest_title.text() + ' ')
        if self.ui.guest_first_name.text() != '':
            guest_name += self.ui.guest_first_name.text() + ' '
        guest_name += self.ui.guest_surname.text()
        guest_details.append(guest_name)
        for x in self.ui.guest_address.toPlainText().splitlines():
            guest_details.append(x)
        guest_details.append(self.ui.guest_postcode.text())
        guest_details.append(self.ui.guest_country.text())

        email_add = ""
        if "booking.com" not in self.ui.guest_email.text():
            if "unknown" not in self.ui.guest_email.text():
                if "example.com" not in self.ui.guest_email.text():
                    email_add = self.ui.guest_email.text()

        ro = QSqlQuery()
        ro.exec_("select * from rates_stay where id_bookings = '{0}' and board_basis = 'RO'".format(self.ui.booking_id.text()))
        room_only = False
        if ro.size() > 0:
            room_only = True



        reg_card = printed_forms.Reg_card(guest_details=guest_details, 
                                          room_no=self.ui.room_num.text(), 
                                       start_date=self.ui.arrival_date.text(), 
                                       end_date=self.ui.departure_date.text(), 
                                       adults=self.ui.adults.text(), 
                                       kids=self.ui.kids.text(), email=email_add, room_only=room_only)

    def key_card_print(self):

        dep_date = self.ui.departure_date.date().toPyDate()
        end_date = datetime.combine(dep_date, datetime.min.time()) + timedelta(hours=13)
        key_card = printed_forms.Key_card(start_date=datetime.now(), room=self.ui.room_num.text(), 
                                          end_date=end_date, 
                                      password=self.ui.booking_id.text())




    def group_member_clicked(self, model_index):
        self.g_res_id = self.group_view.model().index(model_index.row(), 2).data()
        self.gst_form = Guest_form(self.g_res_id, self)
        self.gst_form.show()

    def correspondence_clicked(self, model_index):
        self.corres_file = self.corres_view.model().index(model_index.row(), 2).data()
        webbrowser.open(self.corres_file)        

    def disable_posting(self, ui):
        self.ui = ui
        self.ui.post_charge1.setEnabled(False)
        self.ui.post_charge2.setEnabled(False)
        self.ui.post_charge3.setEnabled(False)
        self.ui.post_charge4.setEnabled(False)
        self.ui.transfer1.setEnabled(False)
        self.ui.transfer2.setEnabled(False)
        self.ui.transfer3.setEnabled(False)
        self.ui.transfer4.setEnabled(False)
        self.ui.void_item1.setEnabled(False)
        self.ui.void_item2.setEnabled(False)
        self.ui.void_item3.setEnabled(False)
        self.ui.void_item4.setEnabled(False)
        self.ui.folio_move1.setEnabled(False)
        self.ui.folio_move2.setEnabled(False)
        self.ui.folio_move3.setEnabled(False)
        self.ui.folio_move4.setEnabled(False)
        self.ui.check_out_1.setEnabled(False)
        self.ui.check_out_2.setEnabled(False)
        self.ui.check_out_3.setEnabled(False)
        self.ui.check_out_4.setEnabled(False)
        self.ui.check_out_all.setEnabled(False)
        if self.ui.arrival_date.date() != hotdate.toPyDate():
            self.ui.check_in.setEnabled(False)


    def disable_payment(self, ui):
        self.ui.post_payment1.setEnabled(False)
        self.ui.post_payment2.setEnabled(False)
        self.ui.post_payment3.setEnabled(False)
        self.ui.post_payment4.setEnabled(False)


    def print_fol(self, person="", folio_num=0, trans_date="", res_ref = ""):
        print_me = Printfolio(person=user, folio=folio_num, trans_date=hotdate, res_ref=self.res_id, address=self.ui.which_address.currentText())
        print_me.print_fol(prn_res_ref = self.res_id)


    def email_fol(self, person="", folio_num=0, trans_date="", res_ref = ""):
        print_me = Printfolio(person=user, folio=folio_num, trans_date=hotdate, res_ref=self.res_id, address=self.ui.which_address.currentText(), email_fol=True)
        print_me.email_fol(prn_res_ref = self.res_id)



    def setup_group_view(self): 
        group_ref = self.guest_model.record(0).value("reference")
        self.group_model = QSqlQueryModel(self)
        self.group_model.setQuery("SELECT room, label, id, arrival_date, departure_date from bookings_guests "
                                  "WHERE cancelled IS NULL "
                                  "AND reference = '{0}'".format(group_ref))
        self.group_view = self.ui.group_members
        self.group_view.setModel(self.group_model)
        self.group_view.setColumnHidden(2, True)
        self.group_view.resizeColumnsToContents()

    def setup_correspondence_view(self): 
        corres_id = self.guest_model.record(0).value("reference")
        self.corres_model = QSqlQueryModel(self)
        self.corres_model.setQuery("select datetime as Date, user as User, document_link, notes as Notes from guest_correspondence where reference = '{0}' ORDER BY datetime DESC".format(corres_id))

        self.corres_view = self.ui.guest_correspondence
        self.corres_view.setModel(self.corres_model)
        self.corres_view.setColumnHidden(2, True)
        self.corres_view.resizeColumnsToContents()
        self.corres_view.setSelectionBehavior(QTableView.SelectRows)



    def setup_rates_view(self):
        self.rates_model = QSqlRelationalTableModel(self)
        self.rates_model.setTable("rates_stay")
        self.rates_model.setRelation(6, QSqlRelation("rates", "rate_code", "rate_description"))
        self.rates_model.setFilter("id_bookings = '{0}'".format(self.res_id))
        self.rates_model.setSort(1, Qt.AscendingOrder)
        self.rates_model.setHeaderData(1, Qt.Horizontal, "Date")
        self.rates_model.setHeaderData(2, Qt.Horizontal, "Basis")
        self.rates_model.setHeaderData(6, Qt.Horizontal, "Code")
        self.rates_model.setHeaderData(4, Qt.Horizontal, "Amount")

        self.rates_model.select()
        self.rates_view = self.ui.rates
        self.rates_view.setModel(self.rates_model)
        self.rates_view.setItemDelegate(QSqlRelationalDelegate(self.rates_view))
        self.rates_view.setColumnHidden(0, True)
        self.rates_view.setColumnHidden(3, True)
        self.rates_view.setColumnHidden(5, True)
        self.rates_view.horizontalHeader().moveSection(4,6)
        #self.rates_view.resizeColumnsToContents()
        self.rates_view.setColumnWidth(6, 167)
        self.rates_view.setItemDelegateForColumn(4, FloatDelegate(2))
        self.rates_view.setEditTriggers(QAbstractItemView.AllEditTriggers)
        self.rates_view.setItemDelegate(Viewrates(self))

        #print("Finished setting up rates veiw")


    def setup_folio1(self):
        self.folio1_model = QSqlQueryModel(self)
        self.folio1_model.setQuery("SELECT trans_timestamp, person, charge_code, description, dr, cr, id, dr_tax, cr_tax from "
                                   " transactions where folio = 1 and account = '{0}' ORDER BY trans_timestamp".format(self.res_id))
        self.folio1_model.setHeaderData(0, Qt.Horizontal, "Time stamp")
        self.folio1_model.setHeaderData(1, Qt.Horizontal, "User")
        self.folio1_model.setHeaderData(2, Qt.Horizontal, "Charge code")
        self.folio1_model.setHeaderData(3, Qt.Horizontal, "Description")
        self.folio1_model.setHeaderData(4, Qt.Horizontal, "Debit")
        self.folio1_model.setHeaderData(5, Qt.Horizontal, "Credit")

        self.folio1_view = self.ui.folio1_1
        self.folio1_view.setModel(self.folio1_model)
        self.col_4 = FloatDelegate(2)
        self.col_5 = FloatDelegate(2)

        self.folio1_view.setItemDelegateForColumn(4, self.col_4)
        self.folio1_view.setItemDelegateForColumn(5, self.col_5)        
        self.folio1_view.setColumnWidth(0, 120)
        self.folio1_view.setColumnWidth(1, 80)
        self.folio1_view.setColumnWidth(2, 80)
        self.folio1_view.setColumnWidth(3, 345)
        self.folio1_view.setColumnWidth(4, 50)
        self.folio1_view.setColumnWidth(5, 50)
        self.folio1_view.setColumnHidden(6, True)
        self.folio1_view.setColumnHidden(7, True)
        self.folio1_view.setColumnHidden(8, True)
        self.folio1_view.setSelectionBehavior(QTableView.SelectRows)
        self.folio1_view.setDragEnabled(True)



        cr_total = dr_total  = 0
        for i in range(self.folio1_model.rowCount()):
            dr_total = dr_total + self.folio1_model.index(i,4).data()
            cr_total = cr_total + self.folio1_model.index(i,5).data()
        self.ui.folio1_balance.setText("{:.2f}".format(dr_total - cr_total))
        self.ui.folio1_balance.setAlignment(Qt.AlignRight)
        self.ui.folios.setTabText(0, "Folio 1 £" + "{:.2f}".format(dr_total - cr_total))

        #if self.ui.folio1_co.text() != '':
            #self.ui.folio1.setEnabled(False)

        self.ui.total_folios.setText("{:.2f}".format(float(self.ui.folio1_balance.text())+float(self.ui.folio2_balance.text())+float(self.ui.folio3_balance.text())+float(self.ui.folio4_balance.text())))
        self.ui.total_folios.setAlignment(Qt.AlignRight)


    def setup_folio2(self):
        self.folio2_model = QSqlQueryModel(self)
        self.folio2_model.setQuery("SELECT trans_timestamp, person, charge_code, description, dr, cr, id, dr_tax, cr_tax from "
                                   " transactions where folio = 2 and account = '{0}'".format(self.res_id))
        self.folio2_model.setHeaderData(0, Qt.Horizontal, "Time stamp")
        self.folio2_model.setHeaderData(1, Qt.Horizontal, "User")
        self.folio2_model.setHeaderData(2, Qt.Horizontal, "Charge code")
        self.folio2_model.setHeaderData(3, Qt.Horizontal, "Description")
        self.folio2_model.setHeaderData(4, Qt.Horizontal, "Debit")
        self.folio2_model.setHeaderData(5, Qt.Horizontal, "Credit")
        self.folio2_view = self.ui.folio2_2
        self.folio2_view.setModel(self.folio2_model)


        self.folio2_view.setItemDelegateForColumn(4, self.col_4)
        self.folio2_view.setItemDelegateForColumn(5, self.col_5)        

        self.folio2_view.setColumnWidth(0, 120)
        self.folio2_view.setColumnWidth(1, 80)
        self.folio2_view.setColumnWidth(2, 80)
        self.folio2_view.setColumnWidth(3, 395)
        self.folio2_view.setColumnWidth(4, 50)
        self.folio2_view.setColumnWidth(5, 50)
        self.folio2_view.setColumnHidden(6, True)
        self.folio2_view.setColumnHidden(7, True)
        self.folio2_view.setColumnHidden(8, True)
        self.folio2_view.setSelectionBehavior(QTableView.SelectRows)
        self.folio2_view.setAcceptDrops(True)

        cr_total = dr_total  = 0
        for i in range(self.folio2_model.rowCount()):
            dr_total = dr_total + self.folio2_model.index(i,4).data()
            cr_total = cr_total + self.folio2_model.index(i,5).data()
        self.ui.folio2_balance.setText("{:.2f}".format(dr_total - cr_total))
        self.ui.folios.setTabText(1, "Folio 2 £" + "{:.2f}".format(dr_total - cr_total))

        #if self.ui.folio2_co.text() != '':
            #self.ui.folio2.setEnabled(False)

        self.ui.total_folios.setText("{:.2f}".format(float(self.ui.folio1_balance.text())+float(self.ui.folio2_balance.text())+float(self.ui.folio3_balance.text())+float(self.ui.folio4_balance.text())))
        self.ui.total_folios.setAlignment(Qt.AlignRight)


    def setup_folio3(self):
        self.folio3_model = QSqlQueryModel(self)
        self.folio3_model.setQuery("SELECT trans_timestamp, person, charge_code, description, dr, cr, id, dr_tax, cr_tax from "
                                   " transactions where folio = 3 and account = '{0}'".format(self.res_id))
        self.folio3_model.setHeaderData(0, Qt.Horizontal, "Time stamp")
        self.folio3_model.setHeaderData(1, Qt.Horizontal, "User")
        self.folio3_model.setHeaderData(2, Qt.Horizontal, "Charge code")
        self.folio3_model.setHeaderData(3, Qt.Horizontal, "Description")
        self.folio3_model.setHeaderData(4, Qt.Horizontal, "Debit")
        self.folio3_model.setHeaderData(5, Qt.Horizontal, "Credit")
        self.folio3_view = self.ui.folio3_3
        self.folio3_view.setModel(self.folio3_model)
        

        self.folio3_view.setItemDelegateForColumn(4, self.col_4)
        self.folio3_view.setItemDelegateForColumn(5, self.col_5)        
        

        self.folio3_view.setColumnWidth(0, 120)
        self.folio3_view.setColumnWidth(1, 80)
        self.folio3_view.setColumnWidth(2, 80)
        self.folio3_view.setColumnWidth(3, 395)
        self.folio3_view.setColumnWidth(4, 50)
        self.folio3_view.setColumnWidth(5, 50)
        self.folio3_view.setColumnHidden(6, True)
        self.folio3_view.setColumnHidden(7, True)
        self.folio3_view.setColumnHidden(8, True)
        self.folio3_view.setSelectionBehavior(QTableView.SelectRows)

        cr_total = dr_total  = 0
        for i in range(self.folio3_model.rowCount()):
            dr_total = dr_total + self.folio3_model.index(i,4).data()
            cr_total = cr_total + self.folio3_model.index(i,5).data()
        self.ui.folio3_balance.setText("{:.2f}".format(dr_total - cr_total))

        self.ui.folios.setTabText(2, "Folio 3 £" + "{:.2f}".format(dr_total - cr_total))

        #if self.ui.folio3_co.text() != '':
            #self.ui.folio3.setEnabled(False)

        self.ui.total_folios.setText("{:.2f}".format(float(self.ui.folio1_balance.text())+float(self.ui.folio2_balance.text())+float(self.ui.folio3_balance.text())+float(self.ui.folio4_balance.text())))
        self.ui.total_folios.setAlignment(Qt.AlignRight)


    def setup_folio4(self):
        self.folio4_model = QSqlQueryModel(self)
        self.folio4_model.setQuery("SELECT trans_timestamp, person, charge_code, description, dr, cr, id, dr_tax, cr_tax from "
                                   "transactions where folio = 4 and account = '{0}'".format(self.res_id))
        self.folio4_model.setHeaderData(0, Qt.Horizontal, "Time stamp")
        self.folio4_model.setHeaderData(1, Qt.Horizontal, "User")
        self.folio4_model.setHeaderData(2, Qt.Horizontal, "Charge code")
        self.folio4_model.setHeaderData(3, Qt.Horizontal, "Description")
        self.folio4_model.setHeaderData(4, Qt.Horizontal, "Debit")
        self.folio4_model.setHeaderData(5, Qt.Horizontal, "Credit")

        self.folio4_view = self.ui.folio4_4
        self.folio4_view.setModel(self.folio4_model)
        
        self.folio4_view.setItemDelegateForColumn(4, self.col_4)
        self.folio4_view.setItemDelegateForColumn(5, self.col_5)        
        
        self.folio4_view.setColumnWidth(0, 120)
        self.folio4_view.setColumnWidth(1, 80)
        self.folio4_view.setColumnWidth(2, 80)
        self.folio4_view.setColumnWidth(3, 395)
        self.folio4_view.setColumnWidth(4, 50)
        self.folio4_view.setColumnWidth(5, 50)
        self.folio4_view.setColumnHidden(6, True)
        self.folio4_view.setColumnHidden(7, True)
        self.folio4_view.setColumnHidden(8, True)
        self.folio4_view.setSelectionBehavior(QTableView.SelectRows)

        cr_total = dr_total  = 0
        for i in range(self.folio4_model.rowCount()):
            dr_total = dr_total + self.folio4_model.index(i,4).data()
            cr_total = cr_total + self.folio4_model.index(i,5).data()
        self.ui.folio4_balance.setText("{:.2f}".format(dr_total - cr_total)) 
        self.ui.folios.setTabText(3, "Folio 4 £" + "{:.2f}".format(dr_total - cr_total))

        #if self.ui.folio4_co.text() != '':
            #self.ui.folio4.setEnabled(False)

        self.ui.total_folios.setText("{:.2f}".format(float(self.ui.folio1_balance.text())+float(self.ui.folio2_balance.text())+float(self.ui.folio3_balance.text())+float(self.ui.folio4_balance.text())))
        self.ui.total_folios.setAlignment(Qt.AlignRight)




    def populate_gst(self, guest_id):
        #print(self.guest_model.record(0).value("guest_id"))
        self.guest_individual = QSqlTableModel(self)
        self.guest_individual.setTable("guests")
        #self.guest_individual.setFilter("id = '{0}'".format(self.guest_model.record(0).value("guest_id")))
        self.guest_individual.setFilter("id = '{0}'".format(guest_id))
        self.guest_individual.select()
        self.guest_mapper = QDataWidgetMapper(self)
        self.guest_mapper.setModel(self.guest_individual)
        self.guest_mapper.addMapping(self.ui.guest_title, 1)
        self.guest_mapper.addMapping(self.ui.guest_first_name, 2)
        self.guest_mapper.addMapping(self.ui.guest_surname, 3)
        self.guest_mapper.addMapping(self.ui.guest_address, 4)
        self.guest_mapper.addMapping(self.ui.guest_postcode, 5)
        self.guest_mapper.addMapping(self.ui.guest_country, 6)
        self.guest_mapper.addMapping(self.ui.guest_phone, 7)
        self.guest_mapper.addMapping(self.ui.guest_email, 8)
        self.guest_mapper.addMapping(self.ui.guest_comments, 9)
        self.guest_mapper.toFirst()
        self.ui.booker_info_tab_widget.setTabText(0, "Guest: " + self.ui.guest_surname.text())


    def populate_companies(self, company_id):
        self.companies_mod = QSqlTableModel(self)
        self.companies_mod.setTable("companies")
        self.companies_mod.setFilter("company_id = '{0}'".format(company_id))
        self.companies_mod.select()
        self.company_mapper = QDataWidgetMapper(self)
        self.company_mapper.setModel(self.companies_mod)
        self.company_mapper.addMapping(self.ui.company_id, self.companies_mod.fieldIndex("company_id"))
        self.company_mapper.addMapping(self.ui.company_name, self.companies_mod.fieldIndex("company_name"))
        self.company_mapper.addMapping(self.ui.booker_name,  self.companies_mod.fieldIndex("booker"))
        self.company_mapper.addMapping(self.ui.company_address,  self.companies_mod.fieldIndex("address"))
        self.company_mapper.addMapping(self.ui.company_postcode,  self.companies_mod.fieldIndex("postcode"))
        self.company_mapper.addMapping(self.ui.company_country,  self.companies_mod.fieldIndex("country"))
        self.company_mapper.addMapping(self.ui.company_phone,  self.companies_mod.fieldIndex("phone1"))
        self.company_mapper.addMapping(self.ui.company_email,  self.companies_mod.fieldIndex("email"))
        self.company_mapper.addMapping(self.ui.company_comments,  self.companies_mod.fieldIndex("comments"))
        self.company_mapper.toFirst()
        self.ui.booker_info_tab_widget.setTabText(1, "Company: " + self.ui.company_id.text())












    def populate_companies_old(self):

        self.companies_model = QSqlRelationalTableModel(self)
        self.companies_model.setTable("companies")
        self.companies_model.setRelation(COMPANY_ID,QSqlRelation("companies", "company_id", "company_name"))
        self.companies_model.setSort(1, Qt.AscendingOrder)
        self.companies_model.select()
        self.mapper.setItemDelegate(QSqlRelationalDelegate(self))

        view = QTableView()

        view.setSelectionBehavior(QTableView.SelectRows)
        self.ui.company_id.setView(view)

        self.companies_view = self.ui.company_id
        self.companies_view.setModel(self.companies_model)
        self.ui.company_id.setModelColumn(self.companies_model.fieldIndex("company_id"))

        for i in range(2, self.companies_model.columnCount()):
            view.setColumnHidden(i, True)

        view.resizeColumnsToContents()
        view.horizontalHeader().setStretchLastSection(True)
        self.mapper.addMapping(self.ui.company_id, self.companies_model.fieldIndex("company_id"))
        self.ui.booker_info_tab_widget.setTabText(1, "Company: " + self.ui.company_id.currentText())    #This isn't working


    def populate_ledgers(self):

        self.ledger_model = QSqlRelationalTableModel(self)
        self.ledger_model.setTable("ledger_accounts")
        self.ledger_model.setRelation(LEDGER_ID,QSqlRelation("ledger_accounts", "ledger_id", "ledger_name"))
        self.ledger_model.select()
        self.mapper.setItemDelegate(QSqlRelationalDelegate(self))
        self.ledger_model.sort(1, Qt.AscendingOrder)

        view = QTableView()


        view.setSelectionBehavior(QTableView.SelectRows)
        self.ui.ledger_account.setView(view)

        self.ledger_view = self.ui.ledger_account
        self.ledger_view.setModel(self.ledger_model)
        self.ui.ledger_account.setModelColumn(self.ledger_model.fieldIndex("ledger_id"))



        for i in range(2, self.ledger_model.columnCount()):
            view.setColumnHidden(i, True)

        view.resizeColumnsToContents()
        view.horizontalHeader().setStretchLastSection(True)

        self.mapper.addMapping(self.ui.ledger_account, LEDGER_ID)
        self.ui.booker_info_tab_widget.setTabText(3, "Ledger: " + self.ui.ledger_account.currentText())




    def populate_newspapers(self):

        self.newspapers_model = QSqlRelationalTableModel(self)
        self.newspapers_model.setTable("newspapers")
        self.newspapers_model.setRelation(NEWSPAPER,QSqlRelation("newspapers", "newspaper", "newspaper"))
        self.newspapers_model.setSort(0, Qt.AscendingOrder)
        self.newspapers_model.select()
        self.mapper.setItemDelegate(QSqlRelationalDelegate(self))

        view = QTableView()

        view.setSelectionBehavior(QTableView.SelectRows)
        self.ui.newspaper.setView(view)

        self.newspapers_view = self.ui.newspaper
        self.newspapers_view.setModel(self.newspapers_model)
        self.ui.newspaper.setModelColumn(self.newspapers_model.fieldIndex("newspaper"))

        for i in range(1, self.newspapers_model.columnCount()):
            view.setColumnHidden(i, True)


        view.resizeColumnsToContents()
        view.horizontalHeader().setStretchLastSection(True)


        self.mapper.addMapping(self.ui.newspaper, 32)        



    def company_info(self):
        self.ui.company_address.setPlainText(self.companies_view.model().index(self.companies_view.currentIndex(), 2).data())
        self.ui.company_postcode.setText(self.companies_view.model().index(self.companies_view.currentIndex(), 3).data())
        self.ui.company_email.setText(self.companies_view.model().index(self.companies_view.currentIndex(), 5).data())
        self.ui.company_country.setText(self.companies_view.model().index(self.companies_view.currentIndex(), 7).data())
        self.ui.company_phone.setText(self.companies_view.model().index(self.companies_view.currentIndex(), 4).data())
        self.ui.booker_name.setText(self.companies_view.model().index(self.companies_view.currentIndex(), 9).data())
        self.ui.company_comments.setPlainText(self.companies_view.model().index(self.companies_view.currentIndex(), 6).data())
        self.ui.booker_info_tab_widget.setTabText(1, "Company: " + self.ui.company_id.currentText())


    def ledger_info(self):
        self.ui.ledger_address.setText(self.ledger_view.model().index(self.ledger_view.currentIndex(), 2).data())
        self.ui.ledger_name.setText(self.ledger_view.model().index(self.ledger_view.currentIndex(), 1).data())
        self.ui.ledger_postcode.setText(self.ledger_view.model().index(self.ledger_view.currentIndex(), 3).data())
        self.ui.ledger_email.setText(self.ledger_view.model().index(self.ledger_view.currentIndex(), 5).data())
        self.ui.ledger_country.setText(self.ledger_view.model().index(self.ledger_view.currentIndex(), 7).data())
        self.ui.ledger_phone.setText(self.ledger_view.model().index(self.ledger_view.currentIndex(), 4).data())
        self.ui.ledger_contact.setText(self.ledger_view.model().index(self.ledger_view.currentIndex(), 9).data())
        self.ui.ledger_comments.setPlainText(self.ledger_view.model().index(self.ledger_view.currentIndex(), 6).data())
        if self.ledger_view.model().index(self.ledger_view.currentIndex(), 1).data() != " ":
            index = self.ui.which_address.findText("Ledger", QtCore.Qt.MatchFixedString)
            if index >= 0:
                self.ui.which_address.setCurrentIndex(index)      

        self.ui.booker_info_tab_widget.setTabText(3, "Ledger: " + self.ui.ledger_account.currentText())



    def update_record(self):
        row  = self.mapper.currentIndex()
        if self.ui.guest_surname.text() == '':
            QMessageBox.warning(self, 'Update booking...','You must have a guest surname to close the booking')
            return

        submit = self.mapper.submit()
        submit1 = self.guest_mapper.submit()
        if self.companies_mod.record(0).value("company_id") is not None:
            submit2 = self.company_mapper.submit()
            print("company_mapper", submit2)
            if submit2 == False:
                print(self.guest_model.lastError().text())
                QMessageBox.critical(None, "Saving Error!", "Sorry, I couldn't save your company changes.  The computer stuff is: " + self.companies_mod.lastError().text())
                return    

        print("mapper: ", submit)
        print("guest_mapper", submit1)        

        if submit == False:
            print(self.guest_model.lastError().text())
            QMessageBox.critical(None, "Saving Error!", "Sorry, I couldn't save your booking changes.  The computer stuff is: " + self.guest_model.lastError().text())
            return
        if submit1 == False:
            print(self.guest_model.lastError().text())
            QMessageBox.critical(None, "Saving Error!", "Sorry, I couldn't save your guest changes.  The computer stuff is: " + self.guest_model.lastError().text())
            return

        else:
            query = QSqlQuery()
            SQL = ("UPDATE bookings "
                   "SET company_id = ?"
                   "WHERE id = ?")
            query.prepare(SQL)
            query.addBindValue(self.ui.company_id.text())
            query.addBindValue(self.res_id)
            query.exec_()
            if not query.isActive():
                QMessageBox.critical(None, "Saving Error!", "Sorry, I couldn't save your company to the booking.  The computer stuff is: " + query.lastError().text())
                return



    def populate_guest_letters(self):
        self.letters_model = QSqlTableModel(self)
        self.letters_model.setTable("guest_letters")
        if self.guest_model.record(0).value("cancelled").toString() == '':
            area = 'reservations'
        else:
            area = 'cancellations'

        self.letters_model.setFilter("area = '{0}'".format(area))
        self.letters_model.select()
        self.ui.guest_letters_cbo.setModel(self.letters_model)
        #self.ui.guest_letters_cbo.setModelColumn(self.letters_model.fieldIndex("letter"))

        view = QTableView()
        view.setSelectionBehavior(QTableView.SelectRows)
        self.ui.guest_letters_cbo.setView(view)

        self.letter_view = self.ui.guest_letters_cbo
        self.letter_view.setModel(self.letters_model)
        self.ui.guest_letters_cbo.setModelColumn(self.letters_model.fieldIndex("letter"))



        #view.setColumnHidden(1, True)




    def post_charges(self, folio_num):
        #print(folio_num)
        self.post_form = Posting_form(self, folio_num=folio_num, res_id=self.res_id, parent=self)
        #self.post_form.setModal(True)
        #self.post_form.show()
        self.post_form.exec_()

    def post_payments(self, folio_num):
        #print(folio_num)
        self.paying_form = Paying_form(self, folio_num=folio_num, res_id=self.res_id, parent=self)
        #self.paying_form.show()
        self.paying_form.exec_()

    def transfer_transactions(self, folio_num, model_indexes, folio_view):
        if model_indexes == []:
            QMessageBox.warning(self, "Transfer transactions", "How can I transfer anything if you don't select the transactions?")
            return
        self.transfer_form = Transfer_form(parent=None, folio_num=folio_num, model_indexes=model_indexes, guest_form=self, folio_view=folio_view, res_id=self.res_id)
        self.transfer_form.setModal(True)
        self.transfer_form.show()


    def void_transactions(self, folio_num, model_indexes, folio_view):
        trans_id_dict = {}
        for model_index in model_indexes:
            trans_id = folio_view.model().index(model_index.row(), 6).data()
            if trans_id not in trans_id_dict:
                trans_id_dict[trans_id] = [folio_view.model().index(model_index.row(), 3).data(), folio_view.model().index(model_index.row(), 2).data(), 
                                           folio_view.model().index(model_index.row(), 4).data(), folio_view.model().index(model_index.row(), 5).data(), 
                                               folio_view.model().index(model_index.row(), 2).data(), folio_view.model().index(model_index.row(), 7).data(), 
                                               folio_view.model().index(model_index.row(), 8).data()]

        QSqlDatabase.database().transaction()

        for trans_id in trans_id_dict:

            # Don't void payments!
            pay_type_query = QSqlQuery()
            pay_type_query.prepare("select charge_group from charge_codes where charge_code=?")
            pay_type_query.addBindValue(trans_id_dict[trans_id][1])
            pay_type_query.exec_()
            pay_type_query.first()
            if pay_type_query.isActive():
                if pay_type_query.value("charge_group") == "Payments":
                    QMessageBox.warning(self, "Cannot Void!",  "Cannot void a payment!  Please post a debit instead.")
                    continue

            #Don't void Interface items
            post_type_query = QSqlQuery()
            post_type_query.prepare("SELECT user_ok FROM charge_codes WHERE charge_code=?")
            post_type_query.addBindValue(trans_id_dict[trans_id][1])
            post_type_query.exec_()
            post_type_query.first()
            if post_type_query.isActive():
                if not post_type_query.value("user_ok"):
                    QMessageBox.warning(self, "Cannot Void!",  "Cannot void an interface charge!  Please use the interface instead.")
                    continue

            query = QSqlQuery()
            query.prepare("UPDATE transactions set folio=? WHERE ID=?")
            query.addBindValue('4')
            query.addBindValue(trans_id)
            query.exec_()
            if not query.isActive():
                QSqlDatabase.database().rollback()
                print(query.lastError().text())
                QMessageBox.warning(self, "Transfer failed!",  query.lastError().text())
                return

            desc = trans_id_dict[trans_id][0]
            desc = "VOID: " + desc
            charge_code = trans_id_dict[trans_id][1]
            dr = trans_id_dict[trans_id][2]
            cr = trans_id_dict[trans_id][3]
            account = trans_id_dict[trans_id][4]
            dr_tax = trans_id_dict[trans_id][5]
            cr_tax = trans_id_dict[trans_id][6]

            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, dr, dr_tax, folio, account, hotdate, ledger_id) "
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            query.addBindValue(trans_time)
            query.addBindValue(user)
            query.addBindValue(charge_code)
            query.addBindValue(desc)
            query.addBindValue(dr)
            query.addBindValue(dr_tax)
            query.addBindValue(cr)
            query.addBindValue(cr_tax)
            query.addBindValue(4)
            query.addBindValue(self.res_id)
            query.addBindValue(hotdate.toString('yyyy-MM-dd'))
            query.addBindValue("RGL")
            query.exec_()
            if not query.isActive():
                QSqlDatabase.database().rollback()
                print(query.lastError().text()) 
                QMessageBox.warning("Void failed!" + query.lastError().text(), "Failure!")
                return

            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, dr, dr_tax, folio, account, hotdate, ledger_id) "
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            query.addBindValue(trans_time)
            query.addBindValue(user)
            query.addBindValue(self.res_id)
            query.addBindValue(desc)
            query.addBindValue(cr)
            query.addBindValue(cr_tax)
            query.addBindValue(dr)
            query.addBindValue(dr_tax)
            query.addBindValue(4)
            query.addBindValue(charge_code)
            query.addBindValue(hotdate.toString('yyyy-MM-dd'))
            query.addBindValue("RGL")
            query.exec_()
            if not query.isActive():
                QSqlDatabase.database().rollback()
                print(query.lastError().text()) 
                QMessageBox.warning("Void failed!" + query.lastError().text(), "Failure!")
                return



            QSqlDatabase.database().commit()



        self.setup_folio1()    
        self.setup_folio2()    
        self.setup_folio3()    
        self.setup_folio4()    

    def move_transactions(self, folio_num, model_indexes, folio_view):

        fol_num, ok = QInputDialog.getText(self, 'Folio move', 'Enter the folio number')
        if not ok:
            return
        if fol_num not in ('1','2','3','4'):
            return
        current_row = -1
        trans_id_list = []
        for model_index in model_indexes:
            trans_id = folio_view.model().index(model_index.row(), 6).data()
            if trans_id not in trans_id_list:
                trans_id_list.append(trans_id)

        for trans_id in trans_id_list:
            query = QSqlQuery()
            query.prepare("UPDATE transactions set folio=? WHERE ID=?")
            query.addBindValue(fol_num)
            query.addBindValue(trans_id)
            query.exec_()
            if not query.isActive():
                print(query.lastError().text())
                QMessageBox.warning("Move failed!" + query.lastError().text(), "Failure!")
            else:
                self.setup_folio1()    
                self.setup_folio2()    
                self.setup_folio3()    
                self.setup_folio4()    


    def check_in(self):
        if self.ui.ci_co.text() != "":
            QMessageBox.warning(self, "Check in", "Whoa! This one seems to already be checked in.")
            return

        if "@guest.booking.com" in self.ui.guest_email.text():
            QMessageBox.warning(self, "Check in", "Hey, you really need to change the Booking.com Email address before you check them in!")
            return

        if "@unknown.org" in self.ui.guest_email.text():
            QMessageBox.warning(self, "Check in", "Hey, you really need to change the unknown.org Email address before you check them in!")
            return
        if self.ui.guest_email.text()=="":
            QMessageBox.warning(self, "Check in", "Hey, you know that you have to put an email address in.  Do you job and get the email address!")
            return

        query = QSqlQuery()
        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
        query.exec_("UPDATE bookings set checked_in='{0}' WHERE ID='{1}'".format(trans_time, self.res_id))
        if not query.isActive():
            print(query.lastError().text())
            QMessageBox.warning("Check in failed!" + query.lastError().text(), "Failure!")
            return
        self.ui.ci_co.setText(trans_time)


        dep_query = QSqlQuery()

        dr = True
        self.person = user
        self.charge_code = self.res_id
        self.description = "Deposit used"
        self.trans_date = hotdate
        self.res_ref = self.res_id
        SQL = "SELECT id, trans_timestamp, person, charge_code, description, dr, dr_tax, cr, cr_tax, folio, account, hotdate, ledger_id FROM transactions WHERE account = '{0}' AND ledger_id='{1}'".format(self.res_id, "DEP")

        dep_query.exec_(SQL)
        while dep_query.next():
            self.amount = dep_query.value("cr")
            self.folio = 4

            query = QSqlQuery()
            QSqlDatabase.database().transaction()
            if dr:
                query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, dr, dr_tax, folio, account, hotdate, ledger_id)"
                              "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            else:
                query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, folio, account, hotdate, ledger_id)"
                              "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            query.addBindValue(trans_time)
            query.addBindValue(self.person)
            query.addBindValue(self.charge_code)
            query.addBindValue(self.description)
            query.addBindValue(self.amount)
            query.addBindValue(0)
            query.addBindValue(self.folio)
            query.addBindValue(self.res_ref)
            query.addBindValue(self.trans_date)
            query.addBindValue("DEP")

            query.exec_()
            if not query.isActive():
                print(query.lastError().text())
                QSqlDatabase.database().rollback()
                QMessageBox.critical(None, "Check-in Error!", query.lastError().text())
                return

            if dr:
                query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, folio, account, hotdate, ledger_id)"
                              "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            else:
                query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, dr, dr_tax, folio, account, hotdate, ledger_id)"
                              "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            query.addBindValue(trans_time)
            query.addBindValue(self.person)
            query.addBindValue(self.res_ref)
            query.addBindValue(self.description)
            query.addBindValue(self.amount)
            query.addBindValue(0)
            query.addBindValue(1)
            query.addBindValue(self.charge_code)
            query.addBindValue(self.trans_date)
            query.addBindValue("RGL")
            query.exec_()
            if not query.isActive():
                print(query.lastError().text())
                QSqlDatabase.database().rollback()
                QMessageBox.critical(None, "Check-in Error!", query.lastError().text())
                self.close()
                return

            dep_fol_move = QSqlQuery()
            SQL = "UPDATE transactions SET folio=4 WHERE id = '{0}'".format(dep_query.value("id"))
            dep_fol_move.exec_(SQL)

            if not dep_fol_move.isActive():
                print(dep_fol_move.lastError().text())
                QSqlDatabase.datbase().rollback()
                QMessageBox.critical(None, "Check-in Error!", query.lastError().text())

            QSqlDatabase.database().commit()





        myapp.populate_mainform()


    def check_out_now(self, folio_num=0):
        if self.ui.co.text() != "":
            QMessageBox.warning(self, "Check out", "Hey! This guest is already checked out.  Once is enough!")
            return

        if self.ui.ci_co.text() == "":
            QMessageBox.warning(self, "Check out", "Can't check out! You cannot check out a guest until they are checked in.")
            return
        if self.ui.departure_date.date() != hotdate:
            reply = QMessageBox.question(self, 'Check out',
                                         "This guest is not due to depart until " + self.ui.departure_date.text() + ". Are you sure you want to check them out early?", QMessageBox.Yes | 
                QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.No:
                return

        self.update_record()
        co_form = Check_out(self, folio_num=folio_num, res_id=self.res_id, parent=self)
        co_form.setModal(True)
        co_form.exec_()
        myapp.populate_mainform()


    def keyPressEvent(self, event):
        if not event.key() == Qt.Key_Escape:
            super(Guest_form, self).keyPressEvent(event)    

    def closeEvent(self, event):
        if self.guest_id !=0:
            self.update_record()
        else:
            QMessageBox.warning(self, 'Reservations', 'You cannot create a reservation without a guest! \n If you do not need this booking, use the "Abandon" button')
            event.ignore()
            return

        if self.ui.guest_surname.text() == '':
            QMessageBox.warning(self, 'Reservations', 'You cannot create a reservation without a guest surname! \n If you do not need this booking, use the "Abandon" button')
            event.ignore()
            return



        event.accept() # let the window close



class Guest_correspondence(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self)
        self.ui = Ui_guest_correspondence()

        self.ui.setupUi(self)
        self.guests_model = QSqlQueryModel(self)
        self.guests_model.setQuery("SELECT arrival_date, departure_date, label, bookings_guests.reference, "
                                   "guest_correspondence.reference, bookings_guests.id, guest_correspondence.datetime from bookings_guests "
                                   "LEFT JOIN guest_correspondence on bookings_guests.reference = guest_correspondence.reference "
                                   "WHERE guest_correspondence.reference IS NULL AND bookings_guests.cancelled IS NULL AND departure_date > '{departure}' AND channel_ref <> 'EXP'"
                                   "order by arrival_date ".format(departure=hotdate.toString(Qt.ISODate)))
        self.view = self.ui.guest_view
        self.view.setSelectionBehavior(QTableView.SelectRows)
        self.view.setModel(self.guests_model)
        self.view.setColumnHidden(4, True)
        self.view.setColumnHidden(6, True)
        self.setStyleSheet("background-color: " + bg_colour)
        self.view.clicked.connect(self.group_member_clicked)
        header_labels = ['Arrival date', 'Departure date', 'Name', 'Group reference', 'Booking id']
        #self.guests_model.setHorizontalHeaderLabels(header_labels)
        self.ui.guest_view.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        




    def group_member_clicked(self, model_index):
        self.g_res_id = self.view.model().index(model_index.row(), 5).data()
        self.gst_form = Guest_form(self.g_res_id, self)
        self.gst_form.exec_()


class View_room_config(QSqlRelationalDelegate):
    def __init__(self, parent=None):
        QItemDelegate.__init__(self)

    def createEditor(self, parent, option, index):
        if index.column() in [1, 2]:
            combo = super().createEditor(parent, option, index)
 

class Viewrates(QSqlRelationalDelegate):
    def __init__(self, parent=None):
        QItemDelegate.__init__(self)

    def createEditor(self, parent, option, index):
        if index.column() == 4:
            spinbox = QSpinBox(parent)
            spinbox.setRange(0,10000)
            return spinbox
        elif index.column() == 6:
            combo = super().createEditor(parent, option, index)
            #self.combo.currentIndexChanged.connect(self.update_info_fields)
            return combo

    def update_info_fields(self):
        pass

class Spin_delegates(object):
    pass




class Posting_form(QDialog):
    def __init__(self, guest_form, folio_num, res_id, parent=None):
        self.guest_form = guest_form
        self.folio_num = folio_num
        #print("folio: ", self.folio_num)
        QDialog.__init__(self)
        self.res_id = res_id
        #print("res_id: ", self.res_id)
        self.ui = Ui_posting_form()
        self.ui.setupUi(self)
        self.chargecodes_model = QSqlQueryModel(self)
        self.chargecodes_model.setQuery("select charge_code, description, tax_code from charge_codes WHERE charge_group <> 'Payments' AND charge_group <> 'Ledger' ORDER BY charge_code")
        view = QTableView()
        #view.horizontalHeader.visible
        #view.verticalHeader.hide()
        view.resizeColumnsToContents()
        view.setSelectionBehavior(QTableView.SelectRows)
        self.ui.charge_codes_combo.setView(view)
        self.chargecodes_view = self.ui.charge_codes_combo
        #self.chargecodes_view.setView(view)
        self.chargecodes_view.setModel(self.chargecodes_model)
        self.ui.res_id_edit.setText(self.res_id)
        self.ui.accept_button.clicked.connect(self.post_charge)
        self.ui.reject_button.clicked.connect(self.close)
        self.setStyleSheet("background-color: " + bg_colour)




    def post_charge(self):

        posting = Postings(charge_code=self.ui.charge_codes_combo.currentText(), 
                           person=user, 
                           description=self.ui.charge_description.text(), 
                           amount=self.ui.charge_amount.text(), 
                           folio=self.folio_num, 
                           trans_date=hotdate, 
                           res_ref=self.res_id,
                           dr=self.ui.debit.isChecked())

        self.guest_form.setup_folio1()    
        self.guest_form.setup_folio2()    
        self.guest_form.setup_folio3()    
        self.guest_form.setup_folio4()    
        self.close()


class Paying_form(QDialog):
    def __init__(self, guest_form, folio_num, res_id=0, parent=None):
        self.res_id = res_id
        self.guest_form = guest_form
        self.folio_num = folio_num
        QDialog.__init__(self)
        self.res_id = res_id
        #print("res_id: ", self.res_id)
        self.ui = Ui_posting_form()
        self.ui.setupUi(self)
        self.ui.res_id_edit.setText(self.res_id)
        self.chargecodes_model = QSqlQueryModel(self)
        self.chargecodes_model.setQuery("select charge_code, description from charge_codes WHERE charge_group = 'Payments' AND charge_code <> 'LEDGER' ORDER BY charge_code")
        self.ui.credit.setChecked(True)
        self.ui.credit.setText("Credit / Payment")
        self.ui.debit.setText("Debit / Refund")
        view = QTableView
        self.chargecodes_view = self.ui.charge_codes_combo
        #self.chargecodes_view.setView(view)
        self.chargecodes_view.setModel(self.chargecodes_model)
        self.ui.res_id_edit.setText(self.res_id)
        self.ui.accept_button.clicked.connect(self.post_payment)
        self.ui.reject_button.clicked.connect(self.close)
        self.setStyleSheet("background-color: " + bg_colour)


        if self.guest_form.ui.ci_co.text() == '':
            self.led = "DEP"
            self.ui.charge_description.setText("Deposit")

        else:
            self.led = "RGL"




    def post_payment(self):

        posting = Postings(charge_code=self.ui.charge_codes_combo.currentText(), 
                           person=user, 
                           description=self.ui.charge_description.text(), 
                           amount=self.ui.charge_amount.text(), 
                           folio=self.folio_num, 
                           trans_date=hotdate, 
                           res_ref=self.res_id,
                           dr=self.ui.debit.isChecked(),
                           ledger=self.led)

        self.guest_form.setup_folio1()    
        self.guest_form.setup_folio2()    
        self.guest_form.setup_folio3()    
        self.guest_form.setup_folio4()
        self.close()        

class Transfer_form(QDialog):
    def __init__(self, folio_num, model_indexes, folio_view, guest_form, res_id = 0, parent=None):
        QDialog.__init__(self)
        self.model_indexes = model_indexes
        #if self.model_indexes == []:
            #QMessageBox.warning(self, "Transfer transactions", "How can I transfer anything if you don't select the transactions?")
            #return
        self.folio_num = folio_num
        self.guest_form = guest_form
        self.res_id = res_id
        self.ui = Ui_current_guests()
        self.ui.setupUi(self)
        self.folio_view = folio_view
        self.accounts_model = QSqlQueryModel(self)
        self.accounts_model.setQuery("SELECT bookings_guests.room, bookings_guests.label, bookings_guests.id FROM bookings_guests  "
                                     "WHERE checked_in IS NOT NULL AND checked_out IS NULL "
                                     " ORDER BY to_number(bookings_guests.room, text(9999))".format(hotdate_string, hotdate_string))
        view = QTableView
        self.accounts_view = self.ui.current_accounts
        self.accounts_view.setModel(self.accounts_model)
        self.accounts_view.setColumnHidden(2, True)
        self.accounts_view.setColumnWidth(0,40)
        self.accounts_view.setColumnWidth(1,200)
        self.accounts_view.setSelectionBehavior(QTableView.SelectRows)
        self.accounts_view.clicked.connect(self.select_account)
        self.setStyleSheet("background-color: " + bg_colour)
        

    def select_account(self, model_index):
        new_res_id = self.accounts_view.model().index(model_index.row(), 2).data()
        trans_id_dict = {}
        for model_index in self.model_indexes:
            trans_id = self.folio_view.model().index(model_index.row(), 6).data()
            if trans_id not in trans_id_dict:
                trans_id_dict[trans_id] = [self.folio_view.model().index(model_index.row(), 3).data(), self.folio_view.model().index(model_index.row(), 2).data(), 
                                           self.folio_view.model().index(model_index.row(), 4).data(), self.folio_view.model().index(model_index.row(), 5).data(), 
                                               self.folio_view.model().index(model_index.row(), 2).data(), self.folio_view.model().index(model_index.row(), 7).data(), 
                                               self.folio_view.model().index(model_index.row(), 8).data()]

        QSqlDatabase.database().transaction()
        for trans_id in trans_id_dict:
            query = QSqlQuery()
            query.prepare("UPDATE transactions set folio=? WHERE ID=?")
            query.addBindValue('4')
            query.addBindValue(trans_id)
            query.exec_()
            if not query.isActive():
                QSqlDatabase.database().rollback()
                print(query.lastError().text())
                QMessageBox.warning("Transfer failed!" + query.lastError().text(), "Failure!")
                return

            desc = trans_id_dict[trans_id][0]
            desc = "xfr to: " + new_res_id + " "+ desc
            charge_code = trans_id_dict[trans_id][1]
            dr = trans_id_dict[trans_id][2]
            cr = trans_id_dict[trans_id][3]
            account = trans_id_dict[trans_id][4]
            dr_tax = trans_id_dict[trans_id][5]
            cr_tax = trans_id_dict[trans_id][6]

            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, dr, dr_tax, folio, account, hotdate, ledger_id)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            query.addBindValue(trans_time)
            query.addBindValue(user)
            query.addBindValue(charge_code)
            query.addBindValue(desc)
            query.addBindValue(dr)
            query.addBindValue(dr_tax)
            query.addBindValue(cr)
            query.addBindValue(cr_tax)
            query.addBindValue(4)
            query.addBindValue(self.res_id)
            query.addBindValue(hotdate.toString('yyyy-MM-dd'))
            query.addBindValue("RGL")
            query.exec_()
            if not query.isActive():
                QSqlDatabase.database().rollback()
                print(query.lastError().text())
                QMessageBox.warning("Transfer failed!" + query.lastError().text(), "Failure!")
                return

            desc = trans_id_dict[trans_id][0]
            desc = "Xfr from: " + self.res_id + " "+ desc
            charge_code = trans_id_dict[trans_id][1]
            cr = trans_id_dict[trans_id][2]
            dr = trans_id_dict[trans_id][3]
            cr_tax = trans_id_dict[trans_id][5]
            dr_tax = trans_id_dict[trans_id][6]
            account = new_res_id

            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, dr, dr_tax, folio, account, hotdate, ledger_id)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
            t = datetime.now()
            trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
            query.addBindValue(trans_time)
            query.addBindValue(user)
            query.addBindValue(charge_code)
            query.addBindValue(desc)
            query.addBindValue(dr)
            query.addBindValue(dr_tax)
            query.addBindValue(cr)
            query.addBindValue(cr_tax)
            query.addBindValue(self.folio_num)
            query.addBindValue(new_res_id)
            query.addBindValue(hotdate.toString('yyyy-MM-dd'))
            query.addBindValue("RGL")
            query.exec_()
            if not query.isActive():
                QSqlDatabase.database().rollback()
                print(query.lastError().text())
                QMessageBox.warning("Transfer failed!" + query.lastError().text(), "Failure!")
                return

        QSqlDatabase.database().commit()         
        self.guest_form.setup_folio1()    
        self.guest_form.setup_folio2()    
        self.guest_form.setup_folio3()    
        self.guest_form.setup_folio4()    
        self.close()





class Postings():
    def __init__(self, person=user, charge_code=None, description="", amount=0, folio=1, trans_date=None, res_ref = "", dr=True, ledger = "RGL"):
        self.person = person
        self.charge_code = charge_code
        self.description = description
        self.amount = amount
        self.folio = folio
        self.trans_date = trans_date
        self.res_ref = res_ref
        tax_code = QSqlQuery()
        tax_code.exec_("SELECT tax_code from charge_codes WHERE charge_code = '{0}'".format(self.charge_code))
        tax_code.first()
        tax = taxes[tax_code.value(0)]+1.0

        query = QSqlQuery()
        QSqlDatabase.database().transaction()
        if dr:
            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, dr, dr_tax, folio, account, hotdate, ledger_id)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
        else:
            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, folio, account, hotdate, ledger_id)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
        query.addBindValue(trans_time)
        query.addBindValue(self.person)
        query.addBindValue(self.charge_code)
        query.addBindValue(self.description)
        query.addBindValue(self.amount)
        query.addBindValue("{:.2f}".format(float(self.amount)-float(self.amount)/tax))
        query.addBindValue(self.folio)
        query.addBindValue(self.res_ref)
        query.addBindValue(self.trans_date)
        query.addBindValue(ledger)

        query.exec_()
        if not query.isActive():
            print(query.lastError().text())
            QSqlDatabase.database().rollback()
            QMessageBox.critical(None, "Posting Error!", query.lastError().text())
            return

        if dr:
            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, folio, account, hotdate, ledger_id)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
        else:
            query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, dr, dr_tax, folio, account, hotdate, ledger_id)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
        query.addBindValue(trans_time)
        query.addBindValue(self.person)
        query.addBindValue(self.res_ref)
        query.addBindValue(self.description)
        query.addBindValue(self.amount)
        query.addBindValue("{:.2f}".format(float(self.amount)-(float(self.amount)/tax)))
        query.addBindValue(self.folio)
        query.addBindValue(self.charge_code)
        query.addBindValue(self.trans_date)
        query.addBindValue(ledger)
        query.exec_()
        if not query.isActive():
            print(query.lastError().text())
            QSqlDatabase.database().rollback()
            QMessageBox.critical(None, "Posting Error!", query.lastError().text())
            self.close()
            return


        QSqlDatabase.database().commit()


class Printfolio():
    """Prints out the guest bill/folio"""
    def __init__(self, person=user, folio=0, trans_date=None, res_ref = "", copies=1, email_fol=False, address="Guest"):
        self.person = person
        self.folio = folio
        self.trans_date = trans_date
        self.res_ref = res_ref

        the_path = os.getcwd()
        bank_file = open(the_path + os.sep + 'bank_details.txt','r')
        bank_stuff = bank_file.read()

        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        csr_name = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)        
        csr_totals = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        csr_address = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        csr_guest = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        ledger_account = False
        query = QSqlQuery()
        query_hotel = QSqlQuery()

        #Get hotel data
        SQL = "SELECT param_text FROM params WHERE id = 3"
        query_hotel.exec_(SQL)
        query_hotel.first()
        if query_hotel.isActive():
            vat_no = query_hotel.value("param_text")
        else:
            vat_no = "Not found"

        SQL = "SELECT param_text FROM params WHERE id = 4"
        query_hotel.exec_(SQL)
        query_hotel.first()
        if query_hotel.isActive():
            report_line_1 = query_hotel.value("param_text")
        else:
            report_line_1 = ""

        SQL = "SELECT param_text FROM params WHERE id = 6"
        query_hotel.exec_(SQL)
        query_hotel.first()
        if query_hotel.isActive():
            report_line_2 = query_hotel.value("param_text")
        else:
            report_line_2 = ""


        if folio == 0:
            SQL = ("SELECT hotdate, person, charge_code, description,  "
                   "dr, dr_tax, dr-dr_tax as dr_net, "
                   "cr,  cr_tax,  cr-cr_tax as cr_net "
                   "FROM transactions where account = %s "
                   "AND charge_code NOT IN (SELECT ledger_id FROM ledger_accounts) ORDER BY trans_timestamp")
            cursor.execute(SQL,(self.res_ref,))

            SQL = ("SELECT COUNT(account) "
                   "FROM transactions where account = '{}' "
                   "AND charge_code IN (SELECT ledger_id FROM ledger_accounts)").format(self.res_ref)

            query.exec_(SQL)
            query.first()
            if query.isActive():
                if query.value("count")>0:
                    ledger_account = True


        else:
            SQL = ("SELECT hotdate, person, charge_code, description,  "
                   "dr, dr_tax, dr-dr_tax as dr_net, "
                   "cr,  cr_tax,  cr-cr_tax as cr_net "
                   "FROM transactions where account = %s AND folio = %s "
                   "AND charge_code NOT IN (SELECT ledger_id FROM ledger_accounts) ORDER BY trans_timestamp")
            cursor.execute(SQL,(self.res_ref, self.folio))

            SQL = "SELECT COUNT(account) FROM transactions WHERE account = '{}' AND folio = {} AND charge_code IN (SELECT ledger_id FROM ledger_accounts)".format(self.res_ref, self.folio)

            query.exec_(SQL)
            query.first()
            if query.isActive():
                if query.value("count")>0:
                    ledger_account = True

        rows = cursor.fetchall()

        #Get totals
        total_dr = 0
        total_dr_tax = 0
        total_cr = 0
        total_cr_tax = 0
        dr_subject_to = 0
        dr_subject_to_tax = 0
        cr_subject_to = 0
        cr_subject_to_tax = 0

        for row in rows:
            if row["cr"] != None:
                total_cr = total_cr + row["cr"]
            if row["cr_tax"] != None:
                total_cr_tax = total_cr_tax + row["cr_tax"]
                if row["cr_tax"] !=0:
                    cr_subject_to = cr_subject_to + row["cr"]
                    cr_subject_to_tax = cr_subject_to_tax + (row["cr"] - row["cr_tax"])
            if row["dr"] != None:
                total_dr = total_dr + row["dr"]
            if row["dr_tax"] != None:
                total_dr_tax = total_dr_tax + row["dr_tax"]
                if row["dr_tax"] != 0:
                    dr_subject_to = dr_subject_to + row["dr"]
                    dr_subject_to_tax = dr_subject_to_tax + (row["dr"] - row["dr_tax"])



        SQL_name = ("SELECT label, addr, postcode, country, id, booker_ref, company_id, ledger_id, room_id, "
                    "checked_out, folio1_checked_out, folio2_checked_out, folio3_checked_out, room, "
                    " folio4_checked_out, guest_id FROM bookings WHERE id = %s")

        csr_name.execute(SQL_name,(self.res_ref,))
        bkg = csr_name.fetchone()

        SQL = ("SELECT * FROM guests "
               "WHERE id = %s")
        csr_guest.execute(SQL, (bkg['guest_id'],))
        guest = csr_guest.fetchone()

        guest_name = ''
        full_address = ''
        if guest['title'] is not None:
            guest_name = guest_name + guest['title'] + ' '
        if guest['first_name'] is not None:
            guest_name = guest_name + guest['first_name'] + ' '
        guest_name = guest_name + guest['last_name'] 

        if address == "Company":
            SQL = "SELECT company_id, company_name, address, postcode, country, booker from companies where company_id = %s"
            csr_address.execute(SQL,(bkg["company_id"],))
            company = csr_address.fetchone()
            if company is not None:
                if company["booker"] is not None:
                    full_address = company["booker"] + "\n"
                if company["company_name"] is not None:
                    full_address = full_address + company["company_name"] + "\n" 
                if company["address"] is not None:
                    full_address = full_address + company["address"].replace("\r", "") + "\n"
                if company["postcode"] is not None:
                    if len(company["postcode"]) > 0:
                        full_address = full_address + "\n" + company["postcode"]
                if company["country"] is not None:
                    if company["country"].upper() != "UNITED KINGDOM":
                        if company["country"].upper() != "UK":
                            full_address = full_address + "\n" + company["country"]

        elif address == "Ledger":
            SQL = "SELECT ledger_id, ledger_name, address, postcode, country, contact from ledger_accounts where ledger_id = %s"
            csr_address.execute(SQL,(bkg["ledger_id"],))
            ledger = csr_address.fetchone()
            if ledger is not None:
                if ledger["contact"] is not None:
                    if len(ledger["contact"])>0:
                        full_address = ledger["contact"] + "\n"
                if ledger["ledger_name"]  is not None:
                    full_address = full_address + ledger["ledger_name"] + "" 
                if ledger["address"] is not None:
                    full_address = full_address + ledger["address"].replace("\r", "")
                if ledger["postcode"] is not None:
                    if len(ledger["postcode"]) > 0:
                        full_address = full_address + "\n" + ledger["postcode"]
                if ledger["country"] is not None:
                    if ledger["country"].upper() != "UNITED KINGDOM":
                        if ledger["country"].upper() != "UK":
                            full_address = full_address + "\n" + ledger["country"]

        else:
            if guest["address"] is not None:
                full_address = guest_name + '\n' + guest['address']
            if len(guest["postcode"]) > 0:
                full_address = full_address + "\n" + guest["postcode"]
            if guest["country"].upper() != "UNITED KINGDOM":
                if guest["country"].upper() != "UK":
                    full_address = full_address + "\n" + guest["country"]


        rpt = Report(datasource=rows)

        if address != "bkg":
            name_offset = 216
        else:
            name_offset = 20

        offset = 138

        if folio == 0:
            if bkg["checked_out"] is None:
                print_date = trans_date
            else:
                print_date = QDate(bkg["checked_out"])
        else:
            if bkg["folio" + str(folio) + "_checked_out"] is None:
                print_date = trans_date
            else:
                print_date = QDate(bkg["folio" + str(folio) + "_checked_out"])



        rpt.pageheader = Band([Image(pos=(200,0),  width=115, height=48, text="logo.jpg"),
                               #Element((200, 43), ("Helvetica", 8), text= "London Road, Tetbury, GL8 8JJ"),

                               Element((0, 74), ("Helvetica", 10), text= "Tax point: " + print_date.toString("dd/MM/yyyy")),
                               Element((0, 86), ("Helvetica", 10), text= "Invoice number: " + bkg["id"]),
                               Element((0, 98), ("Helvetica", 10), text = "Our VAT registration: " + vat_no),
                               Element((360, 74), ("Helvetica", 10), text = "" if bkg["booker_ref"] is None else "Your reference: " + bkg["booker_ref"]),
                               Element((360, 86), ("Helvetica", 10), text = "bkg name: " + guest_name),
                               Element((360, 98), ("Helvetica", 10), text = "Room number: "+ bkg["room"]),

                               Element((20, 130), ("Helvetica", 10), text= full_address),

                               Rule((250, 222), 56, thickness = 1),
                               Element((335, 216), ("Helvetica", 10), text= "Debits", align="right"),
                               Rule((335, 222), 55, thickness = 1),

                               Rule((405, 222), 53, thickness = 1),
                               Element((490, 216), ("Helvetica", 10), text= "Credits", align="right"),
                               Rule((490, 222), 50, thickness = 1),

                               Element((56, 216), ("Helvetica", 10), text= "Charge"),
                               Element((0, 228), ("Helvetica", 10), text= "Date"),
                               Element((56, 228), ("Helvetica", 10), text= "code"),
                               Element((100, 228), ("Helvetica", 10), text= "Description"),
                               Element((290, 228), ("Helvetica", 10), text= "Net amt", align="right"),
                               Element((340, 228), ("Helvetica", 10), text= "VAT amt", align="right"),
                               Element((390, 228), ("Helvetica", 10), text= "Gross amt", align="right"),
                               Element((440, 228), ("Helvetica", 10), text= "Net amt", align="right"),
                               Element((490, 228), ("Helvetica", 10), text= "VAT amt", align="right"),
                               Element((540, 228), ("Helvetica", 10), text= "Gross amt", align="right"),

                               Rule((0, 244), 7.5*72, thickness = 1),
                               ])

        rpt.detailband = Band([Element(pos=(0, 0), font=("Helvetica", 10), getvalue=lambda x: x["hotdate"].strftime('%d/%m/%Y')),
                               Element(pos=(56, 0), font=("Helvetica", 10), key="charge_code"),
                               Element(pos=(100, 0), font=("Helvetica", 10), getvalue=lambda x: fill(x["description"], width=30)),
                               Element(pos=(290, 0), font=("Helvetica", 10), key="dr_net", align="right", format=lambda x:'£%0.2f'%x),
                               Element(pos=(340, 0), font=("Helvetica", 10), key="dr_tax", align="right", format=lambda x:'£%0.2f'%x),
                               Element(pos=(390, 0), font=("Helvetica", 10), key="dr", align="right", format=lambda x:'£%0.2f'%x),
                               Element(pos=(440, 0), font=("Helvetica", 10), key="cr_net", align="right", format=lambda x:'£%0.2f'%x),
                               Element(pos=(490, 0), font=("Helvetica", 10), key="cr_tax", align="right", format=lambda x:'£%0.2f'%x),
                               Element(pos=(540, 0), font=("Helvetica", 10), key="cr", align="right", format=lambda x:'£%0.2f'%x)
                               ])

        rpt.pagefooter = Band([Rule((0, 0), 7.5*72, thickness = 1),
                               Element((271, 0), ("Helvetica", 8), align="center", text=report_line_1),
                               Element((271, 10), ("Helvetica", 8), align="center", text=report_line_2),
                               Element((500, 20), ("Helvetica", 10), sysvar = "pagenumber", format = lambda x: "Page %d" % x),])         


        if ledger_account:

            bank_details = [Element(pos=(270,90), font=("Helvetica", 10), align="centre", text=bank_stuff),
                            #Element(pos=(270,100), font=("Helvetica", 10), align="centre", text="Bank Details:"),
                             #Element(pos=(270,110), font=("Helvetica", 10), align="centre", text="Sort code: 40 33 33"),
                             #Element(pos=(270,120), font=("Helvetica", 10), align="centre", text="Bank account: 81658514")
                             ]
        else:
            bank_details = []

        rpt.reportfooter = Band([Rule((0, 0), 7.5*72, thickness = 1),
                                 Element(pos=(290, 0), font=("Helvetica", 10), text='£'+'{:20,.2f}'.format(total_dr-total_dr_tax).strip(), align="right"),
                                 Element(pos=(340, 0), font=("Helvetica", 10), text='£'+'{:20,.2f}'.format(total_dr_tax).strip(), align="right"),
                                 Element(pos=(390, 0), font=("Helvetica", 10), text='£'+'{:20,.2f}'.format(total_dr).strip(), align="right"),
                                 Element(pos=(440, 0), font=("Helvetica", 10), text='£'+'{:20,.2f}'.format(total_cr-total_cr_tax).strip(), align="right"),
                                 Element(pos=(490, 0), font=("Helvetica", 10), text='£'+'{:20,.2f}'.format(total_cr_tax).strip(), align="right"),
                                 Element(pos=(540, 0), font=("Helvetica", 10), text='£'+'{:20,.2f}'.format(total_cr).strip(), align="right"),
                                 Element(pos=(334,24), font=("Helvetica", 10), align="left", text="Gross amount subject to VAT:" ),
                                 Element(pos=(540,24), font=("Helvetica", 10), align="right", text='£'+'{:20,.2f}'.format(dr_subject_to - cr_subject_to).strip()),
                                 Element(pos=(334,36), font=("Helvetica", 10), align="left", text="Net amount subject to VAT:" ),
                                 Element(pos=(540,36), font=("Helvetica", 10), align="right", text='£'+'{:20,.2f}'.format(dr_subject_to_tax - cr_subject_to_tax).strip()),
                                 Element(pos=(334,48), font=("Helvetica", 10), align="left", text="VAT Amount:"),
                                 Element(pos=(540,48), font=("Helvetica", 10), align="right", text='£'+'{:20,.2f}'.format(total_dr_tax - total_cr_tax).strip()),
                                 Element(pos=(384,70), font=("Helvetica-Bold", 12), align="centre", text="The total balance of your bill is: "),
                                 Element(pos=(540,70), font=("Helvetica-Bold", 12), align="right", text="£" + '{:20,.2f}'.format(total_dr - total_cr).strip()),
                                 ] + bank_details) 


        f_name = folio_loc + self.res_ref + ".pdf"
        canvas = Canvas(f_name, pagesize=A4)


        canvas.drawString(14, 545, "-")
        rpt.generate(canvas)
        canvas.save()

    def print_fol(self, copies=1, prn_res_ref = ""):
        #f_name = os.path.expanduser("~folios\\" + prn_res_ref + ".pdf")
        f_name = folio_loc + prn_res_ref + ".pdf"
        a = subprocess.check_output(["gsprint.exe", "-copies",  str(copies), f_name], shell=True)

    def email_fol(self, prn_res_ref = ""):
        email_addresses, message_text, result = Email_folios.get_email_stuff(self, prn_res_ref)

        if result:
            try:
                yag = yagmail.SMTP(user='info@theprioryinn.co.uk', password='sCSr54BoU7@', host='smtp.livemail.co.uk', smtp_starttls=False)
                f_name = folio_loc + prn_res_ref + ".pdf"
                yag.send(email_addresses, "Invoices from The Priory Inn", message_text, [f_name])
                QMessageBox.information(None, "Email", "Email sent :)")
            except:
                QMessageBox.critical(None, "Email Error", "Error: your email was not sent. Sorry :(")
                return
        else:
            QMessageBox.critical(None, "Email Error", "Error: your email was not sent. Sorry :(")






class Check_out(QDialog):
    def __init__(self, guest_form, folio_num=0, res_id=0, parent=None):
        self.res_id = res_id
        QDialog.__init__(self)
        self.guest_form = guest_form
        self.folio_num = folio_num
        self.ui = Ui_checkout()

        self.folio_balances = []
        self.folio_balances.append(self.guest_form.ui.total_folios)
        self.folio_balances.append(self.guest_form.ui.folio1_balance)
        self.folio_balances.append(self.guest_form.ui.folio2_balance)
        self.folio_balances.append(self.guest_form.ui.folio3_balance)
        self.folio_balances.append(self.guest_form.ui.folio4_balance)

        self.folio_co_time = []
        self.folio_co_time.append(self.guest_form.ui.co)
        self.folio_co_time.append(self.guest_form.ui.folio1_co)
        self.folio_co_time.append(self.guest_form.ui.folio2_co)
        self.folio_co_time.append(self.guest_form.ui.folio3_co)
        self.folio_co_time.append(self.guest_form.ui.folio4_co)

        self.folios = []
        self.folios.append("")
        self.folios.append(self.guest_form.ui.folio1)
        self.folios.append(self.guest_form.ui.folio2)
        self.folios.append(self.guest_form.ui.folio3)
        self.folios.append(self.guest_form.ui.folio4)



        self.ui.setupUi(self)
        self.chargecodes_model = QSqlQueryModel(self)
        self.chargecodes_model.setQuery("select charge_code, description from charge_codes WHERE charge_group = 'Payments' OR charge_group = 'Balancing' ORDER BY charge_code")
        view = QTableView
        self.chargecodes_view = self.ui.charge_codes_combo
        self.chargecodes_view.setModel(self.chargecodes_model)
        self.ui.res_id_edit.setText(self.res_id)
        self.ui.res_reference.setText(self.guest_form.ui.booking_ref.text())
        if self.folio_num == 0:
            folio_title = "All folios"
        else:
            folio_title = "Folio " + str(self.folio_num)
        self.ui.folio_title.setText(folio_title)
        self.ui.charge_description.setText("Checked out")
        self.ui.charge_amount.setMinimum(int(float(self.folio_balances[self.folio_num].text())))
        self.ui.charge_amount.setValue(float(self.folio_balances[self.folio_num].text()))
        self.populate_ledgers()

        index = self.ui.ledger.findText(self.guest_form.ledger_model.index(self.guest_form.ui.ledger_account.currentIndex(), 0).data(), QtCore.Qt.MatchFixedString)
        if index > 0:
            self.ui.ledger.setCurrentIndex(index)
            index = self.ui.charge_codes_combo.findText("LEDGER", QtCore.Qt.MatchFixedString)
            self.ui.charge_codes_combo.setCurrentIndex(index)
        else:
            self.ui.charge_codes_combo.setCurrentIndex(0)

        self.setup_payments()

        self.ui.charge_codes_combo.currentIndexChanged.connect(self.setup_payments)
        self.ui.ledger.currentIndexChanged.connect(self.update_ledger)

        self.ui.co_no_print.clicked.connect(lambda: self.co())
        self.ui.co_print.clicked.connect(lambda: self.co(prn=self.ui.prn_copies.value()))
        self.ui.co_email.clicked.connect(lambda: self.co(email_fol=True))

        self.ui.reject_button.clicked.connect(self.close)


    def update_ledger(self):
        if self.ui.charge_codes_combo.currentText() == 'LEDGER':
            index = self.guest_form.ui.ledger_account.findText(self.ui.ledger.currentText(), QtCore.Qt.MatchFixedString)
            self.guest_form.ui.ledger_account.setCurrentIndex(index)

        
        
        
        
    def co(self, prn=0, email_fol=False):
        if self.ui.charge_codes_combo.currentText() == "LEDGER" and self.ui.ledger.currentText() == " ":
            QMessageBox.warning(self, "Check-out", "You must select a ledger account!")
            return

        if float(self.folio_balances[self.folio_num].text()) != 0:
            self.post_payment()
        query = QSqlQuery()
        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
        if self.folio_num == 0:
            start_fol = 1
            end_fol = 4
        else:
            start_fol = self.folio_num
            end_fol = self.folio_num

        for i in range(start_fol, end_fol+1):
            query.exec_("UPDATE bookings set folio{0}_checked_out='{1}' WHERE ID='{2}'".format(str(i), trans_time, self.res_id))
            if not query.isActive():
                print(query.lastError().text())
                QMessageBox.warning("Check out failed!" + query.lastError().text(), "Failure!")
                return
            self.folio_co_time[i].setText(trans_time)
            self.folios[i].setEnabled(False)

        if self.folio_num==0:
            query.exec_("UPDATE bookings set checked_out='{0}', departure_date = '{1}' WHERE ID='{2}'".format(trans_time, hotdate.toString("yyyy-MM-dd"), self.res_id))
            self.guest_form.ui.co.setText(trans_time)
            if not query.isActive():
                QMessageBox.warning(QWidget, "Check out","Check out failed!" + query.lastError().text() )
                return
        if prn > 0:
            x = Printfolio(folio=self.folio_num, trans_date=hotdate, res_ref=self.res_id, copies=prn, address=self.guest_form.ui.which_address.currentText())
            x.print_fol(copies=prn, prn_res_ref=self.res_id)

        if email_fol:
            x = Printfolio(folio=self.folio_num, trans_date=hotdate, res_ref=self.res_id, copies=prn, address=self.guest_form.ui.which_address.currentText())
            #self.email_me = Email_folios(parent=self, res_id=prn_res_ref)        
            email_addresses, message_text, result = Email_folios.get_email_stuff(self, self.res_id)

            if result:
                try:
                    yag = yagmail.SMTP(user='info@theprioryinn.co.uk', password='sCSr54BoU7@', host='smtp.livemail.co.uk', smtp_starttls=False)
                    f_name = folio_loc + self.res_id + ".pdf"
                    yag.send(email_addresses, "Invoices from The Priory Inn", message_text, [f_name])
                    QMessageBox.information(None, "Email", "Email sent :)")
                except:
                    QMessageBox.critical(None, "Email Error", "Error: your email was not sent. Sorry :(")
                    return
            else:
                QMessageBox.critical(None, "Email", "Although you have checked this guest out, you haven't sent any email to them!")





        self.guest_form.setup_folio1()    
        self.guest_form.setup_folio2()    
        self.guest_form.setup_folio3()    
        self.guest_form.setup_folio4()
        self.close()        

        myapp.populate_mainform()



    def checkout_all(self):
        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')

        self.guest_form.ui.co.setText(trans_time)
        query = QSqlQuery()
        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
        query.exec_("UPDATE bookings set checked_out='{0}', departure_date ='{1}' WHERE ID='{2}'".format(trans_time, hotdate.toString("yyyy-MM-dd"), self.res_id))
        if not query.isActive():
            print(query.lastError().text())
            QMessageBox.warning("Check out failed!" + query.lastError().text(), "Failure!")
            return
        self.ui.ci_co.setText(trans_time)
        myapp.populate_mainform()



    def post_payment(self):
        folios = []
        folios.append("")
        folios.append(self.guest_form.ui.folio1_balance)
        folios.append(self.guest_form.ui.folio2_balance)
        folios.append(self.guest_form.ui.folio3_balance)
        folios.append(self.guest_form.ui.folio4_balance)
        QSqlDatabase.database().transaction()
        if self.folio_num == 0:
            start_fol = 1
            end_fol = 4
        else:
            start_fol = self.folio_num
            end_fol = self.folio_num

        for i in range(start_fol, end_fol+1):
            if float(folios[i].text()) != 0:
                query = QSqlQuery()

                if float(folios[i].text()) > 0:
                    query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, folio, account, hotdate, ledger_id)"
                                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
                    amt = float(folios[i].text())
                else:
                    query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, dr, folio, account, hotdate, ledger_id)"
                                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
                    amt = float(folios[i].text()) * -1

                #you need to get the signs right
                t = datetime.now()
                trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
                query.addBindValue(trans_time)
                query.addBindValue(user)
                if self.chargecodes_model.index(self.ui.charge_codes_combo.currentIndex(), 0).data() == "LEDGER":
                    query.addBindValue(self.ui.ledger.currentText())
                else:
                    query.addBindValue(self.ui.charge_codes_combo.currentText())
                query.addBindValue(self.ui.charge_description.text())
                query.addBindValue(amt)
                query.addBindValue(i)
                query.addBindValue(self.res_id)
                query.addBindValue(hotdate.toString('yyyy-MM-dd'))
                query.addBindValue("RGL")
                query.exec_()
                if not query.isActive():
                    QSqlDatabase.database.rollback()
                    print(query.lastError().text())
                    QMessageBox.warning("Check out failed!" + query.lastError().text(), "Failure!")
                    return

                if float(folios[i].text()) > 0:
                    query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, dr, folio, account, hotdate, ledger_id)"
                                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
                    amt = float(folios[i].text())

                else:
                    query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, folio, account, hotdate, ledger_id)"
                                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
                    amt = float(folios[i].text()) * -1


                t = datetime.now()
                trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
                query.addBindValue(trans_time)
                query.addBindValue(user)
                query.addBindValue(self.res_id)
                query.addBindValue(self.ui.charge_description.text())
                query.addBindValue(amt)
                query.addBindValue(i)
                if self.chargecodes_model.index(self.ui.charge_codes_combo.currentIndex(), 0).data() == "LEDGER":
                    query.addBindValue(self.ui.ledger.currentText())
                else:
                    query.addBindValue(self.ui.charge_codes_combo.currentText())
                query.addBindValue(hotdate.toString('yyyy-MM-dd'))
                if self.chargecodes_model.index(self.ui.charge_codes_combo.currentIndex(), 0).data() == "LEDGER":
                    query.addBindValue("AR")
                else:
                    query.addBindValue("RGL")

                query.exec_()
                if not query.isActive():
                    QSqlDatabase.database.rollback()
                    print(query.lastError().text())
                    QMessageBox.warning(self, "Check out failed!" + query.lastError().text(), "Failure!")
                    return

        QSqlDatabase.database().commit()


    def setup_payments(self):
        if self.chargecodes_model.index(self.ui.charge_codes_combo.currentIndex(), 0).data() == "LEDGER":
            self.ui.ledger.setEnabled(True)
            index = self.ui.ledger.findText(self.guest_form.ledger_model.index(self.guest_form.ui.ledger_account.currentIndex(), 0).data(), QtCore.Qt.MatchFixedString)
            if index >= 0:
                self.ui.ledger.blockSignals(True)
                self.ui.ledger.setCurrentIndex(index)
                index = self.ui.charge_codes_combo.findText("LEDGER", QtCore.Qt.MatchFixedString)
                self.ui.charge_codes_combo.setCurrentIndex(index)
                index = self.guest_form.ui.which_address.findText("LEDGER", QtCore.Qt.MatchFixedString)
                self.guest_form.ui.which_address.setCurrentIndex(index)
                self.ui.ledger.blockSignals(False)
            

        else:
            self.ui.ledger.setCurrentIndex(-1)
            self.ui.charge_codes_combo.setEnabled(True)
            self.ui.ledger.setEnabled(False)


    def populate_ledgers(self):

        self.ledger_model = QSqlTableModel(self)
        self.ledger_model.setTable("ledger_accounts")
        self.ledger_model.setSort(1, Qt.AscendingOrder)
        self.ledger_model.select()
        view = QTableView()
        view.setSelectionBehavior(QTableView.SelectRows)
        self.ui.ledger.setView(view)

        self.ledger_view = self.ui.ledger
        self.ledger_view.setModel(self.ledger_model)
        self.ui.ledger.setModelColumn(self.ledger_model.fieldIndex("ledger_id"))

        for i in range(2, self.ledger_model.columnCount()):
            view.setColumnHidden(i, True)

        view.resizeColumnsToContents()
        view.horizontalHeader().setStretchLastSection(True)








class User_management_form(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self)
        self.ui = Ui_pms_users()
        self.ui.setupUi(self)


        self.user_roles_model = QSqlQueryModel(self)
        self.user_roles_model.setQuery("SELECT rolname FROM pg_roles "
                                       "WHERE rolcanlogin = False "
                                       "AND rolname <> 'epos' "
                                       "ORDER BY rolname")
        view = QTableView()
        #view.horizontalHeader.visible
        #view.verticalHeader.hide()
        view.resizeColumnsToContents()
        view.setSelectionBehavior(QTableView.SelectRows)
        self.ui.user_role_cbo.setView(view)
        self.roles_view = self.ui.user_role_cbo
        self.roles_view.setModel(self.user_roles_model)
        self.ui.cancel.clicked.connect(self.close)
        self.ui.add.clicked.connect(self.add_user)
        self.ui.delete_2.clicked.connect(self.delete_user)
        
        self.user_list()


    def user_list(self):
        self.existing_users_model = QSqlQueryModel()
        self.existing_users_model.setQuery("SELECT usename FROM pg_user "
                                           "WHERE usename <> 'postgres' "
                                           "AND usename <> 'channelrush'")
        self.users_view = self.ui.user_list
        self.users_view.setModel(self.existing_users_model)
        self.users_view.setSelectionBehavior(QTableView.SelectRows)
        

    def add_user(self):

        if self.ui.password1.text() != self.ui.password2.text():
            QMessageBox.warning(self,"Create users", "Passwords don't match!  User not created.")
            return
        
        #if len(self.ui.password1.text()) < 5:
            #QMessageBox.warning(self,"Create users", "This password is too short!  It must exceed 4 characters.  User not created.")
            #return

        query = QSqlQuery()
        SQL = "CREATE USER {0} PASSWORD '{1}'".format(self.ui.username.text(), self.ui.password1.text())
        query.exec_(SQL)
        
 
        if not query.isActive():
            print(query.lastError().text())
            QMessageBox.warning(self, "Failure!", "User creation failed!\n" + query.lastError().text())
            return

        SQL = "GRANT {0} TO {1}".format(self.ui.user_role_cbo.currentText(), self.ui.username.text())
        query.exec_(SQL)
        if not query.isActive():
            print(query.lastError().text())
            QMessageBox.warning(self, "Failure", "User creation failed granting rights not worker rights!\n" + query.lastError().text())
            return

        if self.ui.user_role_cbo.currentText() != 'worker':
            SQL = "GRANT worker TO {0}".format(self.ui.username.text())
            query.exec_(SQL)
            if not query.isActive():
                print(query.lastError().text())
                QMessageBox.warning(self, "Failure", "User creation failed granting worker rights!\n" + query.lastError().text())
                return
        self.user_list()

    def delete_user(self):
        self.user_name = self.users_view.model().index(self.users_view.selectedIndexes()[0].row(), 0).data()

        reply = QMessageBox.question(self, 'Confirmation of deletion',
                                     "Are you sure you would like to delete " + self.user_name, QMessageBox.Yes | 
            QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.No:
            return

        query = QSqlQuery()

        SQL = "DROP USER {0}".format(self.user_name)
        query.exec_(SQL)
        if not query.isActive():
            print(query.lastError().text())
            QMessageBox.warning(self, "Failure!", "User delete failed!" + query.lastError().text())
            return
        self.user_list()



class Guest_find_form(QDialog):
    """The form to find guests for deposits, History, cancellations, etc"""

    def __init__(self, parent, search_box=False):
        QDialog.__init__(self)
        self.ui = Ui_guest_find_form()
        self.ui.setupUi(self)
        self.ui.search_date.setDate(QDate.currentDate())
        self.ui.find_button.clicked.connect(self.populate)
        self.ui.rdo_date.setChecked(True)
        self.ui.guest_name.textChanged.connect(self.select_guest_name)
        self.ui.reference.textChanged.connect(self.select_reference)
        self.ui.invoice.textChanged.connect(self.select_invoice)
        self.ui.search_date.dateChanged.connect(self.select_date)
        self.ui.find_where.addItem('Future bookings')
        self.ui.find_where.addItem('Past bookings')
        self.ui.find_where.addItem('Cancelled bookings')
        self.ui.find_where.addItem('Everywhere')
        self.g_find_model = QSqlQueryModel()
        self.g_find_view = self.ui.guest_display
        if search_box:
            self.g_find_view.clicked.connect(self.choose_guest)
        else:
            self.g_find_view.clicked.connect(self.show_guest_form)
          
        self.setStyleSheet("background-color: " + bg_colour)

    def choose_guest(self):
        self.res_id = self.g_find_model.index(self.g_find_view.currentIndex().row(), 3).data()
        self.reference = self.g_find_model.index(self.g_find_view.currentIndex().row(), 4).data()
        print(self.res_id, self.reference)
        self.accept() 
 
    def select_date(self):
        self.ui.rdo_date.setChecked(True)

    def select_reference(self):
        self.ui.rdo_reference.setChecked(True)

    def select_guest_name(self):
        self.ui.rdo_name.setChecked(True)

    def select_invoice(self):
        self.ui.rdo_invoice.setChecked(True)

    def populate(self):

        if self.ui.find_where.currentText() == 'Future bookings':
            where_type = " AND checked_in is NULL AND cancelled is NULL "
        elif self.ui.find_where.currentText() ==  "Past bookings":
            where_type = " AND checked_in is Not NULL AND cancelled is NULL "
        elif self.ui.find_where.currentText() ==  "Cancelled bookings":
            where_type = " AND cancelled is NOT NULL "
        else:
            where_string, where_type = "", ""




        if self.ui.rdo_date.isChecked():

            if self.ui.comboBox.currentText() == "Date booked":
                where_string = " WHERE booked_at > '" + self.ui.search_date.date().toString("yyyy-MM-dd") + "' AND booked_at < '" + self.ui.search_date.date().addDays(1).toString('yyyy-MM-dd') + "'"
                SQL = ("SELECT arrival_date, bookings_guests.label,  bookings_guests.room, bookings_guests.id, bookings_guests.reference "
                       " FROM bookings_guests {0}{1}"
                       " ORDER BY booked_at".format(where_string, where_type))

            elif self.ui.comboBox.currentText() == "Departure date":
                where_string = " WHERE departure_date = '" + self.ui.search_date.date().toString("yyyy-MM-dd") + "'"
                SQL = ("SELECT arrival_date, bookings_guests.label,  bookings_guests.room, bookings_guests.id, bookings_guests.reference "
                       " FROM bookings_guests {0}{1}"
                       " ORDER BY booked_at".format(where_string, where_type))


            else:
                where_string = " WHERE arrival_date = '" + self.ui.search_date.date().toString("yyyy-MM-dd") + "'"
                SQL = ("SELECT arrival_date, bookings_guests.label,  bookings_guests.room, bookings_guests.id, bookings_guests.reference "
                           " FROM bookings_guests  {0}{1}"
                           " ORDER BY booked_at".format(where_string, where_type))

        elif self.ui.rdo_name.isChecked():
            where_string = " WHERE UPPER(label) LIKE '%" + self.ui.guest_name.text().upper() + "%'"
            SQL = ("SELECT arrival_date, bookings_guests.label,  bookings_guests.room, bookings_guests.id, bookings_guests.reference "
                   " FROM bookings_guests  "
                   "{0}{1} "
                   " ORDER BY booked_at".format(where_string, where_type))
        elif self.ui.rdo_reference.isChecked():
            where_string = " WHERE reference = '" + self.ui.reference.text() + "'"
            SQL = ("SELECT arrival_date, bookings_guests.label,  bookings_guests.room, bookings_guests.id, bookings_guests.reference "
                   " FROM bookings_guests  "
                   "{0} {1}"
                   " ORDER BY booked_at".format(where_string, where_type))
        else:
            where_string = " WHERE id = '" + self.ui.invoice.text() + "'"
            SQL = ("SELECT arrival_date, bookings_guests.label,  bookings_guests.room, bookings_guests.id, bookings_guests.reference "
                   " FROM bookings_guests  "
                   "{0} {1}"
                   " ORDER BY booked_at".format(where_string, where_type))



        #print(SQL)
        self.g_find_model.setQuery(SQL)

        self.g_find_view.setModel(self.g_find_model)    
        self.g_find_view.setColumnHidden(3, True)
        self.g_find_view.setColumnWidth(0,80)
        self.g_find_view.setColumnWidth(1,200)
        self.g_find_view.setSelectionBehavior(QTableView.SelectRows)  
        


    def show_guest_form(self, model_index):
        self.res_id = self.g_find_model.index(model_index.row(), 3).data()
        self.reference = self.g_find_model.index(model_index.row(), 4).data()
        self.gst_form = Guest_form(self.res_id)
        self.gst_form.show()


    
    
    
class Email_folios(QDialog):
    def __init__(self, res_id="", parent=None):


        self.folio = folio_num
        #self.res_id = 
        QDialog.__init__(self)
        self.ui = Ui_email_form()
        self.ui.setupUi(self)


        email_sql = ("SELECT "
                     "guests.email AS guest_email, "
                     "companies.email AS company_email, "
                     "ledger_accounts.email AS ledger_email "
                     "FROM public.bookings "
                     "LEFT JOIN guests ON bookings.guest_id = guests.id "
                     "LEFT JOIN companies ON bookings.company_id = companies.company_id "
                     "LEFT JOIN ledger_accounts ON bookings.ledger_id = ledger_accounts.ledger_id "
                     "WHERE bookings.id = '{0}';".format(res_id))


        qry_email = QSqlQuery()
        qry_email.exec_(email_sql)
        qry_email.first()
        if qry_email.isActive():
            self.ui.guest_address.setText(qry_email.value("guest_email"))
            if qry_email.value("guest_email") != "":
                self.ui.guest_address_tick.setChecked(True)
            self.ui.company_address.setText(qry_email.value("company_email"))
            self.ui.accounts_address.setText(qry_email.value("ledger_email"))
            if qry_email.value('ledger_email') != '':
                self.ui.accounts_address_tick.setChecked(True)
        else:
            QMessageBox.warning(self, 'Guest check out', "Couldn't get the email addresses!\n" + qry_email.lastError().text())
        qry_params = QSqlQuery()
        qry_params.exec_("SELECT param_text FROM params WHERE id=5")
        qry_params.first()
        if qry_params.isActive():
            message_text = qry_params.value("param_text")
            self.ui.message_text.setText(message_text)

        self.ui.buttonBox.accepted.connect(self.accept)
        self.ui.buttonBox.rejected.connect(self.reject)

    def email_addresses(self):
        email_addresses = []
        if self.ui.guest_address_tick.checkState() == 2:
            email_addresses.append(self.ui.guest_address.text())
        if self.ui.company_address_tick.checkState() == 2:
            email_addresses.append(self.ui.company_address.text())
        if self.ui.accounts_address_tick.checkState() == 2:
            email_addresses.append(self.ui.accounts_address.text())
        if self.ui.other_address_tick.checkState() == 2:
            email_addresses.append(self.ui.other_address.text())
            email_addresses.append('accounts@relishmail.co.uk')

        return email_addresses

    def message_text(self):
        return self.ui.message_text.toPlainText()




    @staticmethod
    def get_email_stuff(parent = None, res_id = ""):
        dialog = Email_folios(res_id, parent)
        result = dialog.exec_()
        addresses = dialog.email_addresses()
        msg_text = dialog.message_text()
        return (addresses, msg_text, result == QDialog.Accepted)



class Email_document(QDialog):
    def __init__(self, res_id="",  parent=None, message_text="", subject=""):


        QDialog.__init__(self)
        self.ui = Ui_email_form()
        self.ui.setupUi(self)
        self.setStyleSheet("background-color: " + bg_colour)


        email_sql = ("SELECT "
                     "guests.email AS guest_email, "
                     "companies.email AS company_email, "
                     "ledger_accounts.email AS ledger_email "
                     "FROM public.bookings "
                     "LEFT JOIN guests ON bookings.guest_id = guests.id "
                     "LEFT JOIN companies ON bookings.company_id = companies.company_id "
                     "LEFT JOIN ledger_accounts ON bookings.ledger_id = ledger_accounts.ledger_id "
                     "WHERE bookings.id = '{0}';".format(res_id))

        qry_email = QSqlQuery()
        qry_email.exec_(email_sql)
        qry_email.first()
        if qry_email.isActive():
            self.ui.guest_address.setText(qry_email.value("guest_email"))
            if qry_email.value("guest_email") != "":
                self.ui.guest_address_tick.setChecked(True)
            self.ui.company_address.setText(qry_email.value("company_email"))
            self.ui.accounts_address.setText(qry_email.value("ledger_email"))
        self.ui.message_text.setText(message_text)
        self.ui.subject.setText(subject)
        self.ui.buttonBox.accepted.connect(self.accept)
        self.ui.buttonBox.rejected.connect(self.reject)

    def email_addresses(self):
        email_addresses = []
        if self.ui.guest_address_tick.checkState() == 2:
            email_addresses.append(self.ui.guest_address.text())
        if self.ui.company_address_tick.checkState() == 2:
            email_addresses.append(self.ui.company_address.text())
        if self.ui.accounts_address_tick.checkState() == 2:
            email_addresses.append(self.ui.accounts_address.text())
        if self.ui.other_address_tick.checkState() == 2:
            email_addresses.append(self.ui.other_address.text())

        return email_addresses

    def message_text(self):
        return self.ui.message_text.toHtml() 
    def subject_text(self):
        return self.ui.subject.text()




    @staticmethod
    def get_email_stuff(parent = None, res_id = "", message_text="", subject=""):
        dialog = Email_document(res_id, parent, message_text, subject)
        result = dialog.exec_()
        addresses = dialog.email_addresses()
        msg_text = dialog.message_text()
        subj_text = dialog.subject_text()

        return (addresses, msg_text, subj_text, result == QDialog.Accepted)





class Date_dialog(QDialog):
    def __init__(self, parent=None, title="Enter date...", start_date=QDate.currentDate(), end_date=QDate.currentDate()):
        super(Date_dialog, self).__init__(parent)
        self.daterange = []
        self.ui = Ui_date_dialog_form()
        self.ui.setupUi(self)
        self.setWindowTitle(title)
        self.ui.start_date.setDate(start_date)
        self.ui.end_date.setDate(end_date)


    def get_date(self):
        return self.ui.date_edit.date().toPyDate()


class CustomQCompleter(QCompleter):
    def __init__(self, parent=None):
        super(CustomQCompleter, self).__init__(parent)
        self.local_completion_prefix = ""
        self.source_model = None

    def setModel(self, model):
        self.source_model = model
        super(CustomQCompleter, self).setModel(self.source_model)

    def updateModel(self):
        local_completion_prefix = self.local_completion_prefix
        class InnerProxyModel(QSortFilterProxyModel):
            def filterAcceptsRow(self, sourceRow, sourceParent):
                index0 = self.sourceModel().index(sourceRow, 0, sourceParent)
                return local_completion_prefix.lower() in self.sourceModel().data(index0).lower()
        proxy_model = InnerProxyModel()
        proxy_model.setSourceModel(self.source_model)
        super(CustomQCompleter, self).setModel(proxy_model)

    def splitPath(self, path):
        self.local_completion_prefix = path
        self.updateModel()
        return ""


class FloatDelegate(QItemDelegate):
    def __init__(self, decimals, parent=None):
        QItemDelegate.__init__(self, parent=parent)
        self.nDecimals = decimals

    def paint(self, painter, option, index):
        value = index.model().data(index, Qt.EditRole)      #Segfaults here
        try:
            number = float(value)
            painter.drawText(option.rect, QtCore.Qt.AlignRight|QtCore.Qt.AlignVCenter, "{:.{}f}".format(number, self.nDecimals))
        except :
            QItemDelegate.paint(self, painter, option, index)

class Login(QDialog):
    """User login """
    def __init__(self):
        QDialog.__init__(self)
        self.ui = Ui_login_form()
        self.ui.setupUi(self)
        self.ui.buttonBox.accepted.connect(lambda: self.handle_login(servers=servers))
        servers = {}
        with open('servers.csv', newline='') as csvfile:
            server_reader = csv.reader(csvfile)
            for row in server_reader:
                self.ui.cbo_db_name.addItem(row[1])
                servers[row[1]] = (row[0],row[2],row[3])



    def handle_login(self, servers=''):
        global user
        global pword
        global database
        global server
        global bg_colour
        user = self.ui.username.text()
        pword = self.ui.password.text()
        the_key = self.ui.cbo_db_name.currentText()
        server = servers[the_key][0]
        database = servers[the_key][1]
        bg_colour = servers[the_key][2]



class Rooms_avail():
    """Determines room avialability from a list of rooms and dates. 
    Expects a list of lists for each stay consisting of room, arrival_date, departure_date, room_type and res_id to ignore.
    The res_id to ignore is so a booking can adjust arrival and dep dates
    If room_id is None then only room_type check is done.
    Dates need to be python dates)"""
    def __init__(self, rooms_to_check=[], parent=None):
        self.rooms_to_check = rooms_to_check
        self.parent = parent
        if len(self.rooms_to_check) == 0:
            self.available = False
            return

        self.available = False
        self.room_available = 0
        self.room_type_available = 0

        SQL = ("SELECT room_type, base_num "
               "FROM room_types "
               "WHERE base_type IS NOT NULL")

        self.room_types = {}
        room_types_qry = QSqlQuery()
        room_types_qry.exec_(SQL)
        while room_types_qry.next():
            self.room_types[room_types_qry.value("room_type")] = room_types_qry.value("base_num")

        for ind_booking in self.rooms_to_check:
            if ind_booking[0] is not None:
                self.individual_room()


        if self.room_available == 0:
            self.room_available = True
        else:
            self.room_available = False
            return

        #Check the room type to make sure the unallocated bookings are accounted for
        self.room_type()


    def individual_room(self):

        for room in self.rooms_to_check:
            if room[1] < hotdate.toPyDate():
                room[1] = hotdate.toPyDate()
            nights = room[2] - room[1]
            for each_night in range(nights.days):

                SQL = ("SELECT COUNT(id) as num_rooms_booked "
                       "FROM bookings "
                       "WHERE room = ? "
                       "AND arrival_date <= ? "
                       "AND departure_date > ? "
                       "AND cancelled Is NULL "
                       "AND checked_out Is NULL "
                       )

                if room[4] is not None:
                    SQL = SQL + "AND id <> ?"

                arr_date = room[1] + timedelta(days=each_night)
                dep_date = room[1] + timedelta(days=each_night + 1)
                query = QSqlQuery()
                query.prepare(SQL)

                query.addBindValue(room[0])
                query.addBindValue(arr_date.strftime('%Y-%m-%d'))
                query.addBindValue(arr_date.strftime('%Y-%m-%d'))
                if room[4] is not None:
                    query.addBindValue(room[4])
                query.exec_()
                query.first()

                if not query.isActive():
                    print(query.lastError().text())
                    QMessageBox.critical(None, "Availability Error!", "Can't check availability!\n" + query.lastError().text())
                    self.room_available = False
                    return

                self.room_available += query.value("num_rooms_booked")

    def room_type(self):
        self.available = False
        self.num_rooms_booked = 0

        #Check to see if we should exclued a booking (ie for room moves)
        check_booking = ''
        for booking in self.rooms_to_check:
            if booking[4] is not None:
                check_booking = " AND id <> '" + booking[4] + "' "

        the_bookings = {}
        room_types = {}

        for the_booking in self.rooms_to_check:
            if booking[3] not in the_bookings:
                the_bookings[booking[3]] = {}

            nights = the_booking[2] - the_booking[1]
            for each_night in range(nights.days):
                if the_booking[1] + timedelta(days=each_night) not in the_bookings[booking[3]]:
                    the_bookings[booking[3]][the_booking[1] + timedelta(days=each_night)] = 1
                else:
                    the_bookings[booking[3]][the_booking[1] + timedelta(days=each_night)] += 1



        query_temp = QSqlQuery()
        SQL = 'DROP TABLE IF EXISTS avail_temp'
        query_temp.exec_(SQL)
        SQL = ("CREATE TEMP TABLE avail_temp "
               "(avail_date DATE, "
               "room_type TEXT, "
               "rooms_sold INTEGER)")
        query_temp.exec_(SQL)
        if not query_temp.isActive():
            QMessageBox.warning(self.parent, "Availability check", "Can't check availability.  Creating temp table caused: \n" + query_temp.lastError().text())
            return
        SQL = ("INSERT INTO avail_temp (avail_date, room_type, rooms_sold) VALUES ")
        for room_type, avail_date in the_bookings.items():
            for the_date, rooms_sold in avail_date.items():
                SQL = SQL + " ('" + the_date.strftime('%Y-%m-%d') + "', '" + room_type + "', " + str(rooms_sold) + "),"
        SQL = SQL[:-1]
        query_temp.exec_(SQL)    
        if not query_temp.isActive():
            QMessageBox.warning(self.parent, "Availability check", "Can't check availability.  Inserting records into temp table caused: \n" + query_temp.lastError().text())
            return




        SQL = """SELECT avail_date, available , room_type FROM (SELECT avail_date::date, rt.room_type, (rt.base_num - COUNT(bookings.id)) - rooms_sold as available, rooms_sold
	FROM avail_temp
        JOIN (SELECT room_type, base_num 
   	FROM room_types
   	WHERE base_num is not null) rt on avail_temp.room_type = rt.room_type
	LEFT JOIN (SELECT arrival_date, id, departure_date, room_type
	FROM bookings 
  	WHERE cancelled IS NULL
  	AND checked_out IS NULL
        {0}
        ) bookings
 	ON avail_date BETWEEN bookings.arrival_date AND (bookings.departure_date - INTERVAL '1 DAY')
        AND bookings.room_type = rt.room_type
        GROUP BY avail_date, rt.room_type, base_num, rooms_sold) availability
        WHERE available < 0""".format(check_booking)
        query_avail = QSqlQuery()
        query_avail.exec_(SQL)
        if not query_avail.isActive():
            print(query_avail.lastError().text())
            QMessageBox.warning(self.parent, "Select rooms...", "Couldn't find availability!\n" + query_avail.lastError().text())
            return
        if query_avail.size() > 0:
            self.room_available = False
        else:
            self.room_available = True
        print(self.room_available)    
        print(query_avail.size())






    def room_type_old(self):

        self.available = False
        self.num_rooms_booked = 0

        for room in self.rooms_to_check:
            nights = room[2] - room[1]
            rt = room[3]
            for each_night in range(nights.days):

                arr_date = room[1] + timedelta(days=each_night)
                dep_date = room[1] + timedelta(days=each_night + 1)


                SQL = ("SELECT room_type, count(id) AS num_rooms_booked "
                       "FROM bookings "
                       "WHERE "
                       "room_type = ? "
                       "AND arrival_date <= ? "
                       "AND departure_date >= ? "
                       "AND cancelled IS NULL "
                       "AND checked_out IS NULL "
                       )
                if room[4] is not None:
                    SQL = SQL + "AND id <> ? "
                SQL = SQL + "group by room_type"


                query = QSqlQuery()
                query.prepare(SQL)
                query.addBindValue(rt)
                query.addBindValue(arr_date.strftime('%Y-%m-%d'))
                query.addBindValue(dep_date.strftime('%Y-%m-%d'))
                if room[4] is not None:
                    query.addBindValue(room[4])

                query.exec_()
                query.first()

                if not query.isActive():
                    print(query.lastError().text())
                    QMessageBox.critical(None, "Availability Error!", "Can't check availability!\n" + query.lastError().text())
                    self.available = False
                    return

                if query.value("num_rooms_booked") is None:
                    num_rooms_booked = 0
                else:
                    num_rooms_booked = query.value("num_rooms_booked")

                if  self.num_rooms_booked < num_rooms_booked:
                    self.num_rooms_booked = num_rooms_booked

        query = QSqlQuery()
        SQL = ("SELECT base_num "
               "FROM room_types "
               "WHERE base_type = ? ")
        query.prepare(SQL)
        query.addBindValue(rt)
        query.exec_()
        query.first()

        if not query.isActive():
            print(query.lastError().text())
            QMessageBox.critical(None, "Availability Error!", "Can't check availability!\n" + query.lastError().text())
            self.available = False
            return
        total_rooms = query.value("base_num")    
        self.room_type_available = total_rooms - self.num_rooms_booked    





if __name__=="__main__":

    folio_num = 0
    pword = ''
    dbase = ''
    server = ''

    #Login
    while True:
        if Login().exec_() == QDialog.Accepted:
            db = QSqlDatabase.addDatabase("QPSQL");
            db.setHostName(server)
            db.setDatabaseName(database)
            db.setUserName(user)
            db.setPassword(pword)
            if (db.open()==False):
                QMessageBox.critical(None, "Database Error", db.lastError().text())
            else:
                break
        else:
            sys.exit()
    
    
    taxes = {}
    query = QSqlQuery()
    query.exec_("SELECT tax_code , tax_rate from tax")
    while query.next():
        taxes[query.value(0)] = query.value(1)

    query = QSqlQuery()
    query.exec_("SELECT param_number from params where id = 2")
    query.first()
    hotel_code = str(int(query.value(0)))
    folio_num = ''
    myapp = Ci_Co()
    splash.close()

    myapp.show()
    sys.exit(app.exec_())



