# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rooming_list.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_rooming_list(object):
    def setupUi(self, rooming_list):
        rooming_list.setObjectName("rooming_list")
        rooming_list.resize(784, 640)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        rooming_list.setWindowIcon(icon)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(rooming_list)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(rooming_list)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.reference = QtWidgets.QLineEdit(rooming_list)
        self.reference.setObjectName("reference")
        self.horizontalLayout.addWidget(self.reference)
        spacerItem = QtWidgets.QSpacerItem(488, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.room_list = QtWidgets.QTableWidget(rooming_list)
        self.room_list.setObjectName("room_list")
        self.room_list.setColumnCount(0)
        self.room_list.setRowCount(0)
        self.verticalLayout_2.addWidget(self.room_list)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)

        self.retranslateUi(rooming_list)
        QtCore.QMetaObject.connectSlotsByName(rooming_list)

    def retranslateUi(self, rooming_list):
        _translate = QtCore.QCoreApplication.translate
        rooming_list.setWindowTitle(_translate("rooming_list", "Rooming list"))
        self.label.setText(_translate("rooming_list", "Group reference"))

import pms_rc
