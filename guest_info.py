# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'guest_info.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_guest_info(object):
    def setupUi(self, guest_info):
        guest_info.setObjectName("guest_info")
        guest_info.resize(453, 343)
        self.layoutWidget = QtWidgets.QWidget(guest_info)
        self.layoutWidget.setGeometry(QtCore.QRect(0, 0, 451, 341))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout_16 = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout_16.setObjectName("gridLayout_16")
        self.label_14 = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_14.setFont(font)
        self.label_14.setObjectName("label_14")
        self.gridLayout_16.addWidget(self.label_14, 6, 0, 1, 1)
        self.guest_email = QtWidgets.QLineEdit(self.layoutWidget)
        self.guest_email.setText("")
        self.guest_email.setObjectName("guest_email")
        self.gridLayout_16.addWidget(self.guest_email, 6, 1, 1, 1)
        self.label_24 = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_24.setFont(font)
        self.label_24.setObjectName("label_24")
        self.gridLayout_16.addWidget(self.label_24, 7, 0, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.gridLayout_16.addWidget(self.label_5, 3, 0, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.gridLayout_16.addWidget(self.label_6, 4, 0, 1, 1)
        self.guest_country = QtWidgets.QLineEdit(self.layoutWidget)
        self.guest_country.setText("")
        self.guest_country.setObjectName("guest_country")
        self.gridLayout_16.addWidget(self.guest_country, 4, 1, 1, 1)
        self.guest_postcode = QtWidgets.QLineEdit(self.layoutWidget)
        self.guest_postcode.setObjectName("guest_postcode")
        self.gridLayout_16.addWidget(self.guest_postcode, 3, 1, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_13.setFont(font)
        self.label_13.setObjectName("label_13")
        self.gridLayout_16.addWidget(self.label_13, 5, 0, 1, 1)
        self.guest_phone = QtWidgets.QLineEdit(self.layoutWidget)
        self.guest_phone.setText("")
        self.guest_phone.setObjectName("guest_phone")
        self.gridLayout_16.addWidget(self.guest_phone, 5, 1, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.gridLayout_16.addWidget(self.label_7, 1, 0, 1, 1)
        self.gridLayout_15 = QtWidgets.QGridLayout()
        self.gridLayout_15.setObjectName("gridLayout_15")
        self.guest_title = QtWidgets.QLineEdit(self.layoutWidget)
        self.guest_title.setObjectName("guest_title")
        self.gridLayout_15.addWidget(self.guest_title, 2, 0, 1, 1)
        self.guest_first_name = QtWidgets.QLineEdit(self.layoutWidget)
        self.guest_first_name.setObjectName("guest_first_name")
        self.gridLayout_15.addWidget(self.guest_first_name, 2, 1, 1, 1)
        self.guest_surname = QtWidgets.QLineEdit(self.layoutWidget)
        self.guest_surname.setObjectName("guest_surname")
        self.gridLayout_15.addWidget(self.guest_surname, 2, 2, 1, 1)
        self.gridLayout_15.setColumnStretch(0, 1)
        self.gridLayout_15.setColumnStretch(1, 2)
        self.gridLayout_15.setColumnStretch(2, 3)
        self.gridLayout_16.addLayout(self.gridLayout_15, 1, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.gridLayout_16.addWidget(self.label_4, 2, 0, 1, 1)
        self.guest_comments = QtWidgets.QTextEdit(self.layoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.guest_comments.sizePolicy().hasHeightForWidth())
        self.guest_comments.setSizePolicy(sizePolicy)
        self.guest_comments.setMaximumSize(QtCore.QSize(1000, 100))
        self.guest_comments.setObjectName("guest_comments")
        self.gridLayout_16.addWidget(self.guest_comments, 7, 1, 1, 1)
        self.guest_id = QtWidgets.QLineEdit(self.layoutWidget)
        self.guest_id.setObjectName("guest_id")
        self.gridLayout_16.addWidget(self.guest_id, 0, 1, 1, 1)
        self.label_36 = QtWidgets.QLabel(self.layoutWidget)
        self.label_36.setObjectName("label_36")
        self.gridLayout_16.addWidget(self.label_36, 0, 0, 1, 1)
        self.guest_address = QtWidgets.QTextEdit(self.layoutWidget)
        self.guest_address.setObjectName("guest_address")
        self.gridLayout_16.addWidget(self.guest_address, 2, 1, 1, 1)
        self.label_14.setBuddy(self.guest_email)
        self.label_24.setBuddy(self.guest_comments)
        self.label_5.setBuddy(self.guest_postcode)
        self.label_6.setBuddy(self.guest_country)
        self.label_13.setBuddy(self.guest_phone)
        self.label_7.setBuddy(self.guest_title)

        self.retranslateUi(guest_info)
        QtCore.QMetaObject.connectSlotsByName(guest_info)

    def retranslateUi(self, guest_info):
        _translate = QtCore.QCoreApplication.translate
        guest_info.setWindowTitle(_translate("guest_info", "Form"))
        self.label_14.setText(_translate("guest_info", "email"))
        self.label_24.setText(_translate("guest_info", "Guest comments"))
        self.label_5.setText(_translate("guest_info", "Postcode"))
        self.label_6.setText(_translate("guest_info", "Country"))
        self.label_13.setText(_translate("guest_info", "Telephone"))
        self.label_7.setText(_translate("guest_info", "Guest name"))
        self.guest_title.setToolTip(_translate("guest_info", "Title like Mr, Mrs, Dr, Rev"))
        self.guest_title.setPlaceholderText(_translate("guest_info", "Mr"))
        self.guest_first_name.setToolTip(_translate("guest_info", "First name"))
        self.guest_first_name.setPlaceholderText(_translate("guest_info", "Firstname"))
        self.guest_surname.setToolTip(_translate("guest_info", "Surname or last name"))
        self.guest_surname.setPlaceholderText(_translate("guest_info", "Surname"))
        self.label_4.setText(_translate("guest_info", "Address"))
        self.label_36.setText(_translate("guest_info", "Guest ID"))

