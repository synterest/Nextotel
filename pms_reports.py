# -*- coding: utf-8 -*-
from PollyReports import *
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.units import cm
from textwrap import fill

from PyQt5.QtSql import *

import psycopg2
import psycopg2.extras
from config import *
import random

import sys
from datetime import datetime, timedelta, time
import os
import subprocess
import webbrowser
import csv




if sys.platform == 'linux':
    import cups
    conn = cups.Connection()
    printers = conn.getPrinters ()
    for printer in printers:
        print(printer, printers[printer]["device-uri"])
        
def get_hotel_data(user="", pword="", database="pms", server=""):
    cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
    cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  

    SQL = "SELECT param_text FROM params WHERE id = 4"
    cursor.execute(SQL,)
    rows = cursor.fetchall()
    if len(rows) > 0:
        report_line_1 = rows[0]["param_text"]
    else:
        report_line_1 = ""
    
    SQL = "SELECT param_text FROM params WHERE id = 6"
    cursor.execute(SQL,)
    rows = cursor.fetchall()
    if len(rows) > 0:
        report_line_2 = rows[0]["param_text"]
    else:
        report_line_2 = ""
    
    return report_line_1, report_line_2


class Arrivals():
    def __init__(self, arrival_date=datetime.now(), user="", pword="", database="pms", server=""):
        self.reportname = "arrivals"
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)
        
        logo = "logo.jpg"

        # Database stuvv
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        SQL = ("select rooms.room, bookings_guests.reference, bookings_guests.label, bookings_guests.id, arrival_date, departure_date, "
               "num_adults, num_children, notes, channel_ref, voucher "
               "from bookings_guests LEFT JOIN rooms ON bookings_guests.room_id = rooms.hls_code "
               "where cancelled is Null AND arrival_date = %s "
               "ORDER BY to_number(rooms.room, text(9999))")
        cursor.execute(SQL,(arrival_date.strftime('%Y-%m-%d'),))
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text=logo),
                               Element(pos=(382,0), font=(title_font), align="centre", text="Arrivals List for " + arrival_date.strftime('%d/%m/%Y')),
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Rm"),
                               Element(pos=(24, 54), font=(all_font),   align = "left", text="Name"),
                               Element(pos=(134, 54), font=(all_font),   align = "left", text="Reference"),
                               Element(pos=(224, 54), font=(all_font),   align = "right", text="Adults"),
                               Element(pos=(264, 54), font=(all_font),   align = "right", text="Children"),
                               Element(pos=(284, 54), font=(all_font),   align = "left", text="Voucher"),
                               Element(pos=(334, 54), font=(all_font),   align = "left", text="Channel"),
                               Element(pos=(384, 54), font=(all_font),   align = "left", text="Notes"),
                               Rule((0, 66), 765, thickness = 1),
                               
                              ])
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="room"),
                               Element(pos=(24, 0), font=all_font, align="left", key="label"),
                               Element(pos=(134, 0), font=all_font, align="left", key="reference"),
                               Element(pos=(224, 0), font=all_font, align="right", key="num_adults"),
                               Element(pos=(264, 0), font=all_font, align="right", key="num_children"),
                               Element(pos=(284, 0), font=all_font, align="left", key="voucher"),
                               Element(pos=(334, 0), font=all_font, align="left", key="channel_ref"),
                               Element(pos=(384, 0), font=all_font, align="left", getvalue=lambda x: x["notes"] if x["notes"] == None else fill(x["notes"], width=80) )
                               ])
 
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        
        fname = report_loc + "arrivals.pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
            
            
class In_house():
    def __init__(self, trans_date=datetime.now(), user="", pword="", database="pms", server=""):
        self.reportname = "Registered Guest Ledger"
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)
        rpt_date = trans_date
        trans_date = trans_date + timedelta(days=1)
        
        logo = "logo.jpg"

        # Database stuvv
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        SQL = ("SELECT room, bookings_guests.id, label, bookings_guests.arrival_date, bookings_guests.departure_date, MAX(bookings_guests.booked_at) AS booked_at, "
               "bookings_guests.num_adults, bookings_guests.num_children, " 
               "Sum(transactions.dr) AS dr, "
               "Sum(transactions.cr) AS cr, "
               "Sum(transactions.dr_tax) AS dr_tax, "
               "Sum(transactions.cr_tax) AS cr_tax, "
               "coalesce(Sum(transactions.dr),0) - coalesce(Sum(transactions.cr),0) AS total "
               "FROM (select * FROM transactions WHERE hotdate < %s) as transactions INNER JOIN bookings_guests ON transactions.account = bookings_guests.id  "
               "WHERE bookings_guests.checked_in < %s AND (bookings_guests.checked_out IS Null OR bookings_guests.checked_out > %s) "
               "GROUP BY room, bookings_guests.id, bookings_guests.label, bookings_guests.arrival_date, bookings_guests.departure_date, bookings_guests.num_adults, bookings_guests.num_children "
               "ORDER BY to_number(room, text(9999));")
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),trans_date.strftime('%Y-%m-%d'),trans_date.strftime('%Y-%m-%d')))
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text=logo),
                               Element(pos=(382,0), font=(title_font), align="centre", text="Registered guest ledger for " + rpt_date.strftime('%d/%m/%Y')),
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Rm"),
                               Element(pos=(24, 54), font=(all_font),   align = "left", text="Name"),
                               Element(pos=(154, 54), font=(all_font),   align = "left", text="Invoice"),
                               Element(pos=(244, 54), font=(all_font),   align = "right", text="Adults"),
                               Element(pos=(284, 54), font=(all_font),   align = "right", text="Children"),
                               Element(pos=(324, 54), font=(all_font),   align = "left", text="Arrival"),
                               Element(pos=(389, 54), font=(all_font),   align = "left", text="Departure"),
                               Element(pos=(504, 54), font=(all_font),   align = "right", text="Dr"),
                               Element(pos=(554, 54), font=(all_font),   align = "right", text="Cr"),
                               Element(pos=(604, 54), font=(all_font),   align = "right", text="Balance"),
                               Element(pos=(714, 54), font=(all_font),   align = "right", text="Booked at"),
                               Rule((0, 66), 765, thickness = 1),
                               
                              ])
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="room"),
                               Element(pos=(24, 0), font=all_font, align="left", key="label"),
                               Element(pos=(154, 0), font=all_font, align="left", key="id"),
                               Element(pos=(244, 0), font=all_font, align="right", key="num_adults"),
                               Element(pos=(284, 0), font=all_font, align="right", key="num_children"),
                               Element(pos=(324, 0), font=all_font, align="left", getvalue=lambda x: x["arrival_date"].strftime('%d/%m/%Y')),
                               Element(pos=(389, 0), font=all_font, align="left", getvalue=lambda x: x["departure_date"].strftime('%d/%m/%Y')),
                               Element(pos=(504, 0), font=all_font, align="right", key="dr"),
                               Element(pos=(554, 0), font=all_font, align="right", key="cr"),
                               Element(pos=(604, 0), font=all_font, align="right", key="total" ),
                               Element(pos=(714, 0), font=all_font, align="right", key="booked_at" )
                               ])
 

        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1), 
            SumElement(pos=(504, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(554, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(604, 0), font=all_font, key="total",  align="right", format=lambda x:'£%0.2f'%x),
        ])    





        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        
        fname = report_loc + "rgl" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
    
    
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
            
 
 
class Deposits_ledger():
    def __init__(self, trans_date=datetime.now(), user="", pword="", database="pms", server=""):
        self.reportname = "Deposits Ledger"
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        
        rpt_date = trans_date
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)
        
        logo = "logo.jpg"

        # Database stuvv
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        SQL = ("SELECT room, bookings_guests.id, bookings_guests.label, bookings_guests.arrival_date, bookings_guests.departure_date, min(hotdate) as hotdate, bookings_guests.num_adults, bookings_guests.num_children,"
               "Sum(transactions.dr) AS dr, Sum(transactions.cr) AS cr, Sum(transactions.dr_tax) AS dr_tax, "
               "Sum(transactions.cr_tax) AS cr_tax, Sum(transactions.dr) - Sum(transactions.cr) AS total "
               "FROM transactions INNER JOIN bookings_guests ON transactions.account = bookings_guests.id "
               "GROUP BY room, bookings_guests.id, bookings_guests.label, bookings_guests.arrival_date, bookings_guests.departure_date, bookings_guests.num_adults, bookings_guests.num_children, "
               "bookings_guests.checked_in "
               "HAVING Sum(transactions.dr) - Sum(transactions.cr) <> 0 AND arrival_date >= %s --AND checked_in IS Null "
               "ORDER BY arrival_date;")

        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text=logo),
                               Element(pos=(382,0), font=(title_font), align="centre", text="Deposits ledger for " + rpt_date.strftime('%d/%m/%Y')),
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Rm"),
                               Element(pos=(24, 54), font=(all_font),   align = "left", text="Name"),
                               Element(pos=(154, 54), font=(all_font),   align = "left", text="Invoice"),
                               Element(pos=(244, 54), font=(all_font),   align = "right", text="Adults"),
                               Element(pos=(284, 54), font=(all_font),   align = "right", text="Children"),
                               Element(pos=(324, 54), font=(all_font),   align = "left", text="Arrival"),
                               Element(pos=(389, 54), font=(all_font),   align = "left", text="Departure"),
                               Element(pos=(504, 54), font=(all_font),   align = "right", text="Dr"),
                               Element(pos=(554, 54), font=(all_font),   align = "right", text="Cr"),
                               Element(pos=(604, 54), font=(all_font),   align = "right", text="Balance"),
                               Rule((0, 66), 765, thickness = 1),
                               
                              ])
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="room"),
                               Element(pos=(24, 0), font=all_font, align="left", key="label"),
                               Element(pos=(154, 0), font=all_font, align="left", key="id"),
                               Element(pos=(244, 0), font=all_font, align="right", key="num_adults"),
                               Element(pos=(284, 0), font=all_font, align="right", key="num_children"),
                               Element(pos=(324, 0), font=all_font, align="left", getvalue=lambda x: x["arrival_date"].strftime('%d/%m/%Y')),
                               Element(pos=(389, 0), font=all_font, align="left", getvalue=lambda x: x["departure_date"].strftime('%d/%m/%Y')),
                               Element(pos=(504, 0), font=all_font, align="right", key="dr"),
                               Element(pos=(554, 0), font=all_font, align="right", key="cr"),
                               Element(pos=(604, 0), font=all_font, align="right", key="total" )
                               ])
 

        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1), 
            SumElement(pos=(504, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(554, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(604, 0), font=all_font, key="total",  align="right", format=lambda x:'£%0.2f'%x),
        ])    





        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        
        fname = report_loc + "deposits" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
  
                  
class Departures():
    def __init__(self, departure_date=datetime.now(), user="", pword="", database="pms", server=""):
        self.reportname = "arrivals"
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        
        logo = "logo.jpg"
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)
        # Database stuvv
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        SQL = ("select rooms.room, bookings.reference, bookings.label, bookings.id, departure_date, departure_date, "
               "num_adults, num_children, notes, channel_ref, voucher "
               "from bookings LEFT JOIN rooms ON bookings.room_id = rooms.hls_code "
               "where cancelled is Null AND departure_date = %s "
               "ORDER BY to_number(rooms.room, text(9999))")
        cursor.execute(SQL,(departure_date.strftime('%Y-%m-%d'),))
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text=logo),
                               Element(pos=(382,0), font=(title_font), align="centre", text="Departures List for " + departure_date.strftime('%d/%m/%Y')),
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Rm"),
                               Element(pos=(24, 54), font=(all_font),   align = "left", text="Name"),
                               Element(pos=(134, 54), font=(all_font),   align = "left", text="Reference"),
                               Element(pos=(224, 54), font=(all_font),   align = "right", text="Adults"),
                               Element(pos=(264, 54), font=(all_font),   align = "right", text="Children"),
                               Element(pos=(284, 54), font=(all_font),   align = "left", text="Voucher"),
                               Element(pos=(334, 54), font=(all_font),   align = "left", text="Channel"),
                               Element(pos=(384, 54), font=(all_font),   align = "left", text="Notes"),
                               Rule((0, 66), 765, thickness = 1),
                               
                              ])
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="room"),
                               Element(pos=(24, 0), font=all_font, align="left", key="label"),
                               Element(pos=(134, 0), font=all_font, align="left", key="reference"),
                               Element(pos=(224, 0), font=all_font, align="right", key="num_adults"),
                               Element(pos=(264, 0), font=all_font, align="right", key="num_children"),
                               Element(pos=(284, 0), font=all_font, align="left", key="voucher"),
                               Element(pos=(334, 0), font=all_font, align="left", key="channel_ref"),
                               Element(pos=(384, 0), font=all_font, align="left", getvalue=lambda x: x["notes"] if x["notes"] == None else fill(x["notes"], width=80) )
                               ])
 
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        
        fname = report_loc + "departures" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
            


class Res_made():
    def __init__(self, res_made_date=datetime.now(), user="", pword="", database="pms", server=""):
        self.reportname = "Bookings made on"
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor) 
        next_date = res_made_date + timedelta(days=1)

        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)

        
        SQL = ("select rooms.room, bookings.reference, bookings.label, bookings.id, departure_date, departure_date, "
               "num_adults, num_children, notes, channel_ref, voucher "
               "from bookings LEFT JOIN rooms ON bookings.room_id = rooms.hls_code "
               "where cancelled is Null AND booked_at > %s AND booked_at < %s"
               "ORDER BY departure_date")
        cursor.execute(SQL,(res_made_date.strftime('%Y-%m-%d'),next_date.strftime('%Y-%m-%d')))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text="Bookings made on " + res_made_date.strftime('%d/%m/%Y')),
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Arrival"),
                               Element(pos=(54, 54), font=(all_font),   align = "left", text="Name"),
                               Element(pos=(164, 54), font=(all_font),   align = "left", text="Reference"),
                               Element(pos=(254, 54), font=(all_font),   align = "right", text="Adults"),
                               Element(pos=(294, 54), font=(all_font),   align = "right", text="Children"),
                               Element(pos=(314, 54), font=(all_font),   align = "left", text="Voucher"),
                               Element(pos=(364, 54), font=(all_font),   align = "left", text="Channel"),
                               Element(pos=(414, 54), font=(all_font),   align = "left", text="Notes"),
                               Rule((0, 66), 765, thickness = 1),
                               
                              ])
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", getvalue=lambda x: x["departure_date"]),
                               Element(pos=(54, 0), font=all_font, align="left", key="label"),
                               Element(pos=(164, 0), font=all_font, align="left", key="reference"),
                               Element(pos=(254, 0), font=all_font, align="right", key="num_adults"),
                               Element(pos=(294, 0), font=all_font, align="right", key="num_children"),
                               Element(pos=(314, 0), font=all_font, align="left", key="voucher"),
                               Element(pos=(364, 0), font=all_font, align="left", key="channel_ref"),
                               Element(pos=(414, 0), font=all_font, align="left", getvalue=lambda x: x["notes"] if x["notes"] == None else fill(x["notes"], width=70) )
                               ])
 
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        
        fname = report_loc + "res_made" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
    
    
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname  ], shell=False)



 
class Board_basis():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server=""):
        self.reportname = "arrivals"
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)
        
        SQL = ("SELECT rooms.room, bookings.label, bookings.notes, bookings.reference, bookings.num_adults, bookings.num_children, rates_stay.rate_code, rates_stay.rate_date, rates.board_basis, rates.rate_description "
               "FROM ((bookings INNER JOIN rooms ON bookings.room_id = rooms.hls_code) INNER JOIN rates_stay ON bookings.id = rates_stay.id_bookings) INNER JOIN rates ON rates_stay.rate_code = rates.rate_code "
               "WHERE cancelled is Null AND rates_stay.rate_date=%s AND bookings.room_id < '3600' "
               "ORDER BY to_number(rooms.room, text(9999))")
        cursor.execute(SQL,(run_date.strftime('%Y-%m-%d'),))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text="Board basis List for " + run_date.strftime('%d/%m/%Y')),
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Rm"),
                               Element(pos=(24, 54), font=(all_font),   align = "left", text="Name"),
                               Element(pos=(134, 54), font=(all_font),   align = "left", text="Reference"),
                               Element(pos=(224, 54), font=(all_font),   align = "right", text="Adults"),
                               Element(pos=(264, 54), font=(all_font),   align = "right", text="Children"),
                               Element(pos=(284, 54), font=(all_font),   align = "left", text="Board basis"),
                               Element(pos=(400, 54), font=(all_font),   align = "left", text="Notes"),
                               Rule((0, 66), 765, thickness = 1),
                               
                              ])
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="room"),
                               Element(pos=(24, 0), font=all_font, align="left", key="label"),
                               Element(pos=(134, 0), font=all_font, align="left", key="reference"),
                               Element(pos=(224, 0), font=all_font, align="right", key="num_adults"),
                               Element(pos=(264, 0), font=all_font, align="right", key="num_children"),
                               Element(pos=(284, 0), font=all_font, align="left", key="rate_description"),
                               Element(pos=(400, 0), font=all_font, align="left", key="notes"),
                               
                                       ])
 
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        
        fname = report_loc + "board_basis" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
    
    
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname  ], shell=False)
            
            
            
            
            
            

        

class Audit_trail():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Transaction list for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT * from transactions where hotdate = %s ORDER By trans_timestamp")
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Timestamp"),
                               Element(pos=(100, 54), font=(all_font),   align = "left", text="Person"),
                               Element(pos=(150, 46), font=(all_font),   align="left", text="Charge"),
                               Element(pos=(150, 54), font=(all_font),   align = "left", text="code"),
                               Element(pos=(200, 54), font=(all_font),   align = "left", text="Account"),
                               
                               Element(pos=(250, 54), font=(all_font),   align = "left", text="Description"),
                               Element(pos=(500, 54), font=(all_font),   align = "right", text="Dr"),
                               Element(pos=(550, 54), font=(all_font),   align = "right", text="Cr"),
                               Element(pos=(600, 54), font=(all_font),   align = "right", text="Dr tax"),
                               Element(pos=(650, 54), font=(all_font),   align = "right", text="Cr tax"),
                               Element(pos=(700, 54), font=(all_font),   align = "left", text="Ledger ID"),

                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="trans_timestamp"),
                               Element(pos=(100, 0), font=all_font, align="left", key="person"),
                               Element(pos=(150, 0), font=all_font, align="left", key="charge_code"),
                               Element(pos=(200, 0), font=all_font, align="left", key="account"),
                               Element(pos=(250, 0), font=all_font, align="left", getvalue=lambda x: fill(x["description"], width=40)),
                               Element(pos=(500, 0), font=all_font, align="right", key="dr"),
                               Element(pos=(550, 0), font=all_font, align="right", key="cr"),
                               Element(pos=(600, 0), font=all_font, align="right", key="dr_tax"),
                               Element(pos=(650, 0), font=all_font, align="right", key="cr_tax"),
                               Element(pos=(700, 0), font=all_font, align="left", key="ledger_id"),
                               
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        

        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1), 
            SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            ])    
        
        fname = report_loc + "transaction_list" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
            
            
class Cash_up():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Cash up report for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        sub_font = ("Helvetica-Bold", 12)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT transactions.hotdate, person, transactions.charge_code, charge_codes.description as descr, transactions.trans_timestamp, "
               "transactions.description, transactions.dr, transactions.cr, coalesce(transactions.cr,0) - coalesce(transactions.dr,0) as bal, "
               "transactions.ledger_id, charge_codes.charge_group "
               "FROM transactions INNER JOIN charge_codes ON transactions.charge_code = charge_codes.charge_code "
               "WHERE charge_group='Payments' and hotdate = %s ORDER BY transactions.charge_code, trans_timestamp;")

        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname), 
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Timestamp"),
                               Element(pos=(100, 54), font=(all_font),  align = "left", text="Person"),
                               Element(pos=(150, 54), font=(all_font),  align = "left", text="Charge code"),
                               Element(pos=(225, 54), font=(all_font),  align = "left", text="Description"),
                               Element(pos=(500, 54), font=(all_font),  align = "right", text="Dr"),
                               Element(pos=(550, 54), font=(all_font),  align = "right", text="Cr"),
                               Element(pos=(600, 54), font=(all_font),  align = "right", text="Total"),
                               Rule((0, 66), 765, thickness = 1),
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="trans_timestamp"),
                               Element(pos=(100, 0), font=all_font, align="left", key="person"),
                               Element(pos=(150, 0), font=all_font, align="left", key="charge_code"),
                               Element(pos=(225, 0), font=all_font, align="left", key="descr"),
                               Element(pos=(500, 0), font=all_font, align="right", key="dr"),
                               Element(pos=(550, 0), font=all_font, align="right", key="cr"),
                               ])       
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), foot_font, align="center", text=report_lines[1]),
                               Element((765, 5), all_font, align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), all_font, align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        

        rpt.groupfooters = [
            Band([Rule((0, 0), 765),
                  Element((36, 0), sub_font, getvalue=lambda x: x["descr"]),
                  SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
                  SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
                  SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
                  ], getvalue = lambda x: x[3])
        ]                



        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1),
            Element((36, 0), ("Helvetica-Bold", 12), text="Total"),
            SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
            
            ])    
        
        fname = report_loc + "cash_up" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
    
    
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
            
    
class Housekeeping():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Housekeeping list for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 8)
        foot_font = ("Helvetica", 8)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  

        SQL = (
            """select rooms.room, rooms.sleeping,
            arrivals.label as arr_name, arrivals.voucher as arr_voucher, arrivals.num_adults as arr_adults, arrivals.num_children as arr_children, 
			arrivals.notes as arr_notes, arrivals.room_type as arr_room_type, departures.label as dep_name, 
            stayovers.label as stay_name, stayovers.voucher as stay_voucher, stayovers.num_adults as stay_adults, stayovers.num_children as stay_children, 
            stayovers.arrival_date as arrival_date, stayovers.departure_date as departure_date
            FROM rooms

            LEFT OUTER JOIN
            (SELECT label, room, voucher, num_adults, num_children, notes, room_type FROM bookings_guests 
            WHERE  arrival_date = %s AND cancelled is Null) arrivals
            ON rooms.room = arrivals.room
            
            LEFT OUTER JOIN
            (SELECT label, room, room_id, voucher FROM bookings_guests WHERE departure_date = %s AND cancelled is Null) departures
            ON rooms.room = departures.room
 
            LEFT OUTER JOIN
            (SELECT label, room, room_id, voucher, num_adults, num_children, arrival_date, departure_date, room_type_id FROM bookings_guests 
			WHERE departure_date > %s AND arrival_date < %s AND 
            cancelled is Null) stayovers
            ON rooms.room = stayovers.room
 
            WHERE
             rooms.sleeping = TRUE
            ORDER BY
            to_number(rooms.room, text(9999))
               """)
        
        
        
        rpt_date = trans_date.strftime('%Y-%m-%d')
        cursor.execute(SQL,(rpt_date, rpt_date, rpt_date, rpt_date))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align="left", text="Room"),
                               Element(pos=(50, 54), font=(all_font),  align="left", text="Arrival"),
                               Element(pos=(100, 54), font=(all_font), align="left", text="Stayover"),
                               Element(pos=(150, 54), font=(all_font), align="left", text="Departure"),
                               Element(pos=(200, 54), font=(all_font), align="left", text="Guests"),
                               Element(pos=(250, 54), font=(all_font), align="left", text="Voucher"),
                               Element(pos=(300, 54), font=(all_font), align="left", text="Booking notes"),
                               Element(pos=(450, 54), font=(all_font), align="left", text="Housekeeping notes"),
                               Element(pos=(550, 54), font=(all_font), align="left", text="Room Type"),
                               Element(pos=(650, 54), font=(all_font), align="left", text="Cleaned by"),
                               Element(pos=(700, 54), font=(all_font), align="left", text="Checked by"),

                               Rule((0, 66), 765, thickness = 1),
                               ])
        
        name_width = 10
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="room"),
                               Element(pos=(50, 0), font=all_font, align="left", getvalue=lambda x: x["arr_name"] if x["arr_name"]== None else fill(x["arr_name"], width=name_width)),
                               Element(pos=(100, 0), font=all_font, align="left", getvalue=lambda x: x["stay_name"] if x["stay_name"]== None else fill(x["stay_name"], width=name_width)),
                               Element(pos=(150, 0), font=all_font, align="left", getvalue=lambda x: x["dep_name"] if x["dep_name"]== None else fill(x["dep_name"], width=name_width)),
                               
                               Element(pos=(200, 0), font=all_font, align="left", getvalue=lambda x: " " if x["arr_adults"] == None and x["stay_adults"] == None
                                       else str(x["arr_adults"]) + "a " + str(x["arr_children"]) + "c" if x["stay_adults"]== None 
                                       else str(x["stay_adults"]) + "a " + str(x["stay_children"]) + "c" ), 
                               
                               Element(pos=(250, 0), font=all_font, align="left", key="arr_voucher"),
                               Element(pos=(300, 0), font=all_font, align="left", getvalue=lambda x: x["arr_notes"] if x["arr_notes"] == None else fill(x["arr_notes"], width=40)),

                               Element(pos=(450, 0), font=all_font, align="left", getvalue=lambda x: " " if x["departure_date"] == None else 
                                      "Change all linnen" if (((trans_date - x["arrival_date"]).days) % 3 == 0 and x["departure_date"] - timedelta(days=1) != trans_date) else " "
                                       
                                       ),
                               Element(pos=(550, 0), font=all_font, align="left", key="arr_room_type"), 
                               Rule((0, 0), 765, thickness = 1),
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        

       
        fname = report_loc + "housekeeping" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()


        #webbrowser.open(fname)

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
                
class Deep_cleans():

    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Deep cleaning list for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)

        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT room, deepcleandate, deepcleancycle from rooms WHERE deepcleandate IS NOT Null AND deepcleandate < current_date - 30 ORDER BY deepcleandate")
        
        
        
        
        cursor.execute(SQL,)
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Room"),
                               Element(pos=(150, 54), font=(all_font),   align = "left", text="Last deep clean"),
                               Element(pos=(300, 54), font=(all_font),   align = "left", text="Deep clean cycle"),
                              
                               Rule((0, 66), 765, thickness = 1),
                               ])
        
         
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="room"),
                               Element(pos=(150, 0), font=all_font, align="left", getvalue=lambda x: x["deepcleandate"].strftime('%d/%m/%Y')),
                               Element(pos=(300, 0), font=all_font, align="left", key="deepcleancycle"),
                                
                                 ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        

       
        fname = report_loc + "deep_cleans" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()


        #if True:
            #subprocess.Popen('deep_cleans.pdf',shell=True)
        #else:
        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname  ], shell=False)
                
class Maintenance_dockets():
    def __init__(self):
        #if True:
            #subprocess.Popen('maintenance_dockets.pdf',shell=True)
        #else:
        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-portrait", "maintenance_dockets.pdf"], shell=True)
        else:
            a = subprocess.check_output(["lp", "maintenance_dockets.pdf"  ], shell=False)        

class Housekeeping_checklist():
    def __init__(self, dep_date=datetime.date(datetime.now())):
        
        if dep_date.weekday() < 5:
            which_list = "weekday"
        else:
            which_list = "weekend"
        
        report_name = "housekeeping_checklist_" + which_list + ".pdf"
        
        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-portrait", report_name], shell=True)
        else:
            a = subprocess.check_output(["lp", report_name  ], shell=False)        


class Settlements():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Settlements list for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT * from transactions  INNER JOIN charge_codes ON transactions.charge_code = charge_codes.charge_code where hotdate = %s AND "
               "charge_codes.charge_group='Payments' ORDER By trans_timestamp")
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Timestamp"),
                               Element(pos=(100, 54), font=(all_font),   align = "left", text="Person"),
                               Element(pos=(150, 46), font=(all_font),   align="left", text="Charge"),
                               
                               Element(pos=(150, 54), font=(all_font),   align = "left", text="code"),
                               Element(pos=(200, 54), font=(all_font),   align = "left", text="Account"),
                               
                               Element(pos=(250, 54), font=(all_font),   align = "left", text="Description"),
                               Element(pos=(500, 54), font=(all_font),   align = "right", text="Dr"),
                               Element(pos=(550, 54), font=(all_font),   align = "right", text="Cr"),
                               Element(pos=(600, 54), font=(all_font),   align = "right", text="Dr tax"),
                               Element(pos=(650, 54), font=(all_font),   align = "right", text="Cr tax"),
                               Element(pos=(700, 54), font=(all_font),   align = "left", text="Ledger ID"),

                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="trans_timestamp"),
                               Element(pos=(100, 0), font=all_font, align="left", key="person"),
                               Element(pos=(150, 0), font=all_font, align="left", key="charge_code"),
                               Element(pos=(200, 0), font=all_font, align="left", key="account"),
                               Element(pos=(250, 0), font=all_font, align="left", getvalue=lambda x: fill(x["description"], width=40)),
                               Element(pos=(500, 0), font=all_font, align="right", key="dr"),
                               Element(pos=(550, 0), font=all_font, align="right", key="cr"),
                               Element(pos=(600, 0), font=all_font, align="right", key="dr_tax"),
                               Element(pos=(650, 0), font=all_font, align="right", key="cr_tax"),
                               Element(pos=(700, 0), font=all_font, align="left", key="ledger_id"),
                               
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        

        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1), 
            SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            ])    
        
        fname = report_loc + "transaction_list" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)


class Breakfast_list():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Breakfast list for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        sub_font = ("Helvetica-Bold", 12)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("""SELECT bookings_guests.id, bookings_guests.room AS RoomNum, bookings_guests.label AS guest_name, bookings_guests.arrival_date, 
        bookings_guests.departure_date, bookings_guests.checked_in, bookings_guests.num_adults, bookings_guests.num_children, bookings_guests.babies,
        bookings_guests.cancelled, newspaper, rate_stay.board_basis 
        FROM bookings_guests 
        INNER JOIN (SELECT rates_stay.rate_code, rates.board_basis, id_bookings FROM rates_stay 
        INNER JOIN rates on rates_stay.rate_code = rates.rate_code
        WHERE rate_date = %s) rate_stay ON bookings_guests.id = rate_stay.id_bookings 
        WHERE bookings_guests.arrival_date < %s AND bookings_guests.departure_date >= %s AND bookings_guests.cancelled Is Null 
        ORDER BY bookings_guests.Room::int;""")

        yesterday = trans_date - timedelta(days=1)
        cursor.execute(SQL,(yesterday.strftime('%Y-%m-%d'), trans_date.strftime('%Y-%m-%d'), trans_date.strftime('%Y-%m-%d')))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname), 
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Chkd in"),
                               Element(pos=(50, 54), font=(all_font),  align = "left", text="Rm"),
                               Element(pos=(75, 54), font=(all_font),  align = "left", text="Guest Name"),
                               Element(pos=(200, 54), font=(all_font),  align = "right", text="Adults"),
                               Element(pos=(225, 54), font=(all_font),  align = "right", text="Kids"),
                               Element(pos=(270, 54), font=(all_font),  align = "right", text="Babies"),
                               Element(pos=(320, 54), font=(all_font),  align = "right", text="H Chairs"),
                               Element(pos=(363, 54), font=(all_font),  align = "right", text="Table #"),
                               Element(pos=(375, 54), font=(all_font),  align = "left", text="Board basis"),
                               Element(pos=(440, 54), font=(all_font),  align = "left", text="Newspapers"),
                               Element(pos=(550, 54), font=(all_font),  align = "right", text="Alarm"),
                               Element(pos=(600, 54), font=(all_font),  align = "right", text="Notes"),
                               Rule((0, 66), 765, thickness = 1),
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 6), font=all_font, align="left", getvalue=lambda x: "" if x["checked_in"] == None else "X"),
                               Element(pos=(50, 6), font=all_font, align="left", key="roomnum"),
                               Element(pos=(75, 6), font=all_font, align="left", getvalue=lambda x: x["guest_name"] if x["guest_name"] == None else fill(x["guest_name"], width=22)),
                               Element(pos=(200, 6), font=all_font, align="right", key="num_adults"),
                               Element(pos=(225, 6), font=all_font, align="right", key="num_children"),
                               Element(pos=(270, 6), font=all_font, align="right", key="babies"),
                               Image(pos=(290,0),  width=30, height=30,  text="box.png"),
                               Image(pos=(330,0),  width=30, height=30,  text="box.png"),
                               Element(pos=(375, 6), font=all_font, align="left", key="board_basis"),
                               Element(pos=(440, 6), font=all_font, align="left", key="newspaper")
                               ])       
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), foot_font, align="center", text=report_lines[1]),
                               Element((765, 5), all_font, align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), all_font, align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        




        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1),
            Element((36, 0), ("Helvetica-Bold", 12), text="Total"),
            SumElement(pos=(200, 0), font=all_font, key="num_adults",  align="right"),
            SumElement(pos=(250, 0), font=all_font, key="num_children",  align="right"),
            #SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
            
            ])    
        
        
        fname = report_loc + "breakfast_list" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
    
    
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname ], shell=False)
        #os.startfile("breakfast_list.pdf")
        #import webbrowser
        #webbrowser.open("breakfast_list.pdf")
 
 
class Revenue():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Revenue for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        sub_font = ("Helvetica-Bold", 12)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT trans_timestamp, person, transactions.charge_code, account, "
               "transactions.description, dr, cr, dr_tax, cr_tax, ledger_id, charge_codes.nominal_code "
               "FROM transactions "
               "INNER JOIN charge_codes ON transactions.charge_code = charge_codes.charge_code "
               "WHERE hotdate = %s AND nominal_code > '3999' "
               "AND nominal_code < '5000' ORDER BY transactions.charge_code, trans_timestamp")
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Timestamp"),
                               Element(pos=(100, 54), font=(all_font),   align = "left", text="Person"),
                               Element(pos=(150, 46), font=(all_font),   align="left", text="Charge"),
                               Element(pos=(150, 54), font=(all_font),   align = "left", text="code"),
                               Element(pos=(200, 54), font=(all_font),   align = "left", text="Account"),
                               
                               Element(pos=(250, 54), font=(all_font),   align = "left", text="Description"),
                               Element(pos=(500, 54), font=(all_font),   align = "right", text="Dr"),
                               Element(pos=(550, 54), font=(all_font),   align = "right", text="Cr"),
                               Element(pos=(600, 54), font=(all_font),   align = "right", text="Dr tax"),
                               Element(pos=(650, 54), font=(all_font),   align = "right", text="Cr tax"),
                               Element(pos=(700, 54), font=(all_font),   align = "left", text="Ledger ID"),

                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="trans_timestamp"),
                               Element(pos=(100, 0), font=all_font, align="left", key="person"),
                               Element(pos=(150, 0), font=all_font, align="left", key="charge_code"),
                               Element(pos=(200, 0), font=all_font, align="left", key="account"),
                               Element(pos=(250, 0), font=all_font, align="left", getvalue=lambda x: fill(x["description"], width=40)),
                               Element(pos=(500, 0), font=all_font, align="right", key="dr"),
                               Element(pos=(550, 0), font=all_font, align="right", key="cr"),
                               Element(pos=(600, 0), font=all_font, align="right", key="dr_tax"),
                               Element(pos=(650, 0), font=all_font, align="right", key="cr_tax"),
                               Element(pos=(700, 0), font=all_font, align="left", key="ledger_id"),
                               
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        rpt.groupfooters = [
         Band([Rule((0, 0), 765),
               Element((36, 0), sub_font, getvalue=lambda x: x["charge_code"]),
               SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
               SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
               #SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
               ], getvalue = lambda x: x[2])
     ]                
        
        
        

        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1), 
            SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            ])    
        
        fname = report_loc + "revenue" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname ], shell=False)
              
        
        #webbrowser.open("revenue.pdf")


class Bookings_without_rates():
    """Finds future bookins which do not have rates"""
    
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Future bookings without rates from " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        sub_font = ("Helvetica-Bold", 12)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT bg.id, label, arrival_date, departure_date, reference, booked_at, room "
               "FROM bookings_guests bg "
               "LEFT JOIN rates_stay rs ON bg.id = rs.id_bookings "
               "WHERE rs.id_bookings IS NULL "
               "AND arrival_date >= %s "
               "AND cancelled IS NULL "
               "ORDER BY arrival_date, label")
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Arrival"),
                               Element(pos=(75, 54), font=(all_font),   align = "left", text="Departure"),
                               Element(pos=(150, 54), font=(all_font),   align="left", text="Label"),
                               Element(pos=(300, 54), font=(all_font),   align = "left", text="ID"),
                               Element(pos=(350, 54), font=(all_font),   align = "left", text="Reference"),
                               Element(pos=(450, 54), font=(all_font),   align = "left", text="Booked at"),
                               Element(pos=(600, 54), font=(all_font),   align = "right", text="Room"),
                               #Element(pos=(550, 54), font=(all_font),   align = "right", text="Cr"),
                               #Element(pos=(600, 54), font=(all_font),   align = "right", text="Dr tax"),
                               #Element(pos=(650, 54), font=(all_font),   align = "right", text="Cr tax"),
                               #Element(pos=(700, 54), font=(all_font),   align = "left", text="Ledger ID"),

                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", getvalue=lambda x: x["arrival_date"].strftime('%d/%m/%Y')),
                               Element(pos=(75, 0), font=all_font, align="left", getvalue=lambda x: x["departure_date"].strftime('%d/%m/%Y')),
                               Element(pos=(150, 0), font=all_font, align="left", key="label"),
                               Element(pos=(300, 0), font=all_font, align="left", key="id"),
                               Element(pos=(350, 0), font=all_font, align="left", key="reference"),
                               Element(pos=(450, 0), font=all_font, align="left", key="booked_at"),
                               Element(pos=(600, 0), font=all_font, align="right", key="room"),
                               #Element(pos=(600, 0), font=all_font, align="right", key="dr_tax"),
                               #Element(pos=(650, 0), font=all_font, align="right", key="cr_tax"),
                               #Element(pos=(700, 0), font=all_font, align="left", key="ledger_id"),
                               
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        #rpt.groupfooters = [
         #Band([Rule((0, 0), 765),
               #Element((36, 0), sub_font, getvalue=lambda x: x["charge_code"]),
               #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
               #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
               ##SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
               #], getvalue = lambda x: x[2])
     #]                
        
        
        

        #rpt.reportfooter = Band([
            #Rule((0, 0), 765, thickness = 1), 
            #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #])    
        
        fname = report_loc + "bookings_without_rates" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname ], shell=False)
              
        
        #webbrowser.open(fname)

        
class Channelrush_exceptions():
    """Finds future bookins which do not have rates"""
    
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Interface exceptions from " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        sub_font = ("Helvetica-Bold", 12)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT * from bookings_sync ORDER BY arrival_date, last_name")
        cursor.execute(SQL,)
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Arrival"),
                               Element(pos=(75, 54), font=(all_font),   align = "left", text="Departure"),
                               Element(pos=(150, 54), font=(all_font),   align="left", text="Last name"),
                               Element(pos=(250, 54), font=(all_font),   align = "left", text="ID"),
                               Element(pos=(300, 54), font=(all_font),   align = "left", text="Rooms"),
                               Element(pos=(350, 54), font=(all_font),   align = "left", text="Reference"),
                               Element(pos=(475, 54), font=(all_font),   align = "left", text="Issue"),
                               #Element(pos=(550, 54), font=(all_font),   align = "right", text="Cr"),
                               #Element(pos=(600, 54), font=(all_font),   align = "right", text="Dr tax"),
                               #Element(pos=(650, 54), font=(all_font),   align = "right", text="Cr tax"),
                               #Element(pos=(700, 54), font=(all_font),   align = "left", text="Ledger ID"),

                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", getvalue=lambda x: x["arrival_date"].strftime('%d/%m/%Y')),
                               Element(pos=(75, 0), font=all_font, align="left", getvalue=lambda x: x["departure_date"].strftime('%d/%m/%Y')),
                               Element(pos=(150, 0), font=all_font, align="left", key="last_name"),
                               Element(pos=(250, 0), font=all_font, align="left", key="id_bookings"),
                               Element(pos=(300, 0), font=all_font, align="left", key="num_rooms"),
                               Element(pos=(350, 0), font=all_font, align="left", key="cm_reference"),
                               Element(pos=(475, 0), font=all_font, align="left", key="issue"),
                               #Element(pos=(600, 0), font=all_font, align="right", key="dr_tax"),
                               #Element(pos=(650, 0), font=all_font, align="right", key="cr_tax"),
                               #Element(pos=(700, 0), font=all_font, align="left", key="ledger_id"),
                               
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        #rpt.groupfooters = [
         #Band([Rule((0, 0), 765),
               #Element((36, 0), sub_font, getvalue=lambda x: x["charge_code"]),
               #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
               #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
               ##SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
               #], getvalue = lambda x: x[2])
     #]                
        
        
        

        #rpt.reportfooter = Band([
            #Rule((0, 0), 765, thickness = 1), 
            #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #])    
        
        fname = report_loc + "interface_exceptions" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        #if sys.platform != 'linux':
            #a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        #else:
            #a = subprocess.check_output(["lp", fname ], shell=False)
              
        
        webbrowser.open(fname)
        
        
        
        
class Low_rate_bookings():
    """Finds future bookins which do not have rates"""
    
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "Future bookings with low rates from " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        sub_font = ("Helvetica-Bold", 12)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT reference, bookings_guests.id, arrival_date, departure_date, label, "
               "booked_at, room, amount "
               "FROM rates_stay "
               "INNER JOIN bookings_guests on bookings_guests.id = rates_stay.id_bookings "
               "WHERE amount < 49 "
               "AND room_type <> 'DUM' "
               "AND rate_date > %s "
               "ORDER BY arrival_date, label")
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Arrival"),
                               Element(pos=(75, 54), font=(all_font),   align = "left", text="Departure"),
                               Element(pos=(150, 54), font=(all_font),   align="left", text="Label"),
                               Element(pos=(300, 54), font=(all_font),   align = "left", text="ID"),
                               Element(pos=(350, 54), font=(all_font),   align = "left", text="Reference"),
                               Element(pos=(450, 54), font=(all_font),   align = "left", text="Booked at"),
                               Element(pos=(600, 54), font=(all_font),   align = "right", text="Room"),
                               Element(pos=(650, 54), font=(all_font),   align = "right", text="Rate"),
                               #Element(pos=(600, 54), font=(all_font),   align = "right", text="Dr tax"),
                               #Element(pos=(650, 54), font=(all_font),   align = "right", text="Cr tax"),
                               #Element(pos=(700, 54), font=(all_font),   align = "left", text="Ledger ID"),

                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", getvalue=lambda x: x["arrival_date"].strftime('%d/%m/%Y')),
                               Element(pos=(75, 0), font=all_font, align="left", getvalue=lambda x: x["departure_date"].strftime('%d/%m/%Y')),
                               Element(pos=(150, 0), font=all_font, align="left", key="label"),
                               Element(pos=(300, 0), font=all_font, align="left", key="id"),
                               Element(pos=(350, 0), font=all_font, align="left", key="reference"),
                               Element(pos=(450, 0), font=all_font, align="left", key="booked_at"),
                               Element(pos=(600, 0), font=all_font, align="right", key="room"),
                               Element(pos=(650, 0), font=all_font, align="right", key="amount"),
                               #Element(pos=(650, 0), font=all_font, align="right", key="cr_tax"),
                               #Element(pos=(700, 0), font=all_font, align="left", key="ledger_id"),
                               
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        #rpt.groupfooters = [
         #Band([Rule((0, 0), 765),
               #Element((36, 0), sub_font, getvalue=lambda x: x["charge_code"]),
               #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
               #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
               ##SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
               #], getvalue = lambda x: x[2])
     #]                
        
        
        

        #rpt.reportfooter = Band([
            #Rule((0, 0), 765, thickness = 1), 
            #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #])    
        
        fname = report_loc + "bookings_without_rates" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname ], shell=False)
              
        
        #webbrowser.open(fname)
    
    
        
        
    
    
class Rooms_revenue():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now()), end_date=datetime.date(datetime.now())):
        self.reportname = "Rooms revenue for " + trans_date.strftime('%d/%m/%Y') + " to " + end_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        sub_font = ("Helvetica-Bold", 12)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT count(room) "
               "FROM rooms "
               "WHERE roomtype <> 'DUM' or roomtype IS NOT NULL")
        cursor.execute(SQL,)
        row = cursor.fetchone()
        available_rooms = row[0]
        nights = end_date - trans_date
        nights = nights.days
        nights = nights + 1
        available_rooms = available_rooms * nights
        
        
        SQL = ("SELECT * FROM "
	       "(select 1 as key, sum(dr) as debits, sum(cr) as credits "
	       " FROM transactions "
	        "WHERE hotdate >= %s "
	        "AND hotdate <= %s "
	        "AND account = 'ACCOM') as trans "
	        "LEFT JOIN "
		"(SELECT 1 as key, count(bookings.id) as rooms_sold "
		"from bookings "
	        "where departure_date > %s "
	        "AND arrival_date <= %s "
	        "AND room_id::int < 3657 "
	        "AND cancelled IS NULL) "
                "as rmsld "
                "ON trans.key = rmsld.key ")
	
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),end_date.strftime('%Y-%m-%d'),trans_date.strftime('%Y-%m-%d'),end_date.strftime('%Y-%m-%d')))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(100, 54), font=(all_font),   align = "right", text="Sales"),
                               Element(pos=(150, 46), font=(all_font),   align = "right", text="Rooms"),
                               Element(pos=(150, 54), font=(all_font),   align = "right", text="sold"),
                               Element(pos=(200, 54), font=(all_font),   align = "right", text="ADR"),
                               Element(pos=(250, 46), font=(all_font),   align = "right", text="Available"),
                               Element(pos=(250, 54), font=(all_font),   align = "right", text="rooms"),
                               Element(pos=(325, 54), font=(all_font),   align = "right", text="Occupancy %"),
                               Element(pos=(375, 54), font=(all_font),   align = "right", text="RevPar"),
                               

                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        
        
        rpt.detailband = Band([
                               Element(pos=(100, 0), font=all_font, align="right", getvalue=lambda x: x["credits"] - x["debits"], format=lambda x:'£%0.2f'%x),
                               Element(pos=(150, 0), font=all_font, align="right", key="rooms_sold"),
                               Element(pos=(200, 0), font=all_font, align="right", getvalue=lambda x: (x["credits"] - x["debits"])/x["rooms_sold"], format=lambda x:'£%0.2f'%x),
                               Element(pos=(250, 0), font=all_font, align="right", text=str(available_rooms)),
                               Element(pos=(325, 0), font=all_font, align="right", getvalue=lambda x: (x["rooms_sold"]/available_rooms)*100, format=lambda x:'%0.2f'%x),
                               Element(pos=(375, 0), font=all_font, align="right", getvalue=lambda x: (x["credits"] - x["debits"])/available_rooms, format=lambda x:'£%0.2f'%x),
                               #Element(pos=(650, 0), font=all_font, align="right", key="cr_tax"),
                               #Element(pos=(700, 0), font=all_font, align="left", key="ledger_id"),
                               
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        #rpt.groupfooters = [
         #Band([Rule((0, 0), 765),
               #Element((36, 0), sub_font, getvalue=lambda x: x["charge_code"]),
               #SumElement(pos=(500, 0), font=all_font, key="d",  align="right", format=lambda x:'£%0.2f'%x),
               #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
               #SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
               #], getvalue = lambda x: x[2])
     #]                
        
        
        

        #rpt.reportfooter = Band([
            #Rule((0, 0), 765, thickness = 1), 
            #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #])    
        
        
        fname = report_loc + "rms_revenue" + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
    
    
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
              
        
        webbrowser.open("revenue.pdf")

class Sales_ledger_invoices():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now()), end_date=datetime.date(datetime.now())):
        self.reportname = "Sales ledger invoices for " + trans_date.strftime('%d/%m/%Y') + " to " + end_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        sub_font = ("Helvetica-Bold", 12)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT hotdate, charge_code, description, folio, account, dr, cr, bookings.id, bookings.booking_id, ledger_accounts.ledger_name "
               "FROM transactions "
               "INNER JOIN bookings ON transactions.charge_code = bookings.id "
               "INNER JOIN ledger_accounts ON transactions.account = ledger_accounts.ledger_id "
               "WHERE transactions.ledger_id = 'AR' "
               "AND hotdate >= %s "
               "AND hotdate <= %s "
               "ORDER BY hotdate")
        
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),end_date.strftime('%Y-%m-%d')))
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Date"),
                               Element(pos=(75, 54), font=(all_font),   align = "left", text="Account"),
                               Element(pos=(125, 54), font=(all_font), align = "left", text = "Name"),
                               Element(pos=(400, 46), font=(all_font),   align="left", text="Invoice"),
                               Element(pos=(400, 54), font=(all_font),   align = "left", text="Number"),
                               Element(pos=(460, 54), font=all_font, align="left", text="Folio"),

                               Element(pos=(550, 46), font=(all_font),   align = "right", text="Total"),
                               Element(pos=(600, 46), font=(all_font),   align = "right", text="Total"),
                               Element(pos=(550, 54), font=(all_font),   align = "right", text="debited"),
                               Element(pos=(600, 54), font=(all_font),   align = "right", text="credited"),
                               Rule((0, 66), 765, thickness = 1),
                               ])
        
        
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", getvalue=lambda x: x["hotdate"].strftime('%d/%m/%Y')),
                               Element(pos=(75, 0), font=all_font, align="left", key="account"),
                               Element(pos=(125, 0), font=all_font, align = "left", key= "ledger_name"),
                               Element(pos=(400, 0), font=all_font, align="left", key="charge_code"),
                               Element(pos=(460, 0), font=all_font, align="left", key="folio"),
                               Element(pos=(550, 0), font=all_font, align="right", key="dr"),
                               Element(pos=(600, 0), font=all_font, align="right", key="cr"),
                               ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
        #rpt.groupfooters = [
         #Band([Rule((0, 0), 765),
               #Element((36, 0), sub_font, getvalue=lambda x: x["charge_code"]),
               #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
               #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
               ##SumElement(pos=(600, 0), font=all_font, key="bal",  align="right", format=lambda x:'£%0.2f'%x),
               #], getvalue = lambda x: x[2])
     #]                
        
        
        

        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1), 
            SumElement(pos=(550, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(600, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            ])    
        

        fname = eod_loc + 'INV_' + trans_date.strftime('%Y-%m-%d') + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname], shell=False)
              
        
        #webbrowser.open("sales_ledger_invoices.pdf")

        
class Daily_close_summary():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "End of day report for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        sub_font = ("Helvetica-Bold", 12)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("SELECT * from end_of_day_rpt "
               "ORDER BY rpt_group_sort, rpt_row")
        cursor.execute(SQL,)
        
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Account"),
                               Element(pos=(225, 54), font=(all_font),   align = "right", text="Total"),
  
                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        rpt.groupheaders = [
         Band([Rule((0, 0), 765),
               Element((36, 0), sub_font, getvalue=lambda x: x["rpt_group"], format = lambda x: x),
               
               ], getvalue = lambda x: x["rpt_group"])
     ]             
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="label"),
                               Element(pos=(225, 0), font=all_font, align="right", key="total"),
                                 
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
           
        
        
        

        #rpt.reportfooter = Band([
            #Rule((0, 0), 765, thickness = 1), 
            #SumElement(pos=(500, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(550, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            ##SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            ##SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #])    
        
        fname = report_loc + 'EOD_' + trans_date.strftime('%Y-%m-%d') + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname ], shell=False)
              
        
        #webbrowser.open(fname)
        

class Daily_close_je():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        self.reportname = "End of day journal entry for " + trans_date.strftime('%d/%m/%Y')
        title_font = ("Helvetica", 16)
        all_font = ("Helvetica", 10)
        foot_font = ("Helvetica", 8)
        sub_font = ("Helvetica-Bold", 12)
        report_lines = get_hotel_data(user=user, pword=pword, database=database, 
                                     server=server)


        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        
        SQL = ("select coalesce(charge_group, 'Ledger movements') as group, account, c.nominal_code, "
               "sum(cr) as cr, sum(dr) AS dr "
               "FROM transactions_close t "
               "LEFT JOIN charge_codes_all c on c.charge_code = t.account  "
               "WHERE hotdate = %s " 
               "AND ((charge_group <> 'Payments' OR charge_group IS Null) OR (charge_group = 'Payments' AND editable IS FALSE)) "
               "group by charge_group, account, c.nominal_code "
               "ORDER BY charge_group, account")
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))    
        rows = cursor.fetchall()
        
        rpt = Report(datasource=rows)
        
        rpt.pageheader = Band([Image(pos=(0,0),  width=115, height=48, text="logo.jpg"),
                               Element(pos=(382,0), font=(title_font), align="centre", text=self.reportname),  #+ run_date.strftime('%d/%m/%Y')
                               Element(pos=(0, 54), font=(all_font),   align = "left", text="Group"),
                               Element(pos=(100, 54), font=(all_font),   align = "left", text="Account"),
                               Element(pos=(200, 54), font=(all_font),   align = "left", text="Nominal code"),
                               Element(pos=(300, 54), font=(all_font),   align = "right", text="Dr"),
                               Element(pos=(400, 54), font=(all_font),   align = "right", text="Cr"),
  
                               Rule((0, 66), 765, thickness = 1),
                           
                               ])
        
        rpt.detailband = Band([Element(pos=(0, 0), font=all_font, align="left", key="group"),
                               Element(pos=(100, 0), font=all_font, align="left", key="account"),
                               Element(pos=(200, 0), font=all_font, align="left", key="nominal_code"),                               Element(pos=(300, 0), font=all_font, align="right", key="dr"),                               Element(pos=(400, 0), font=all_font, align="right", key="cr"),                                 
                                ])       
        
        
        rpt.pagefooter = Band([Rule((0, 0), 765, thickness = 1),
                               Element((382, 0), foot_font, align="center", text=report_lines[0]),
                               Element((382, 10), ("Helvetica", 8), align="center", text=report_lines[1]),
                               Element((765, 5), ("Helvetica", 10), align = "right", sysvar = "pagenumber", format = lambda x: "Page %d" % x),
                               Element((0,   5), ("Helvetica", 10), align = "left", text=datetime.now().strftime('Printed on %d/%m/%Y at %H:%M')),
                               ])         
        
           

        rpt.reportfooter = Band([
            Rule((0, 0), 765, thickness = 1), 
            SumElement(pos=(300, 0), font=all_font, key="dr",  align="right", format=lambda x:'£%0.2f'%x),
            SumElement(pos=(400, 0), font=all_font, key="cr",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(600, 0), font=all_font, key="dr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            #SumElement(pos=(650, 0), font=all_font, key="cr_tax",  align="right", format=lambda x:'£%0.2f'%x),
            ])    
        
        fname = eod_loc + 'JE_' + trans_date.strftime('%Y-%m-%d') + ".pdf"
        canvas = Canvas(fname, pagesize=landscape(A4))
        rpt.generate(canvas)
        canvas.save()

        if sys.platform != 'linux':
            a = subprocess.check_output(["gsprint.exe", "-copies",  "1", "-landscape", fname], shell=True)
        else:
            a = subprocess.check_output(["lp", fname ], shell=False)
              
        
        #webbrowser.open(fname)       
        
class Daily_close_file():
    def __init__(self, run_date=datetime.now(), user="", pword="", database="pms", server="", trans_date=datetime.date(datetime.now())):
        
        cnxn = psycopg2.connect(database=database, user=user, password=pword, host=server)
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)  
        fname = eod_loc + 'file_'  + trans_date.strftime('%Y-%m-%d') + ".csv"
        ofile  = open(fname, "w")
        writer = csv.writer(ofile,  delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC, lineterminator=os.linesep)            
        
        SQL = ("select coalesce(charge_group, 'Ledger movements') as group, account, c.nominal_code, "
               "sum(cr) as cr, sum(dr) AS dr "
               "FROM transactions_close t "
               "LEFT JOIN charge_codes_all c on c.charge_code = t.account  "
               "WHERE hotdate = %s " 
               "AND ((charge_group <> 'Payments' OR charge_group IS Null) OR (charge_group = 'Payments' AND editable IS FALSE)) "
               "group by charge_group, account, c.nominal_code "
               "ORDER BY charge_group, account")
        cursor.execute(SQL,(trans_date.strftime('%Y-%m-%d'),))    
        rows = cursor.fetchall()
        colnames = [desc[0] for desc in cursor.description]
        writer.writerow(colnames)
        for row in rows:
            writer.writerow(row)
            
if __name__ == '__main__':
    a = Channelrush_exceptions(user="dave", pword="74840625", database="pms", server="192.168.0.6", trans_date=datetime.date(datetime(2018, 6, 28)))
