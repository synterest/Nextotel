from datetime import datetime, timedelta, time
import time

import psycopg2
import psycopg2.extras

database = "pms"


########################################################################
class Make_rates():
    """Class to create the rates files for updating"""

    #----------------------------------------------------------------------
    def __init__(self, database='', user='', password='', server='', num_days=365):
        """Constructor"""
        self.database = database
        self.user=user
        self.password=password
        self.server=server
        self.num_days = num_days
        self.prep_updates()
        
   
    

    def prep_updates(self):
        cnxn = psycopg2.connect(database=self.database, user=self.user, password=self.password, host=self.server)
        
        
        self.make_avail(self.num_days)
    
        SQL = "UPDATE availability set oldroomssold = roomssold WHERE availdate >= '" + datetime.now().date().strftime('%Y-%m-%d') + "'"
        cursor = cnxn.cursor()
        cursor.execute(SQL)
        SQL = "UPDATE availability set roomssold = 0 WHERE availdate >= '" + datetime.now().date().strftime('%Y-%m-%d') + "'"
        cursor.execute(SQL)
        print("updates prepared")
    
    
        csr_avail = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        csr_avail.execute("select * from availability order by availdate")
        avail_rows = csr_avail.fetchall()
        today_date = datetime.date(datetime.now())
    
        
        SQL = "SELECT  bookings.* "
        SQL = SQL + "FROM bookings  "
        SQL = SQL + "where arrival_date >= %s  AND cancelled is Null AND checked_out IS NULL;"
        start_date = today_date - timedelta(days=10)
        print(SQL)
        csr_bookings = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        csr_bookings.execute(SQL, (start_date,))
        rows = csr_bookings.fetchall()
        for row in rows:
            n = row["departure_date"] - row["arrival_date"]
            nights = n.days
            csr_availability = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            
            for i in range(nights):
                SQL = "SELECT * FROM availability WHERE availdate = %s and roomtype = %s"
                current_date = row["arrival_date"] + timedelta(days=i)
                csr_availability.execute(SQL,(current_date, row["room_type"]))
                avail_row = csr_availability.fetchone()
                if avail_row is None:
                    SQL = "INSERT INTO availability (roomssold, availdate, roomtype) values (%s, %s, %s)"
                    new_rooms_sold = 1
                else:
                    SQL = "UPDATE availability SET roomssold = %s WHERE availdate = %s and roomtype = %s"
                    new_rooms_sold = avail_row["roomssold"] + 1
                    
                csr_availability.execute(SQL, (new_rooms_sold, current_date, row["room_type"]) )
            
                cnxn.commit()
        print("Finished availability")
        
    
        
    def make_avail(self, numdays=1):
        cnxn = psycopg2.connect(database=self.database, user=self.user, password=self.password, host=self.server)
        
        SQL = "select max(availdate) as availdate from availability"
        cursor = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute(SQL)
        avail = cursor.fetchone()
        if avail["availdate"] is None:
            startdate = datetime.date(datetime.now())
        else:
            startdate = avail["availdate"]
       
        csr_room_types = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        csr_insert = cnxn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        SQL = "SELECT base_type from room_types WHERE base_type IS NOT NULL"
        csr_room_types.execute(SQL)
        room_types = csr_room_types.fetchall()
        
        max_date = datetime.date(datetime.now()) + timedelta(days=numdays)
        
        
        days_to_add = max_date - startdate
        days_to_add = days_to_add.days
        
        if days_to_add < 1:
            return
        
        print(days_to_add)
        for r_type in room_types:
            for all_days in range(numdays):
                SQL = "INSERT INTO availability (roomssold, availdate, roomtype, mlos) VALUES (%s, %s, %s, %s)"
                
                trans_date=startdate + timedelta(days = all_days)
                
                the_data = (0, trans_date, r_type["base_type"], 1)
                csr_insert.execute(SQL, the_data)
                
                
            cnxn.commit()
                
    


if __name__ == "__main__":
    a = Make_rates()
  
    #make_avail(365)