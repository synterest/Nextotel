# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_login_form(object):
    def setupUi(self, login_form):
        login_form.setObjectName("login_form")
        login_form.resize(223, 129)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        login_form.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(login_form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(login_form)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.label = QtWidgets.QLabel(login_form)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.cbo_db_name = QtWidgets.QComboBox(login_form)
        self.cbo_db_name.setObjectName("cbo_db_name")
        self.gridLayout.addWidget(self.cbo_db_name, 0, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(login_form)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 1)
        self.username = QtWidgets.QLineEdit(login_form)
        self.username.setObjectName("username")
        self.gridLayout.addWidget(self.username, 1, 1, 1, 1)
        self.password = QtWidgets.QLineEdit(login_form)
        self.password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password.setObjectName("password")
        self.gridLayout.addWidget(self.password, 2, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(login_form)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.label_2.setBuddy(self.password)
        self.label.setBuddy(self.username)

        self.retranslateUi(login_form)
        self.buttonBox.accepted.connect(login_form.accept)
        self.buttonBox.rejected.connect(login_form.reject)
        QtCore.QMetaObject.connectSlotsByName(login_form)
        login_form.setTabOrder(self.username, self.password)
        login_form.setTabOrder(self.password, self.cbo_db_name)

    def retranslateUi(self, login_form):
        _translate = QtCore.QCoreApplication.translate
        login_form.setWindowTitle(_translate("login_form", "Please log in..."))
        self.label_2.setText(_translate("login_form", "Password:"))
        self.label.setText(_translate("login_form", "User name:"))
        self.label_3.setText(_translate("login_form", "Database:"))

import pms_rc
