# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'guest_correspondence.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_guest_correspondence(object):
    def setupUi(self, guest_correspondence):
        guest_correspondence.setObjectName("guest_correspondence")
        guest_correspondence.resize(852, 453)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        guest_correspondence.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(guest_correspondence)
        self.verticalLayout.setObjectName("verticalLayout")
        self.guest_view = QtWidgets.QTableView(guest_correspondence)
        self.guest_view.setObjectName("guest_view")
        self.verticalLayout.addWidget(self.guest_view)

        self.retranslateUi(guest_correspondence)
        QtCore.QMetaObject.connectSlotsByName(guest_correspondence)

    def retranslateUi(self, guest_correspondence):
        _translate = QtCore.QCoreApplication.translate
        guest_correspondence.setWindowTitle(_translate("guest_correspondence", "Guest correspondence"))

import pms_rc
