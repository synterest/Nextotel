# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'deep_cleans.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_deep_cleans(object):
    def setupUi(self, deep_cleans):
        deep_cleans.setObjectName("deep_cleans")
        deep_cleans.resize(511, 516)
        self.rooms_view = QtWidgets.QTableView(deep_cleans)
        self.rooms_view.setGeometry(QtCore.QRect(10, 20, 491, 481))
        self.rooms_view.setObjectName("rooms_view")

        self.retranslateUi(deep_cleans)
        QtCore.QMetaObject.connectSlotsByName(deep_cleans)

    def retranslateUi(self, deep_cleans):
        _translate = QtCore.QCoreApplication.translate
        deep_cleans.setWindowTitle(_translate("deep_cleans", "Housekeeping"))

