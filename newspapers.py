# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'newspapers.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_newspapers(object):
    def setupUi(self, newspapers):
        newspapers.setObjectName("newspapers")
        newspapers.resize(1026, 650)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        newspapers.setWindowIcon(icon)
        self.verticalLayout = QtWidgets.QVBoxLayout(newspapers)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(898, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.newspaper_sheet = QtWidgets.QTableView(newspapers)
        self.newspaper_sheet.setObjectName("newspaper_sheet")
        self.gridLayout.addWidget(self.newspaper_sheet, 1, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)

        self.retranslateUi(newspapers)
        QtCore.QMetaObject.connectSlotsByName(newspapers)

    def retranslateUi(self, newspapers):
        _translate = QtCore.QCoreApplication.translate
        newspapers.setWindowTitle(_translate("newspapers", "Newspaper prices"))

import pms_rc
