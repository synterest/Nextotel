class Postings():
    def __init__(self, person=None, charge_code=None, description="", amount=0, folio=1, trans_date=None, res_ref = ""):
        self.person = person
        self.charge_code = charge_code
        self.description = description
        self.amount = amount
        self.folio = folio
        self.trans_date = trans_date
        self.res_ref = res_ref
        tax_code = QSqlQuery()
        tax_code.exec_("SELECT tax_code from charge_codes WHERE charge_code = '{0}'".format(self.charge_code))
        tax_code.first()
        tax = taxes[tax_code.value(0)]+1.0
        query = QSqlQuery()
        QSqlDatabase.database().transaction()
        query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, dr, dr_tax, folio, account, hotdate, ledger_id)"
                   "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
        query.addBindValue(trans_time)
        query.addBindValue(self.person)
        query.addBindValue(self.charge_code)
        query.addBindValue(self.description)
        query.addBindValue(self.amount)
        query.addBindValue("{:.2f}".format(float(self.amount)-float(self.amount)/tax))
        query.addBindValue(self.folio)
        query.addBindValue(self.res_ref)
        query.addBindValue(self.trans_date)
        query.addBindValue("RGL")
        
        query.exec_()
        if not query.isActive():
            print(query.lastError().text())
            QSqlDatabase.database().rollback()
            QMessageBox.critical(None, "Posting Error!", query.lastError().text())

            return
        
        query.prepare("insert into transactions (trans_timestamp, person, charge_code, description, cr, cr_tax, folio, account, hotdate, ledger_id)"
                   "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
        t = datetime.now()
        trans_time = t.strftime('%Y-%m-%d %H:%M:%S')
        query.addBindValue(trans_time)
        query.addBindValue(self.person)
        query.addBindValue(self.res_ref)
        query.addBindValue(self.description)
        query.addBindValue(self.amount)
        query.addBindValue("{:.2f}".format(float(self.amount)-(float(self.amount)/tax)))
        query.addBindValue(self.folio)
        query.addBindValue(self.charge_code)
        query.addBindValue(self.trans_date)
        query.addBindValue("RGL")
        query.exec_()
        if not query.isActive():
            print(query.lastError().text())
            QSqlDatabase.database().rollback()
            QMessageBox.critical(None, "Posting Error!", query.lastError().text())
            self.close()
            return


        QSqlDatabase.database().commit()
 