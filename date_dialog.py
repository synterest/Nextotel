# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'date_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_date_dialog_form(object):
    def setupUi(self, date_dialog_form):
        date_dialog_form.setObjectName("date_dialog_form")
        date_dialog_form.resize(203, 108)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/icons/nexthotel_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        date_dialog_form.setWindowIcon(icon)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(date_dialog_form)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.start_date = QtWidgets.QDateEdit(date_dialog_form)
        self.start_date.setCalendarPopup(True)
        self.start_date.setObjectName("start_date")
        self.verticalLayout.addWidget(self.start_date)
        self.end_date = QtWidgets.QDateEdit(date_dialog_form)
        self.end_date.setCalendarPopup(True)
        self.end_date.setObjectName("end_date")
        self.verticalLayout.addWidget(self.end_date)
        self.buttonBox = QtWidgets.QDialogButtonBox(date_dialog_form)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(date_dialog_form)
        self.buttonBox.accepted.connect(date_dialog_form.accept)
        self.buttonBox.rejected.connect(date_dialog_form.reject)
        QtCore.QMetaObject.connectSlotsByName(date_dialog_form)

    def retranslateUi(self, date_dialog_form):
        _translate = QtCore.QCoreApplication.translate
        date_dialog_form.setWindowTitle(_translate("date_dialog_form", "Enter date..."))

import pms_rc
