import configparser
import sys
import os

config = configparser.ConfigParser()
config_file = sys.path[0] + "/pms.ini"
config.read(config_file)
if config.has_option('file_locations', 'backups'):
    backup_loc = config['file_locations']['backups']
else:
    backup_loc = os.path.expanduser("~/")
if config.has_option('file_locations', 'folios'):
    folio_loc = config['file_locations']['folios']
else:
    folio_loc = os.path.expanduser("~/")
if config.has_option('file_locations', 'reports'):
    report_loc = config['file_locations']['reports']
else:
    report_loc = os.path.expanduser("~/")
if config.has_option('file_locations', 'eod_reports'):
    eod_loc = config['file_locations']['eod_reports']
else:
    eod_loc = os.path.expanduser("~/")
